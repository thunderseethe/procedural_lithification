extern crate log;

use log::info;
use std::borrow::Borrow;

#[derive(Debug)]
pub struct SparseVector<Idx, Val> {
    indices: Vec<Idx>,
    data: Vec<Val>,
}

impl<Idx, Val> std::default::Default for SparseVector<Idx, Val> {
    fn default() -> Self {
        Self::with_capacity(9)
    }
}

impl<Idx, Val> SparseVector<Idx, Val> {
    fn with_capacity(capacity: usize) -> Self {
        Self {
            indices: Vec::with_capacity(capacity),
            data: Vec::with_capacity(capacity),
        }
    }
    pub fn keys(&self) -> &[Idx] {
        &self.indices[..]
    }

    pub fn val_iter(&self) -> impl Iterator<Item=&Val> {
        self.data.iter()
    }
    pub fn into_val_iter(self) -> impl Iterator<Item=Val> {
        self.data.into_iter()
    }

    pub fn len(&self) -> usize {
        // indices and data should always be the same length so we rely on data length
        self.data.len()
    }
}

impl<Idx: Ord, Val> SparseVector<Idx, Val> {
    pub fn get<I>(&self, indx_ref: I) -> Option<&Val> 
    where
        I: Borrow<Idx>
    {
        let indx = indx_ref.borrow(); 
        self.indices.binary_search(indx).ok().map(|indx| &self.data[indx])
    }

    pub fn insert(&mut self, key: Idx, val: Val) -> (&Val, Option<Val>) {
        match self.indices.binary_search(&key) {
            Err(idx) => {
                self.indices.insert(idx, key);
                self.data.insert(idx, val);
                (&self.data[idx], None)
            }
            Ok(idx) => {
                let old_val = std::mem::replace(&mut self.data[idx], val);
                (&self.data[idx], Some(old_val))
            },
        }
    }

    pub fn remove<I>(&mut self, key: I) -> Option<Val> 
    where
        I: Borrow<Idx>,
    {
        self.indices.binary_search(key.borrow()).ok().map(|idx| {
            self.indices.remove(idx);
            self.data.remove(idx)
        })
    }

    pub fn contains_key<I>(&self, key: I) -> bool 
    where
        I: Borrow<Idx>
    {
        self.indices.binary_search(key.borrow()).is_ok()
    }

    pub fn iter(&self) -> <&SparseVector<Idx, Val> as IntoIterator>::IntoIter {
        (&self).into_iter()
    }

    pub fn iter_mut(&mut self) -> <&mut SparseVector<Idx, Val> as IntoIterator>::IntoIter {
        self.into_iter()
    }

    pub fn value_iter(&self) -> impl Iterator<Item=&Val> {
        self.data.iter()
    }
}

impl<Idx, Val> IntoIterator for SparseVector<Idx, Val> {
    type Item = <Self::IntoIter as Iterator>::Item;
    type IntoIter = std::iter::Zip<
        std::vec::IntoIter<Idx>,
        std::vec::IntoIter<Val>,
    >;

    fn into_iter(self) -> Self::IntoIter {
        self.indices.into_iter().zip(self.data.into_iter())
    }
}

impl<'a, Idx, Val> IntoIterator for &'a SparseVector<Idx, Val> {
    type Item = <Self::IntoIter as Iterator>::Item;
    type IntoIter = std::iter::Zip<
        std::slice::Iter<'a, Idx>,
        std::slice::Iter<'a, Val>,
    >;

    fn into_iter(self) -> Self::IntoIter {
        self.indices.iter().zip(self.data.iter())
    }
}

impl<'a, Idx, Val> IntoIterator for &'a mut SparseVector<Idx, Val> {
    type Item = <Self::IntoIter as Iterator>::Item;
    type IntoIter = std::iter::Zip<
        std::slice::IterMut<'a, Idx>,
        std::slice::IterMut<'a, Val>,
    >;

    fn into_iter(self) -> Self::IntoIter {
        self.indices.iter_mut().zip(self.data.iter_mut())
    }
}