extern crate heapless;
pub extern crate shared_memory;
extern crate raw_sync;

use heapless::{spsc, spsc::Queue};
use shared_memory::{Shmem, ShmemConf};
pub use shared_memory::ShmemError;
use typenum::U16;
use std::marker::Send;
use std::fmt;

type Q<T> = Queue<T, U16>;

const fn queue_mem_size<T>() -> usize {
    std::mem::size_of::<Q<T>>()
}

pub struct Producer<'a, T> {
    shmem: Shmem,
    producer: spsc::Producer<'a, T, U16>,
}
impl<'a, T: fmt::Debug> fmt::Debug for Producer<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let queue: &Q<T> = unsafe {
            let ptr= self.shmem.as_ptr() as *mut Q<T>;
            ptr.as_ref().unwrap()
        };

        f.debug_struct("Producer")
            .field("producer", queue)
            .finish()
    }
}
unsafe impl<'a, T> Send for Producer<'a, T> {}
impl<'a, T: 'a> Producer<'a, T> {
    pub fn new() -> Result<Self, ShmemError> {
        Self::create_from_os_id::<&str>(None)
    }

    pub fn with_os_id<S: AsRef<str>>(os_id: S) -> Result<Self, ShmemError> {
       Self::create_from_os_id(Some(os_id)) 
    }

    fn create_from_os_id<S: AsRef<str>>(opt_os_id: Option<S>) -> Result<Self, ShmemError> {
        let shmem_conf = ShmemConf::new()
            .size(queue_mem_size::<T>());
        let shmem_conf = if let Some(os_id) = opt_os_id {
            shmem_conf.os_id(os_id)
        } else {
            shmem_conf
        };
        let shmem = shmem_conf.create()?;

        let queue_ptr = shmem.as_ptr() as *mut Q<T>;
        let producer = unsafe {
            std::ptr::write(queue_ptr, Q::new());
            queue_ptr.as_mut().unwrap().split().0
        };

        Ok(Producer { 
            shmem, 
            producer
        })
    }
}

impl<T> Producer<'_, T> {
    pub fn get_os_id(&self) -> &str {
        self.shmem.get_os_id()
    }

    pub fn ready(&self) -> bool {
        self.producer.ready()
    }

    pub fn enqueue(&mut self, item: T) -> Result<(), T> {
        self.producer.enqueue(item)
    }
}

pub struct Consumer<'a, T> {
    shmem: Shmem,
    consumer: spsc::Consumer<'a, T, U16>,
}
unsafe impl<'a, T> Send for Consumer<'a, T> {}
impl<'a, T: fmt::Debug> fmt::Debug for Consumer<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let queue: &Q<T> = unsafe {
            let ptr= self.shmem.as_ptr() as *mut Q<T>;
            ptr.as_ref().unwrap()
        };

        f.debug_struct("Consumer")
            .field("consumer", queue)
            .finish()
    }
}
impl<'a, T: 'a> Consumer<'a, T> {
    pub fn new<S>(os_id: S) -> Result<Consumer<'a, T>, ShmemError> 
    where
        S: AsRef<str>,
    {
        let shmem = ShmemConf::new()
            .os_id(os_id)
            .size(queue_mem_size::<T>())
            .open()?;

        let queue_ptr = shmem.as_ptr() as *mut Q<T>;
        let consumer = unsafe {
            queue_ptr.as_mut().unwrap().split().1
        };

        Ok(Consumer {
            shmem,
            consumer
        })
    }
}
impl<T> Consumer<'_, T> {
    pub fn ready(&self) -> bool {
        self.consumer.ready()
    }
    pub fn dequeue(&mut self) -> Option<T> {
        self.consumer.dequeue()
    }
}
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn pass_a_value() {
        let mut producer: Producer<'_, i32> = Producer::new().unwrap();
        let mut consumer: Consumer<'_, i32> = Consumer::new(producer.get_os_id()).unwrap();

        producer.enqueue(1234i32).unwrap();

        assert_eq!(consumer.dequeue(), Some(1234i32));
    }

    #[test]
    fn fill_producer_buffer() {
        let mut producer: Producer<'_, i32> = Producer::new().unwrap();
        let mut consumer: Consumer<'_, i32> = Consumer::new(producer.get_os_id()).unwrap();

        for i in 0..16i32 {
            producer.enqueue(i).unwrap();
        }
        assert_eq!(producer.enqueue(17), Err(17));
    }

    #[test]
    fn empty_consumer_buffer_is_none() {
        let mut producer: Producer<'_, i32> = Producer::new().unwrap();
        let mut consumer: Consumer<'_, i32> = Consumer::new(producer.get_os_id()).unwrap();

        assert_eq!(consumer.dequeue(), None);
    }

    #[test]
    fn enqueue_dequeu_orders_elements_correctly() {
        let mut producer: Producer<'_, i32> = Producer::new().unwrap();
        let mut consumer: Consumer<'_, i32> = Consumer::new(producer.get_os_id()).unwrap();

        producer.enqueue(1i32).unwrap();
        producer.enqueue(2i32).unwrap();
        producer.enqueue(3i32).unwrap();

        assert_eq!(consumer.dequeue(), Some(1i32));
        assert_eq!(consumer.dequeue(), Some(2i32));
        assert_eq!(consumer.dequeue(), Some(3i32));
    }
}