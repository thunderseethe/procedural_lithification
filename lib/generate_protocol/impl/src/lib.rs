extern crate heck;
extern crate proc_macro;

use heck::CamelCase;
use proc_macro2::{Ident, Span, TokenStream};
use quote::{format_ident, quote, ToTokens};
use syn;
use syn::{
    braced, parse::*, parse_macro_input, Attribute,
    Token,
};

mod message;
use message::*;

mod amethyst;
use amethyst::*;

mod keyword {
    syn::custom_keyword!(name);
    syn::custom_keyword!(message);
    syn::custom_keyword!(client);
    syn::custom_keyword!(server);
}

#[proc_macro]
pub fn generate_protocol(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let protocol: ProtocolSpec = parse_macro_input!(input as ProtocolSpec);
    proc_macro::TokenStream::from(protocol.into_token_stream())
}

struct ProtocolSpec {
    name: Ident,
    client_spec: ClientSpec,
    server_spec: ServerSpec,
}
impl Parse for ProtocolSpec {
    fn parse(input: ParseStream) -> Result<Self> {
        let name = if input.peek(keyword::name) {
            input.parse::<keyword::name>()?;
            let name: Ident = input.parse()?;
            input.parse::<Token![,]>()?;
            name
        } else {
            Ident::new("Protocol", Span::call_site())
        };

        let attrs = input.call(Attribute::parse_outer)?;
        let (client_spec, server_spec) = if input.peek(keyword::client) {
            (input.parse::<ClientSpec>()?.with_attrs(attrs), input.parse::<ServerSpec>()?)
        } else if input.peek(keyword::server) {
            let server_spec = input.parse::<ServerSpec>()?.with_attrs(attrs);
            let client_spec = input.parse::<ClientSpec>()?;
            (client_spec, server_spec)
        } else {
            return Err(Error::new(input.span(), "Protocol must include both a 'client' and 'server' spec."));
        };
        Ok(ProtocolSpec {
            name,
            client_spec,
            server_spec
        })
    }
}
impl ToTokens for ProtocolSpec {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let name = &self.name;
        let client = &self.client_spec;
        let server = &self.server_spec;

        let service_ty_names: Vec<Ident> =
            vec![client.enum_ident(), server.enum_ident()];
        let service_mod_names: Vec<Ident> =
            vec![client.ident(), server.ident()];
        let proto_enum_variants: Vec<Ident> = 
            vec![
                Ident::new(&CLIENT.to_camel_case(), client.client_kwd.span),
                Ident::new(&SERVER.to_camel_case(), server.server_kwd.span),
            ];

        let client_mod = ServiceModuleSpec::new(
            client,
            AmethystSystemSpec::new(client, server)
        );
        let server_mod = ServiceModuleSpec::new(
            server,
            AmethystSystemSpec::new(server, client)
        );

        tokens.extend(quote! {
            #client_mod

            #server_mod
            
            #(impl std::convert::Into<#name> for #service_mod_names::#service_ty_names {
                fn into(self) -> #name {
                    #name::#proto_enum_variants(self)
                }
            })*

            #[derive(::std::fmt::Debug, ::std::clone::Clone, ::std::cmp::PartialEq)]
            pub enum #name {
                #(#proto_enum_variants(#service_mod_names::#service_ty_names)),*
            }
        })
    }
}

pub(crate) trait ServiceIdent<'a> {
    type MessageNameIter: Iterator<Item=&'a Ident>;

    fn ident(& self) -> Ident;
    fn camel_case_ident(&'a self) -> Ident;
    fn enum_ident(&'a self) -> Ident;
    fn message_ty_names(&'a self) -> Self::MessageNameIter;
}

const CLIENT: &str = "client";
struct ClientSpec {
    attrs: Vec<Attribute>,
    client_kwd: keyword::client,
    items: Vec<MessageOrImpl>,
}
impl ClientSpec {
    fn with_attrs(self, attrs: Vec<Attribute>) -> Self {
        Self {
            attrs,
            ..self
        }
    }
}
impl<'a> ServiceIdent<'a> for ClientSpec {
    type MessageNameIter = std::iter::Map<
        std::iter::FilterMap<std::slice::Iter<'a, MessageOrImpl>, 
        for<'r> fn(&'r MessageOrImpl) -> Option<&'r MessageSpec>>, 
        fn(&MessageSpec) -> &Ident>;

    fn message_ty_names(&'a self) -> Self::MessageNameIter {
        // required to coerce our fn item into a fn pointer
        let as_message: fn(&MessageOrImpl) -> Option<&MessageSpec> = MessageOrImpl::as_message;
        self
            .items
            .iter()
            .filter_map(as_message)
            .map(|msg| msg.ident())
    }

    fn ident(&self) -> Ident {
        Ident::new(CLIENT, self.client_kwd.span)
    }

    fn camel_case_ident(&self) -> Ident {
        Ident::new(&CLIENT.to_camel_case(), self.client_kwd.span)
    }

    fn enum_ident(&self) -> Ident {
        format_ident!("{}Protocol", self.camel_case_ident())
    }
}
impl Parse for ClientSpec {
    fn parse(input: ParseStream) -> Result<Self> {
        let attrs = input.call(Attribute::parse_outer)?;
        let client_kwd = input.parse::<keyword::client>()?;
        let content;
        braced!(content in input);
        let items = content.parse_many1(MessageOrImpl::parse)?;
        Ok(ClientSpec {
            attrs,
            client_kwd,
            items,
        })
    }
}
impl ToTokens for ClientSpec {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let enum_name = self.enum_ident();

        let message_names: Vec<&Ident> = self.message_ty_names().collect();
        let messages = &self.items;

        let attrs = &self.attrs;

        tokens.extend(quote! {
            #(#messages)*

            #(#attrs)*
            #[derive(::std::clone::Clone, ::std::fmt::Debug, ::std::cmp::PartialEq)]
            pub enum #enum_name {
                #(#message_names(#message_names)),*
            }

            #(impl ::std::convert::Into<#enum_name> for #message_names {
                fn into(self) -> #enum_name {
                    #enum_name::#message_names(self)
                }
            })*
        });
    }
}
struct ServiceModuleSpec<'a, S, R> {
    service: &'a S,
    amethyst_systems: AmethystSystemSpec<'a, S, R>
}
impl<'a, S, R> ServiceModuleSpec<'a, S, R> {
    fn new(service: &'a S, amethyst_systems: AmethystSystemSpec<'a, S, R>) -> Self {
        ServiceModuleSpec {
            service,
            amethyst_systems
        }
    }
}
impl<'a, S, R> ToTokens for ServiceModuleSpec<'a, S, R> 
where
    S: ToTokens + ServiceIdent<'a>,
    R: ServiceIdent<'a>
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let mod_name = self.service.ident();
        let service = self.service;
        let systems = &self.amethyst_systems;
        tokens.extend(quote! {
            pub mod #mod_name {
                use super::*;

                #service

                #systems
            }
        })
    }
}

const SERVER: &str = "server";
struct ServerSpec {
    attrs: Vec<Attribute>,
    server_kwd: keyword::server,
    items: Vec<MessageOrImpl>,
}
impl<'a> ServiceIdent<'a> for ServerSpec {
    fn ident(&self) -> Ident {
        Ident::new(SERVER, self.server_kwd.span)
    }

    fn camel_case_ident(&self) -> Ident {
        Ident::new(&SERVER.to_camel_case(), self.server_kwd.span)
    }

    fn enum_ident(&self) -> Ident {
        format_ident!("{}Protocol", self.camel_case_ident())
    }

    type MessageNameIter = std::iter::Map<
        std::iter::FilterMap<std::slice::Iter<'a, MessageOrImpl>, 
        for<'r> fn(&'r MessageOrImpl) -> Option<&'r MessageSpec>>, 
        fn(&MessageSpec) -> &Ident>;

    fn message_ty_names(&'a self) -> Self::MessageNameIter {
        // This is required to coerce our fn item into a fn pointer
        let as_message: fn(&MessageOrImpl) -> Option<&MessageSpec> = MessageOrImpl::as_message;
        self 
            .items
            .iter()
            .filter_map(as_message)
            .map((|msg| &msg.ident()) as fn(&MessageSpec) -> &Ident)
    }
}
impl ServerSpec {
    fn with_attrs(self, attrs: Vec<Attribute>) -> Self {
        Self {
            attrs,
            ..self
        }
    }
}
impl Parse for ServerSpec {
    fn parse(input: ParseStream) -> Result<Self> {
        let attrs = input.call(Attribute::parse_outer)?;
        let server_kwd= input.parse::<keyword::server>()?;
        let content;
        braced!(content in input);
        let items = content.parse_many1(MessageOrImpl::parse)?;
        Ok(ServerSpec {
            attrs,
            server_kwd,
            items,
        })
    }
}
impl ToTokens for ServerSpec {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let enum_name = self.enum_ident();

        let message_names: Vec<&Ident> = self.message_ty_names().collect();
        let messages = &self.items;

        let attrs = &self.attrs;

        tokens.extend(quote! {
            #(#messages)*

            #(#attrs)*
            #[derive(::std::clone::Clone, ::std::fmt::Debug, ::std::cmp::PartialEq)]
            pub enum #enum_name {
                #(#message_names(#message_names)),*
            }

            #(impl ::std::convert::Into<#enum_name> for #message_names {
                fn into(self) -> #enum_name {
                    #enum_name::#message_names(self)
                }
            })*
        });
    }
}


///// A service spec is a list of messages to be included in this service.
///// Any annotations applied to the service are applied to the enum generated for this service
//struct ServiceSpec {
//    attrs: Vec<Attribute>,
//    ident: Ident,
//    items: Vec<MessageOrImpl>,
//}
//impl ServiceSpec {
//    fn mod_ident(&self) -> Ident {
//        let spec_name = self.ident.to_string();
//        Ident::new(&spec_name.to_snake_case(), self.ident.span())
//    }
//
//    fn enum_ident(&self) -> Ident {
//        let spec_name = &self.ident.to_string();
//        format_ident!(
//            "{}Protocol",
//            spec_name.to_camel_case(),
//            span = self.ident.span()
//        )
//    }
//}
//impl Parse for ServiceSpec {
//    fn parse(input: ParseStream) -> Result<Self> {
//        let attrs = input.call(Attribute::parse_outer)?;
//        input.parse::<keyword::service>()?;
//        let ident = input.parse::<Ident>()?;
//        let content;
//        braced!(content in input);
//        let items = parse_many1(MessageOrImpl::parse, &content)?;
//        Ok(ServiceSpec {
//            attrs,
//            ident,
//            items,
//        })
//    }
//}
//impl ToTokens for ServiceSpec {
//    fn to_tokens(&self, tokens: &mut TokenStream) {
//        let mod_name = self.mod_ident();
//        let enum_name = self.enum_ident();
//
//        let message_names: Vec<&Ident> = self
//            .items
//            .iter()
//            .filter_map(MessageOrImpl::as_message)
//            .map(|msg| &msg.ident)
//            .collect();
//        let messages = &self.items;
//
//        let attrs = &self.attrs;
//
//        let systems = AmethystSystemSpec::new(&self, &message_names);
//
//        tokens.extend(quote! {
//            pub mod #mod_name {
//                use super::*;
//
//                #(#messages)*
//
//                #(#attrs)*
//                #[derive(::std::clone::Clone, ::std::fmt::Debug, ::std::cmp::PartialEq)]
//                pub enum #enum_name {
//                    #(#message_names(#message_names)),*
//                }
//
//                #(impl ::std::convert::Into<#enum_name> for #message_names {
//                    fn into(self) -> #enum_name {
//                        #enum_name::#message_names(self)
//                    }
//                })*
//
//                #systems
//            }
//        });
//    }
//}


trait ParseMany1 {
    fn parse_many1<T>(self, parse_fn: fn(_: ParseStream) -> Result<T>) -> Result<Vec<T>>;
}
impl<'a> ParseMany1 for ParseStream<'a> {
    fn parse_many1<T>(self, parse_fn: fn(_: ParseStream) -> Result<T>) -> Result<Vec<T>> {
        let first = self.call(parse_fn)?;
        let mut ts = vec![first];
        loop {
            if self.is_empty() {
                break;
            }
            ts.push(self.call(parse_fn)?);
        }
        Ok(ts)
    }
}


