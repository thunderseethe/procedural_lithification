use super::ServiceIdent;
use heck::{SnakeCase};
use proc_macro2::{Ident, TokenStream};
use quote::{format_ident, quote, quote_spanned, ToTokens};

/// Iterates over the value of message_tys formatted as camel case to be used as variable names
fn message_var_name_iter<'a, T: ServiceIdent<'a>>(service: &'a T, postfix: &'a str) -> impl Iterator<Item = Ident> + 'a {
    service.message_ty_names().map(move |msg| {
        format_ident!(
            "{}{}",
            msg.to_string().to_snake_case(),
            postfix,
            span = msg.span()
        )
    })
}
fn append_service_type_ident<'a, T: ServiceIdent<'a>>(service: &'a T, append: &str) -> Ident {
    format_ident!("{}{}", service.camel_case_ident(), append)
}

pub(crate) struct AmethystSystemSpec<'a, S, R> {
    sender_service: &'a S,
    receiver_service: &'a R,
}
impl<'a, S, R> AmethystSystemSpec<'a, S, R> 
where
    S: ServiceIdent<'a>,
    R: ServiceIdent<'a>,
{
    pub(crate) fn new(sender_service: &'a S, receiver_service: &'a R) -> Self {
        AmethystSystemSpec {
            sender_service,
            receiver_service
        }
    }

    fn sender_system(&self) -> TokenStream {
        let sender_name = append_service_type_ident(self.sender_service, "SenderSystem");
        let sender_desc = append_service_type_ident(self.sender_service, "SenderSystemDesc");
        let protocol_name = self.sender_service.enum_ident();

        let reader_id_names: Vec<Ident> = message_var_name_iter(self.sender_service, "_id").collect();
        // We want to ensure these are in the same order
        let channel_names: Vec<Ident> = message_var_name_iter(self.sender_service, "_channel").collect();
        let message_tys: Vec<&Ident> = self.sender_service.message_ty_names().collect();

        quote_spanned! {protocol_name.span()=>
            #[derive(Default)]
            pub struct #sender_desc;
            impl<'a, 'b> ::generate_protocol::amethyst_core::SystemDesc<'a, 'b, #sender_name> for #sender_desc {
                fn build(self, world: &mut ::generate_protocol::specs::World) -> #sender_name {
                    world.exec(|(#(mut #channel_names,)*): (#(::generate_protocol::specs::Write<'_, ::generate_protocol::specs::shrev::EventChannel<#message_tys>>,)*)| {
                        #sender_name {
                            #(#reader_id_names: #channel_names.register_reader(),)*
                        }
                    })
                }
            }

            pub struct #sender_name {
                #(#reader_id_names: ::generate_protocol::specs::shrev::ReaderId<#message_tys>,)*
            }
            impl<'a> ::generate_protocol::specs::System<'a> for #sender_name {
                type SystemData = (
                    #(::generate_protocol::specs::Read<'a, ::generate_protocol::specs::shrev::EventChannel<#message_tys>>,)*
                    ::generate_protocol::specs::Write<'a, ::generate_protocol::specs::shrev::EventChannel<#protocol_name>>,
                );

                fn run(&mut self, (#(#channel_names,)* mut protocol_channel): Self::SystemData) {
                    #[cfg(thread_profiler)]
                    profile_scope!("#sender_name");

                    #(
                        protocol_channel.iter_write(
                        #channel_names.read(&mut self.#reader_id_names).cloned().map(#message_tys::into));
                    )*
                }
            }
        }
    }

    /// Create a stream of tokesn representing the receiver system. The system is produced is named after `sender_service` so that the system names are consistent but it 
    fn receiver_system(&self) -> TokenStream {
        let receiver_namespace = self.receiver_service.ident();
        let receiver_name = append_service_type_ident(self.sender_service, "ReceiverSystem");
        let receiver_desc = append_service_type_ident(self.sender_service, "ReceiverSystemDesc");
        let protocol_name = self.receiver_service.enum_ident();
        let snake_case_proto = protocol_name.to_string().to_snake_case();
        let proto_channel_name =
            format_ident!("{}_channel", snake_case_proto, span = protocol_name.span());
        let reader_id_name = format_ident!(
            "{}_reader_id",
            snake_case_proto,
            span = protocol_name.span()
        );
        let channel_names: Vec<Ident> = message_var_name_iter(self.receiver_service, "_channel").collect();
        let message_tys: Vec<&Ident> = self.receiver_service.message_ty_names().collect();

        quote! {
            use super::#receiver_namespace::#protocol_name;

            #[derive(Default)]
            pub struct #receiver_desc;
            impl<'a, 'b> ::generate_protocol::amethyst_core::SystemDesc<'a, 'b, #receiver_name> for #receiver_desc {
                fn build(self, world: &mut ::generate_protocol::specs::World) -> #receiver_name {
                    world.exec(|mut #proto_channel_name: ::generate_protocol::specs::Write<'_, ::generate_protocol::specs::shrev::EventChannel<#receiver_namespace::#protocol_name>>| {
                       #receiver_name {
                           #reader_id_name: #proto_channel_name.register_reader(),
                       }
                    })
                }
            }

            pub struct #receiver_name {
                #reader_id_name: ::generate_protocol::specs::shrev::ReaderId<#protocol_name>
            }
            impl<'a> ::generate_protocol::specs::System<'a> for #receiver_name {
                type SystemData = (
                    ::generate_protocol::specs::Read<'a, ::generate_protocol::specs::shrev::EventChannel<#receiver_namespace::#protocol_name>>,
                    #(::generate_protocol::specs::Write<'a, ::generate_protocol::specs::shrev::EventChannel<#receiver_namespace::#message_tys>>,)*
                );

                fn run(&mut self, (protocol_channel, #(mut #channel_names,)*): Self::SystemData) {
                    #[cfg(thread_profiler)]
                    profile_scope!("#reciver_name");

                    for evt in protocol_channel.read(&mut self.#reader_id_name) {
                        match evt {
                            #(
                                #receiver_namespace::#protocol_name::#message_tys(msg) => #channel_names.single_write(msg.clone()),
                            )*
                        }
                    }
                }
            }
        }
    }
}
impl<'a, S, R> ToTokens for AmethystSystemSpec<'a, S, R> 
where
    S: ServiceIdent<'a>,
    R: ServiceIdent<'a>,
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let sender_system = self.sender_system();
        let receiver_system = self.receiver_system();

        tokens.extend(quote! {
            #sender_system

            #receiver_system
        });
    }
}

pub(crate) struct ServerAmethystSystemSpec<'a, S, R> {
    server_service: &'a S,
    client_service: &'a R,
}
impl<'a, S, R> ServerAmethystSystemSpec<'a, S, R> 
where
    S: ServiceIdent<'a>,
    R: ServiceIdent<'a>,
{
    pub(crate) fn _new(server_service: &'a S, client_service: &'a R) -> Self {
        Self {
            server_service,
            client_service
        }
    }

    fn sender_system(&self) -> TokenStream {
        let sender_name = append_service_type_ident(self.server_service, "SenderSystem");
        let sender_desc = append_service_type_ident(self.server_service, "SenderSystemDesc");
        let protocol_name = self.server_service.enum_ident();

        let reader_id_names: Vec<Ident> = message_var_name_iter(self.server_service, "_id").collect();
        // We want to ensure these are in the same order
        let channel_names: Vec<Ident> = message_var_name_iter(self.server_service, "_channel").collect();
        let message_tys: Vec<&Ident> = self.server_service.message_ty_names().collect();

        quote_spanned! {protocol_name.span()=>
            #[derive(Default)]
            pub struct #sender_desc;
            impl<'a, 'b> ::generate_protocol::amethyst_core::SystemDesc<'a, 'b, #sender_name> for #sender_desc {
                fn build(self, world: &mut ::generate_protocol::specs::World) -> #sender_name {
                    world.exec(|(#(mut #channel_names,)*): (#(::generate_protocol::specs::Write<'_, ::generate_protocol::specs::shrev::EventChannel<#message_tys>>,)*)| {
                        #sender_name {
                            #(#reader_id_names: #channel_names.register_reader(),)*
                        }
                    })
                }
            }

            pub struct #sender_name {
                #(#reader_id_names: ::generate_protocol::specs::shrev::ReaderId<#message_tys>,)*
            }
            impl<'a> ::generate_protocol::specs::System<'a> for #sender_name {
                type SystemData = (
                    #(::generate_protocol::specs::Read<'a, ::generate_protocol::specs::shrev::EventChannel<#message_tys>>,)*
                    ::generate_protocol::specs::Write<'a, ::generate_protocol::specs::shrev::EventChannel<#protocol_name>>,
                );

                fn run(&mut self, (#(#channel_names,)* mut protocol_channel): Self::SystemData) {
                    #[cfg(thread_profiler)]
                    profile_scope!("#sender_name");

                    #(
                        protocol_channel.iter_write(
                        #channel_names.read(&mut self.#reader_id_names).cloned().map(#message_tys::into));
                    )*
                }
            }
        }
    }

    /// Create a stream of tokesn representing the receiver system. The system is produced is named after `sender_service` so that the system names are consistent but it 
    fn receiver_system(&self) -> TokenStream {
        let receiver_namespace = self.client_service.ident();
        let receiver_name = append_service_type_ident(self.client_service, "ReceiverSystem");
        let receiver_desc = append_service_type_ident(self.client_service, "ReceiverSystemDesc");
        let protocol_name = self.client_service.enum_ident();
        let snake_case_proto = protocol_name.to_string().to_snake_case();
        let proto_channel_name =
            format_ident!("{}_channel", snake_case_proto, span = protocol_name.span());
        let reader_id_name = format_ident!(
            "{}_reader_id",
            snake_case_proto,
            span = protocol_name.span()
        );
        let channel_names: Vec<Ident> = message_var_name_iter(self.client_service, "_channel").collect();
        let message_tys: Vec<&Ident> = self.client_service.message_ty_names().collect();

        quote! {
            use super::#receiver_namespace::#protocol_name;

            #[derive(Default)]
            pub struct #receiver_desc;
            impl<'a, 'b> ::generate_protocol::amethyst_core::SystemDesc<'a, 'b, #receiver_name> for #receiver_desc {
                fn build(self, world: &mut ::generate_protocol::specs::World) -> #receiver_name {
                    world.exec(|mut #proto_channel_name: ::generate_protocol::specs::Write<'_, ::generate_protocol::specs::shrev::EventChannel<#receiver_namespace::#protocol_name>>| {
                       #receiver_name {
                           #reader_id_name: #proto_channel_name.register_reader(),
                       }
                    })
                }
            }

            pub struct #receiver_name {
                #reader_id_name: ::generate_protocol::specs::shrev::ReaderId<#protocol_name>
            }
            impl<'a> ::generate_protocol::specs::System<'a> for #receiver_name {
                type SystemData = (
                    ::generate_protocol::specs::Read<'a, ::generate_protocol::specs::shrev::EventChannel<#receiver_namespace::#protocol_name>>,
                    #(::generate_protocol::specs::Write<'a, ::generate_protocol::specs::shrev::EventChannel<#receiver_namespace::#message_tys>>,)*
                );

                fn run(&mut self, (protocol_channel, #(mut #channel_names,)*): Self::SystemData) {
                    #[cfg(thread_profiler)]
                    profile_scope!("#reciver_name");

                    for evt in protocol_channel.read(&mut self.#reader_id_name) {
                        match evt {
                            #(
                                #receiver_namespace::#protocol_name::#message_tys(msg) => #channel_names.single_write(msg.clone()),
                            )*
                        }
                    }
                }
            }
        }
    }
}
impl<'a, S, R> ToTokens for ServerAmethystSystemSpec<'a, S, R> 
where
    S: ServiceIdent<'a>,
    R: ServiceIdent<'a>,
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let sender_system = self.sender_system();
        let receiver_system = self.receiver_system();

        tokens.extend(quote! {
            #sender_system

            #receiver_system
        });
    }
}