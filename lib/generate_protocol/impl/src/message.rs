use proc_macro2::{Ident, TokenStream};
use quote::{quote_spanned, ToTokens};
use syn;
use syn::{
    parse::*, parse_quote, Attribute, Field, Fields,
    ItemImpl, Token, ItemStruct
};
/// A message is a struct that must be a bag of simple data
/// All of it's fields are public and it does not support generics or lifetimes.
/// Lifetimes are not allowed because the data must be cloneable to pass across a network.
/// Generics are not allowed because I haven't figured out how to make them work with the enum.
//TODO: Try to figure out how to support generics while excluding lifetimes
pub struct MessageSpec(ItemStruct);
impl MessageSpec {
    fn new(s: ItemStruct) -> Self {
        MessageSpec(s)
    }

    fn with_attrs(self, attrs: Vec<Attribute>) -> Self {
        MessageSpec(ItemStruct {
            attrs,
            ..self.0
        })
    }

    pub fn ident(&self) -> &Ident {
        &self.0.ident
    }
}
impl Parse for MessageSpec {
    fn parse(input: ParseStream) -> Result<Self> {
        ItemStruct::parse(input).map(MessageSpec::new)
    }
}
impl ToTokens for MessageSpec {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let fields = self.0.fields.map(|field| Field {
            vis: parse_quote!(pub),
            ..field.clone()
        });

        let struckt = ItemStruct {
            vis: parse_quote!(pub),
            fields,
            ..self.0.clone()
        };

        tokens.extend(quote_spanned!(self.0.ident.span()=>
            #[derive(::std::clone::Clone, ::std::fmt::Debug, ::std::cmp::PartialEq)]
            #struckt 
        ))
    }
}

pub enum MessageOrImpl {
    Message(MessageSpec),
    Impl(ItemImpl),
}
impl MessageOrImpl {
    pub fn as_message(&self) -> Option<&MessageSpec> {
        use MessageOrImpl::*;

        match self {
            Message(ref spec) => Some(spec),
            _ => None,
        }
    }
}
impl Parse for MessageOrImpl {
    fn parse(input: ParseStream) -> Result<Self> {
        use MessageOrImpl::*;

        let attrs = input.call(Attribute::parse_outer)?;
        let lookahead = input.lookahead1();
        if lookahead.peek(Token![struct]) {
            input
                .parse()
                .map(|message: MessageSpec| message.with_attrs(attrs))
                .map(Message)
        } else if lookahead.peek(Token![impl]) {
            input
                .parse()
                .map(|impl_item| ItemImpl { attrs, ..impl_item })
                .map(Impl)
        } else {
            Err(lookahead.error())
        }
    }
}
impl ToTokens for MessageOrImpl {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        use MessageOrImpl::*;

        match self {
            Message(message_spec) => tokens.extend(message_spec.to_token_stream()),
            Impl(item_impl) => tokens.extend(item_impl.to_token_stream()),
        }
    }
}

// This actually isn't a true functor, which is not ideal
trait FunctorRef {
    type In;
    type Out;
    fn map<F>(&self, map_fn: F) -> Self where F: Fn(&Self::In) -> Self::Out;
}

// Map over a Fields transforming each field that is reached a rewrappring as the same Fields
impl FunctorRef for Fields {
    type In = Field;
    type Out = Field;

    fn map<F>(&self, map_fn: F) -> Self 
    where
        F: FnMut(&Self::In) -> Self::Out
    {
        use syn::{FieldsNamed, FieldsUnnamed};
        match self {
            Fields::Unit => Fields::Unit,
            Fields::Named(fields_named) => FieldsNamed {
                named: fields_named.named.iter().map(map_fn).collect(),
                ..fields_named.clone()
            }
            .into(),
            Fields::Unnamed(fields_unnamed) => FieldsUnnamed {
                unnamed: fields_unnamed.unnamed.iter().map(map_fn).collect(),
                ..fields_unnamed.clone()
            }
            .into(),
        }
    }
}