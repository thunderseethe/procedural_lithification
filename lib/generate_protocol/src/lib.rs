pub extern crate amethyst_core;
extern crate generate_protocol_macro;
pub extern crate specs;
extern crate self as generate_protocol;

pub use generate_protocol_macro::generate_protocol;
