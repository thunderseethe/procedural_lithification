#![feature(prelude_import)]
#[prelude_import]
use std::prelude::v1::*;
#[macro_use]
extern crate std;
use generate_protocol::generate_protocol;
pub mod client {
    use super::*;
    pub struct PlayerName(pub String);
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::clone::Clone for PlayerName {
        #[inline]
        fn clone(&self) -> PlayerName {
            match *self {
                PlayerName(ref __self_0_0) => {
                    PlayerName(::core::clone::Clone::clone(&(*__self_0_0)))
                }
            }
        }
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::fmt::Debug for PlayerName {
        fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
            match *self {
                PlayerName(ref __self_0_0) => {
                    let mut debug_trait_builder = f.debug_tuple("PlayerName");
                    let _ = debug_trait_builder.field(&&(*__self_0_0));
                    debug_trait_builder.finish()
                }
            }
        }
    }
    impl ::core::marker::StructuralPartialEq for PlayerName {}
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::cmp::PartialEq for PlayerName {
        #[inline]
        fn eq(&self, other: &PlayerName) -> bool {
            match *other {
                PlayerName(ref __self_1_0) => match *self {
                    PlayerName(ref __self_0_0) => (*__self_0_0) == (*__self_1_0),
                },
            }
        }
        #[inline]
        fn ne(&self, other: &PlayerName) -> bool {
            match *other {
                PlayerName(ref __self_1_0) => match *self {
                    PlayerName(ref __self_0_0) => (*__self_0_0) != (*__self_1_0),
                },
            }
        }
    }
    pub struct PlayerAction(pub String);
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::clone::Clone for PlayerAction {
        #[inline]
        fn clone(&self) -> PlayerAction {
            match *self {
                PlayerAction(ref __self_0_0) => {
                    PlayerAction(::core::clone::Clone::clone(&(*__self_0_0)))
                }
            }
        }
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::fmt::Debug for PlayerAction {
        fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
            match *self {
                PlayerAction(ref __self_0_0) => {
                    let mut debug_trait_builder = f.debug_tuple("PlayerAction");
                    let _ = debug_trait_builder.field(&&(*__self_0_0));
                    debug_trait_builder.finish()
                }
            }
        }
    }
    impl ::core::marker::StructuralPartialEq for PlayerAction {}
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::cmp::PartialEq for PlayerAction {
        #[inline]
        fn eq(&self, other: &PlayerAction) -> bool {
            match *other {
                PlayerAction(ref __self_1_0) => match *self {
                    PlayerAction(ref __self_0_0) => (*__self_0_0) == (*__self_1_0),
                },
            }
        }
        #[inline]
        fn ne(&self, other: &PlayerAction) -> bool {
            match *other {
                PlayerAction(ref __self_1_0) => match *self {
                    PlayerAction(ref __self_0_0) => (*__self_0_0) != (*__self_1_0),
                },
            }
        }
    }
    pub enum ClientProtocol {
        PlayerName(PlayerName),
        PlayerAction(PlayerAction),
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::clone::Clone for ClientProtocol {
        #[inline]
        fn clone(&self) -> ClientProtocol {
            match (&*self,) {
                (&ClientProtocol::PlayerName(ref __self_0),) => {
                    ClientProtocol::PlayerName(::core::clone::Clone::clone(&(*__self_0)))
                }
                (&ClientProtocol::PlayerAction(ref __self_0),) => {
                    ClientProtocol::PlayerAction(::core::clone::Clone::clone(&(*__self_0)))
                }
            }
        }
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::fmt::Debug for ClientProtocol {
        fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
            match (&*self,) {
                (&ClientProtocol::PlayerName(ref __self_0),) => {
                    let mut debug_trait_builder = f.debug_tuple("PlayerName");
                    let _ = debug_trait_builder.field(&&(*__self_0));
                    debug_trait_builder.finish()
                }
                (&ClientProtocol::PlayerAction(ref __self_0),) => {
                    let mut debug_trait_builder = f.debug_tuple("PlayerAction");
                    let _ = debug_trait_builder.field(&&(*__self_0));
                    debug_trait_builder.finish()
                }
            }
        }
    }
    impl ::core::marker::StructuralPartialEq for ClientProtocol {}
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::cmp::PartialEq for ClientProtocol {
        #[inline]
        fn eq(&self, other: &ClientProtocol) -> bool {
            {
                let __self_vi = unsafe { ::core::intrinsics::discriminant_value(&*self) } as isize;
                let __arg_1_vi =
                    unsafe { ::core::intrinsics::discriminant_value(&*other) } as isize;
                if true && __self_vi == __arg_1_vi {
                    match (&*self, &*other) {
                        (
                            &ClientProtocol::PlayerName(ref __self_0),
                            &ClientProtocol::PlayerName(ref __arg_1_0),
                        ) => (*__self_0) == (*__arg_1_0),
                        (
                            &ClientProtocol::PlayerAction(ref __self_0),
                            &ClientProtocol::PlayerAction(ref __arg_1_0),
                        ) => (*__self_0) == (*__arg_1_0),
                        _ => unsafe { ::core::intrinsics::unreachable() },
                    }
                } else {
                    false
                }
            }
        }
        #[inline]
        fn ne(&self, other: &ClientProtocol) -> bool {
            {
                let __self_vi = unsafe { ::core::intrinsics::discriminant_value(&*self) } as isize;
                let __arg_1_vi =
                    unsafe { ::core::intrinsics::discriminant_value(&*other) } as isize;
                if true && __self_vi == __arg_1_vi {
                    match (&*self, &*other) {
                        (
                            &ClientProtocol::PlayerName(ref __self_0),
                            &ClientProtocol::PlayerName(ref __arg_1_0),
                        ) => (*__self_0) != (*__arg_1_0),
                        (
                            &ClientProtocol::PlayerAction(ref __self_0),
                            &ClientProtocol::PlayerAction(ref __arg_1_0),
                        ) => (*__self_0) != (*__arg_1_0),
                        _ => unsafe { ::core::intrinsics::unreachable() },
                    }
                } else {
                    true
                }
            }
        }
    }
    impl ::std::convert::Into<ClientProtocol> for PlayerName {
        fn into(self) -> ClientProtocol {
            ClientProtocol::PlayerName(self)
        }
    }
    impl ::std::convert::Into<ClientProtocol> for PlayerAction {
        fn into(self) -> ClientProtocol {
            ClientProtocol::PlayerAction(self)
        }
    }
    pub struct ClientSenderSystemDesc;
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::default::Default for ClientSenderSystemDesc {
        #[inline]
        fn default() -> ClientSenderSystemDesc {
            ClientSenderSystemDesc {}
        }
    }
    impl<'a, 'b> ::generate_protocol::amethyst_core::SystemDesc<'a, 'b, ClientSenderSystem>
        for ClientSenderSystemDesc
    {
        fn build(self, world: &mut ::generate_protocol::specs::World) -> ClientSenderSystem {
            world.exec(
                |(mut player_name_channel, mut player_action_channel): (
                    ::generate_protocol::specs::Write<
                        '_,
                        ::generate_protocol::specs::shrev::EventChannel<PlayerName>,
                    >,
                    ::generate_protocol::specs::Write<
                        '_,
                        ::generate_protocol::specs::shrev::EventChannel<PlayerAction>,
                    >,
                )| {
                    ClientSenderSystem {
                        player_name_id: player_name_channel.register_reader(),
                        player_action_id: player_action_channel.register_reader(),
                    }
                },
            )
        }
    }
    pub struct ClientSenderSystem {
        player_name_id: ::generate_protocol::specs::shrev::ReaderId<PlayerName>,
        player_action_id: ::generate_protocol::specs::shrev::ReaderId<PlayerAction>,
    }
    impl<'a> ::generate_protocol::specs::System<'a> for ClientSenderSystem {
        type SystemData = (
            ::generate_protocol::specs::Read<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<PlayerName>,
            >,
            ::generate_protocol::specs::Read<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<PlayerAction>,
            >,
            ::generate_protocol::specs::Write<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<ClientProtocol>,
            >,
        );
        fn run(
            &mut self,
            (player_name_channel, player_action_channel, mut protocol_channel): Self::SystemData,
        ) {
            protocol_channel.iter_write(
                player_name_channel
                    .read(&mut self.player_name_id)
                    .cloned()
                    .map(PlayerName::into),
            );
            protocol_channel.iter_write(
                player_action_channel
                    .read(&mut self.player_action_id)
                    .cloned()
                    .map(PlayerAction::into),
            );
        }
    }
    pub struct ClientReceiverSystemDesc;
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::default::Default for ClientReceiverSystemDesc {
        #[inline]
        fn default() -> ClientReceiverSystemDesc {
            ClientReceiverSystemDesc {}
        }
    }
    impl<'a, 'b> ::generate_protocol::amethyst_core::SystemDesc<'a, 'b, ClientReceiverSystem>
        for ClientReceiverSystemDesc
    {
        fn build(self, world: &mut ::generate_protocol::specs::World) -> ClientReceiverSystem {
            world.exec(
                |mut server_protocol_channel: ::generate_protocol::specs::Write<
                    '_,
                    ::generate_protocol::specs::shrev::EventChannel<server::ServerProtocol>,
                >| {
                    ClientReceiverSystem {
                        server_protocol_reader_id: server_protocol_channel.register_reader(),
                    }
                },
            )
        }
    }
    pub struct ClientReceiverSystem {
        server_protocol_reader_id: ::generate_protocol::specs::shrev::ReaderId<ServerProtocol>,
    }
    impl<'a> ::generate_protocol::specs::System<'a> for ClientReceiverSystem {
        type SystemData = (
            ::generate_protocol::specs::Read<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<server::ServerProtocol>,
            >,
            ::generate_protocol::specs::Write<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<server::PlayerPosSync>,
            >,
            ::generate_protocol::specs::Write<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<server::ChunkAt>,
            >,
        );
        fn run(
            &mut self,
            (protocol_channel, mut player_pos_sync_channel, mut chunk_at_channel): Self::SystemData,
        ) {
            for evt in protocol_channel.read(&mut self.server_protocol_reader_id) {
                match evt {
                    server::ServerProtocol::PlayerPosSync(msg) => {
                        player_pos_sync_channel.single_write(msg.clone())
                    }
                    server::ServerProtocol::ChunkAt(msg) => {
                        chunk_at_channel.single_write(msg.clone())
                    }
                }
            }
        }
    }
}
pub mod server {
    use super::*;
    pub struct PlayerPosSync {
        pub x: f32,
        pub y: f32,
        pub z: f32,
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::clone::Clone for PlayerPosSync {
        #[inline]
        fn clone(&self) -> PlayerPosSync {
            match *self {
                PlayerPosSync {
                    x: ref __self_0_0,
                    y: ref __self_0_1,
                    z: ref __self_0_2,
                } => PlayerPosSync {
                    x: ::core::clone::Clone::clone(&(*__self_0_0)),
                    y: ::core::clone::Clone::clone(&(*__self_0_1)),
                    z: ::core::clone::Clone::clone(&(*__self_0_2)),
                },
            }
        }
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::fmt::Debug for PlayerPosSync {
        fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
            match *self {
                PlayerPosSync {
                    x: ref __self_0_0,
                    y: ref __self_0_1,
                    z: ref __self_0_2,
                } => {
                    let mut debug_trait_builder = f.debug_struct("PlayerPosSync");
                    let _ = debug_trait_builder.field("x", &&(*__self_0_0));
                    let _ = debug_trait_builder.field("y", &&(*__self_0_1));
                    let _ = debug_trait_builder.field("z", &&(*__self_0_2));
                    debug_trait_builder.finish()
                }
            }
        }
    }
    impl ::core::marker::StructuralPartialEq for PlayerPosSync {}
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::cmp::PartialEq for PlayerPosSync {
        #[inline]
        fn eq(&self, other: &PlayerPosSync) -> bool {
            match *other {
                PlayerPosSync {
                    x: ref __self_1_0,
                    y: ref __self_1_1,
                    z: ref __self_1_2,
                } => match *self {
                    PlayerPosSync {
                        x: ref __self_0_0,
                        y: ref __self_0_1,
                        z: ref __self_0_2,
                    } => {
                        (*__self_0_0) == (*__self_1_0)
                            && (*__self_0_1) == (*__self_1_1)
                            && (*__self_0_2) == (*__self_1_2)
                    }
                },
            }
        }
        #[inline]
        fn ne(&self, other: &PlayerPosSync) -> bool {
            match *other {
                PlayerPosSync {
                    x: ref __self_1_0,
                    y: ref __self_1_1,
                    z: ref __self_1_2,
                } => match *self {
                    PlayerPosSync {
                        x: ref __self_0_0,
                        y: ref __self_0_1,
                        z: ref __self_0_2,
                    } => {
                        (*__self_0_0) != (*__self_1_0)
                            || (*__self_0_1) != (*__self_1_1)
                            || (*__self_0_2) != (*__self_1_2)
                    }
                },
            }
        }
    }
    impl PlayerPosSync {
        pub fn new(x: f32, y: f32, z: f32) -> Self {
            PlayerPosSync { x, y, z }
        }
    }
    pub struct ChunkAt {
        pub pos: [i32; 3],
        pub data: Vec<u8>,
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::clone::Clone for ChunkAt {
        #[inline]
        fn clone(&self) -> ChunkAt {
            match *self {
                ChunkAt {
                    pos: ref __self_0_0,
                    data: ref __self_0_1,
                } => ChunkAt {
                    pos: ::core::clone::Clone::clone(&(*__self_0_0)),
                    data: ::core::clone::Clone::clone(&(*__self_0_1)),
                },
            }
        }
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::fmt::Debug for ChunkAt {
        fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
            match *self {
                ChunkAt {
                    pos: ref __self_0_0,
                    data: ref __self_0_1,
                } => {
                    let mut debug_trait_builder = f.debug_struct("ChunkAt");
                    let _ = debug_trait_builder.field("pos", &&(*__self_0_0));
                    let _ = debug_trait_builder.field("data", &&(*__self_0_1));
                    debug_trait_builder.finish()
                }
            }
        }
    }
    impl ::core::marker::StructuralPartialEq for ChunkAt {}
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::cmp::PartialEq for ChunkAt {
        #[inline]
        fn eq(&self, other: &ChunkAt) -> bool {
            match *other {
                ChunkAt {
                    pos: ref __self_1_0,
                    data: ref __self_1_1,
                } => match *self {
                    ChunkAt {
                        pos: ref __self_0_0,
                        data: ref __self_0_1,
                    } => (*__self_0_0) == (*__self_1_0) && (*__self_0_1) == (*__self_1_1),
                },
            }
        }
        #[inline]
        fn ne(&self, other: &ChunkAt) -> bool {
            match *other {
                ChunkAt {
                    pos: ref __self_1_0,
                    data: ref __self_1_1,
                } => match *self {
                    ChunkAt {
                        pos: ref __self_0_0,
                        data: ref __self_0_1,
                    } => (*__self_0_0) != (*__self_1_0) || (*__self_0_1) != (*__self_1_1),
                },
            }
        }
    }
    impl ChunkAt {
        pub fn new(pos: [i32; 3], data: Vec<u8>) -> Self {
            ChunkAt { pos, data }
        }
    }
    pub enum ServerProtocol {
        PlayerPosSync(PlayerPosSync),
        ChunkAt(ChunkAt),
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::clone::Clone for ServerProtocol {
        #[inline]
        fn clone(&self) -> ServerProtocol {
            match (&*self,) {
                (&ServerProtocol::PlayerPosSync(ref __self_0),) => {
                    ServerProtocol::PlayerPosSync(::core::clone::Clone::clone(&(*__self_0)))
                }
                (&ServerProtocol::ChunkAt(ref __self_0),) => {
                    ServerProtocol::ChunkAt(::core::clone::Clone::clone(&(*__self_0)))
                }
            }
        }
    }
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::fmt::Debug for ServerProtocol {
        fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
            match (&*self,) {
                (&ServerProtocol::PlayerPosSync(ref __self_0),) => {
                    let mut debug_trait_builder = f.debug_tuple("PlayerPosSync");
                    let _ = debug_trait_builder.field(&&(*__self_0));
                    debug_trait_builder.finish()
                }
                (&ServerProtocol::ChunkAt(ref __self_0),) => {
                    let mut debug_trait_builder = f.debug_tuple("ChunkAt");
                    let _ = debug_trait_builder.field(&&(*__self_0));
                    debug_trait_builder.finish()
                }
            }
        }
    }
    impl ::core::marker::StructuralPartialEq for ServerProtocol {}
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::cmp::PartialEq for ServerProtocol {
        #[inline]
        fn eq(&self, other: &ServerProtocol) -> bool {
            {
                let __self_vi = unsafe { ::core::intrinsics::discriminant_value(&*self) } as isize;
                let __arg_1_vi =
                    unsafe { ::core::intrinsics::discriminant_value(&*other) } as isize;
                if true && __self_vi == __arg_1_vi {
                    match (&*self, &*other) {
                        (
                            &ServerProtocol::PlayerPosSync(ref __self_0),
                            &ServerProtocol::PlayerPosSync(ref __arg_1_0),
                        ) => (*__self_0) == (*__arg_1_0),
                        (
                            &ServerProtocol::ChunkAt(ref __self_0),
                            &ServerProtocol::ChunkAt(ref __arg_1_0),
                        ) => (*__self_0) == (*__arg_1_0),
                        _ => unsafe { ::core::intrinsics::unreachable() },
                    }
                } else {
                    false
                }
            }
        }
        #[inline]
        fn ne(&self, other: &ServerProtocol) -> bool {
            {
                let __self_vi = unsafe { ::core::intrinsics::discriminant_value(&*self) } as isize;
                let __arg_1_vi =
                    unsafe { ::core::intrinsics::discriminant_value(&*other) } as isize;
                if true && __self_vi == __arg_1_vi {
                    match (&*self, &*other) {
                        (
                            &ServerProtocol::PlayerPosSync(ref __self_0),
                            &ServerProtocol::PlayerPosSync(ref __arg_1_0),
                        ) => (*__self_0) != (*__arg_1_0),
                        (
                            &ServerProtocol::ChunkAt(ref __self_0),
                            &ServerProtocol::ChunkAt(ref __arg_1_0),
                        ) => (*__self_0) != (*__arg_1_0),
                        _ => unsafe { ::core::intrinsics::unreachable() },
                    }
                } else {
                    true
                }
            }
        }
    }
    impl ::std::convert::Into<ServerProtocol> for PlayerPosSync {
        fn into(self) -> ServerProtocol {
            ServerProtocol::PlayerPosSync(self)
        }
    }
    impl ::std::convert::Into<ServerProtocol> for ChunkAt {
        fn into(self) -> ServerProtocol {
            ServerProtocol::ChunkAt(self)
        }
    }
    pub struct ServerSenderSystemDesc;
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::default::Default for ServerSenderSystemDesc {
        #[inline]
        fn default() -> ServerSenderSystemDesc {
            ServerSenderSystemDesc {}
        }
    }
    impl<'a, 'b> ::generate_protocol::amethyst_core::SystemDesc<'a, 'b, ServerSenderSystem>
        for ServerSenderSystemDesc
    {
        fn build(self, world: &mut ::generate_protocol::specs::World) -> ServerSenderSystem {
            world.exec(
                |(mut player_pos_sync_channel, mut chunk_at_channel): (
                    ::generate_protocol::specs::Write<
                        '_,
                        ::generate_protocol::specs::shrev::EventChannel<PlayerPosSync>,
                    >,
                    ::generate_protocol::specs::Write<
                        '_,
                        ::generate_protocol::specs::shrev::EventChannel<ChunkAt>,
                    >,
                )| {
                    ServerSenderSystem {
                        player_pos_sync_id: player_pos_sync_channel.register_reader(),
                        chunk_at_id: chunk_at_channel.register_reader(),
                    }
                },
            )
        }
    }
    pub struct ServerSenderSystem {
        player_pos_sync_id: ::generate_protocol::specs::shrev::ReaderId<PlayerPosSync>,
        chunk_at_id: ::generate_protocol::specs::shrev::ReaderId<ChunkAt>,
    }
    impl<'a> ::generate_protocol::specs::System<'a> for ServerSenderSystem {
        type SystemData = (
            ::generate_protocol::specs::Read<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<PlayerPosSync>,
            >,
            ::generate_protocol::specs::Read<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<ChunkAt>,
            >,
            ::generate_protocol::specs::Write<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<ServerProtocol>,
            >,
        );
        fn run(
            &mut self,
            (player_pos_sync_channel, chunk_at_channel, mut protocol_channel): Self::SystemData,
        ) {
            protocol_channel.iter_write(
                player_pos_sync_channel
                    .read(&mut self.player_pos_sync_id)
                    .cloned()
                    .map(PlayerPosSync::into),
            );
            protocol_channel.iter_write(
                chunk_at_channel
                    .read(&mut self.chunk_at_id)
                    .cloned()
                    .map(ChunkAt::into),
            );
        }
    }
    pub struct ServerReceiverSystemDesc;
    #[automatically_derived]
    #[allow(unused_qualifications)]
    impl ::core::default::Default for ServerReceiverSystemDesc {
        #[inline]
        fn default() -> ServerReceiverSystemDesc {
            ServerReceiverSystemDesc {}
        }
    }
    impl<'a, 'b> ::generate_protocol::amethyst_core::SystemDesc<'a, 'b, ServerReceiverSystem>
        for ServerReceiverSystemDesc
    {
        fn build(self, world: &mut ::generate_protocol::specs::World) -> ServerReceiverSystem {
            world.exec(
                |mut client_protocol_channel: ::generate_protocol::specs::Write<
                    '_,
                    ::generate_protocol::specs::shrev::EventChannel<client::ClientProtocol>,
                >| {
                    ServerReceiverSystem {
                        client_protocol_reader_id: client_protocol_channel.register_reader(),
                    }
                },
            )
        }
    }
    pub struct ServerReceiverSystem {
        client_protocol_reader_id: ::generate_protocol::specs::shrev::ReaderId<ClientProtocol>,
    }
    impl<'a> ::generate_protocol::specs::System<'a> for ServerReceiverSystem {
        type SystemData = (
            ::generate_protocol::specs::Read<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<client::ClientProtocol>,
            >,
            ::generate_protocol::specs::Write<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<client::PlayerName>,
            >,
            ::generate_protocol::specs::Write<
                'a,
                ::generate_protocol::specs::shrev::EventChannel<client::PlayerAction>,
            >,
        );
        fn run(
            &mut self,
            ( protocol_channel , mut player_name_channel , mut player_action_channel ) : Self :: SystemData,
        ) {
            for evt in protocol_channel.read(&mut self.client_protocol_reader_id) {
                match evt {
                    client::ClientProtocol::PlayerName(msg) => {
                        player_name_channel.single_write(msg.clone())
                    }
                    client::ClientProtocol::PlayerAction(msg) => {
                        player_action_channel.single_write(msg.clone())
                    }
                }
            }
        }
    }
}
impl std::convert::Into<NetworkProtocol> for client::ClientProtocol {
    fn into(self) -> NetworkProtocol {
        NetworkProtocol::Client(self)
    }
}
impl std::convert::Into<NetworkProtocol> for server::ServerProtocol {
    fn into(self) -> NetworkProtocol {
        NetworkProtocol::Server(self)
    }
}
pub enum NetworkProtocol {
    Client(client::ClientProtocol),
    Server(server::ServerProtocol),
}
#[automatically_derived]
#[allow(unused_qualifications)]
impl ::core::fmt::Debug for NetworkProtocol {
    fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
        match (&*self,) {
            (&NetworkProtocol::Client(ref __self_0),) => {
                let mut debug_trait_builder = f.debug_tuple("Client");
                let _ = debug_trait_builder.field(&&(*__self_0));
                debug_trait_builder.finish()
            }
            (&NetworkProtocol::Server(ref __self_0),) => {
                let mut debug_trait_builder = f.debug_tuple("Server");
                let _ = debug_trait_builder.field(&&(*__self_0));
                debug_trait_builder.finish()
            }
        }
    }
}
#[automatically_derived]
#[allow(unused_qualifications)]
impl ::core::clone::Clone for NetworkProtocol {
    #[inline]
    fn clone(&self) -> NetworkProtocol {
        match (&*self,) {
            (&NetworkProtocol::Client(ref __self_0),) => {
                NetworkProtocol::Client(::core::clone::Clone::clone(&(*__self_0)))
            }
            (&NetworkProtocol::Server(ref __self_0),) => {
                NetworkProtocol::Server(::core::clone::Clone::clone(&(*__self_0)))
            }
        }
    }
}
impl ::core::marker::StructuralPartialEq for NetworkProtocol {}
#[automatically_derived]
#[allow(unused_qualifications)]
impl ::core::cmp::PartialEq for NetworkProtocol {
    #[inline]
    fn eq(&self, other: &NetworkProtocol) -> bool {
        {
            let __self_vi = unsafe { ::core::intrinsics::discriminant_value(&*self) } as isize;
            let __arg_1_vi = unsafe { ::core::intrinsics::discriminant_value(&*other) } as isize;
            if true && __self_vi == __arg_1_vi {
                match (&*self, &*other) {
                    (
                        &NetworkProtocol::Client(ref __self_0),
                        &NetworkProtocol::Client(ref __arg_1_0),
                    ) => (*__self_0) == (*__arg_1_0),
                    (
                        &NetworkProtocol::Server(ref __self_0),
                        &NetworkProtocol::Server(ref __arg_1_0),
                    ) => (*__self_0) == (*__arg_1_0),
                    _ => unsafe { ::core::intrinsics::unreachable() },
                }
            } else {
                false
            }
        }
    }
    #[inline]
    fn ne(&self, other: &NetworkProtocol) -> bool {
        {
            let __self_vi = unsafe { ::core::intrinsics::discriminant_value(&*self) } as isize;
            let __arg_1_vi = unsafe { ::core::intrinsics::discriminant_value(&*other) } as isize;
            if true && __self_vi == __arg_1_vi {
                match (&*self, &*other) {
                    (
                        &NetworkProtocol::Client(ref __self_0),
                        &NetworkProtocol::Client(ref __arg_1_0),
                    ) => (*__self_0) != (*__arg_1_0),
                    (
                        &NetworkProtocol::Server(ref __self_0),
                        &NetworkProtocol::Server(ref __arg_1_0),
                    ) => (*__self_0) != (*__arg_1_0),
                    _ => unsafe { ::core::intrinsics::unreachable() },
                }
            } else {
                true
            }
        }
    }
}
#[main]
pub fn main() -> () {
    extern crate test;
    test::test_main_static(&[])
}
