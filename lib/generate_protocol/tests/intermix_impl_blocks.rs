use generate_protocol::generate_protocol;

generate_protocol! {
server {
  struct PlayerPosSync {
    x: f32,
    y: f32,
    z: f32,
  }

  impl PlayerPosSync {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
      PlayerPosSync { x, y, z }
    }

    pub fn magnitude(&self) -> f32 {
      (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }
  }

  struct ChunkAt {
    pos: [i32; 3],
    data: Vec<u8>,
  }

  impl ChunkAt {
    pub fn new(pos:[i32; 3], data: Vec<u8>) -> Self {
      ChunkAt { pos, data }
    }
    pub fn pos(&self) -> [i32; 3] {
      self.pos
    }
  }
}
client {
  struct PlayerName(String);
  #[derive(PartialOrd)]
  struct PlayerAction(String);
  struct StopServer;
}
}

fn main() {
    let chunk_at = server::ChunkAt::new([1, 1, 1], Vec::new());
    assert_eq!(chunk_at.pos(), [1, 1, 1]);

    let player_pos = server::PlayerPosSync::new(1.0, 1.0, 0.0);
    let magn = player_pos.magnitude();
    assert_eq!(magn, std::f32::consts::SQRT_2);
}
