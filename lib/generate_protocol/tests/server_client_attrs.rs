use generate_protocol::generate_protocol;

generate_protocol! {
#[cfg(not(featuresless))]
server {
    struct CallMeMaybe;
}
#[cfg(not(featureful))]
client {
    struct IJustMetYou {
        and_this_is_crazy: bool,
    }
}
}

fn main() {
    let _client: client::ClientProtocol = client::IJustMetYou {
        and_this_is_crazy: true
    }.into();

    let _server: server::ServerProtocol = server::CallMeMaybe.into();
}