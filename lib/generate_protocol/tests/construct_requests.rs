mod protocol;
use protocol::*;

fn main() {
    let _server_req_a = server::PlayerPosSync::new(1.0, 1.0, 1.0);
    let _server_req_b = server::ChunkAt::new([1, 1, 1], Vec::new());

    let _client_req_a = client::PlayerName("Hello".to_owned());
    let _client_req_b = client::PlayerAction("An Action".to_owned());
}
