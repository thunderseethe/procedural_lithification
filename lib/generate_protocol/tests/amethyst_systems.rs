mod protocol;
use protocol::*;

fn main() {
  let _server_sender = server::ServerSenderSystemDesc::default();
  let _client_sender = client::ClientSenderSystemDesc::default();

  let _server_receiver = server::ServerReceiverSystemDesc::default();
  let _client_receiver = client::ClientReceiverSystemDesc::default();
}