extern crate amethyst_test;
mod protocol;

#[test]
fn tests() {
    let t = trybuild::TestCases::new();
    t.pass("tests/construct_requests.rs");
    t.pass("tests/intermix_impl_blocks.rs");
    t.pass("tests/coerce_into_protocol.rs");
    t.pass("tests/amethyst_systems.rs");
    t.pass("tests/server_client_attrs.rs");
}