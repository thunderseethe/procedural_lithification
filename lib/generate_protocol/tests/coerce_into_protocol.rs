mod protocol;
use protocol::*;

fn main() {
    let player_pos = server::PlayerPosSync::new(1.0, 1.0, 1.0);
    let server: server::ServerProtocol = player_pos.clone().into();
    assert_eq!(
        server,
        server::ServerProtocol::PlayerPosSync(player_pos.clone())
    );

    let network_protocol: NetworkProtocol = server.into();
    assert_eq!(
        network_protocol,
        NetworkProtocol::Server(server::ServerProtocol::PlayerPosSync(
            player_pos
        ))
    );
}
