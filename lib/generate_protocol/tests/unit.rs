extern crate amethyst_test;
mod protocol;

use amethyst_test::prelude::*;
use protocol::*;
use generate_protocol::specs::{World, shrev::{EventChannel, ReaderId}};

struct MyReader(ReaderId<protocol::server::ServerProtocol>);

#[test]
fn test_sender_system() {
    let player_pos_sync = server::PlayerPosSync::new(1.0, 1.0, 1.0);
    let chunk_at = server::ChunkAt::new([1, 1, 1], Vec::new());

    let pps_: server::ServerProtocol = player_pos_sync.clone().into();
    let ca_: server::ServerProtocol = chunk_at.clone().into();

    assert!(AmethystApplication::blank()
        .with_setup(|world: &mut World| {
            let mut server_protocol_chan: EventChannel<server::ServerProtocol> = EventChannel::default();
            let player_pos_sync_chan: EventChannel<server::PlayerPosSync> = EventChannel::default();
            let chunk_at_chan: EventChannel<server::ChunkAt> = EventChannel::default();

            world.insert(player_pos_sync_chan);
            world.insert(chunk_at_chan);
            world.insert(MyReader(server_protocol_chan.register_reader()));
            world.insert(server_protocol_chan);
        })
        .with_effect(move |world: &mut World| {
            world.fetch_mut::<EventChannel<server::PlayerPosSync>>().single_write(player_pos_sync.clone());
            world.fetch_mut::<EventChannel<server::ChunkAt>>().single_write(chunk_at.clone());
        })
        .with_system_desc(server::ServerSenderSystemDesc::default(), "server_sender_system", &[])
        .with_assertion(move |world: &mut World| {
            let server_protocol_chan = world.fetch::<EventChannel<server::ServerProtocol>>();
            let reader_id = &mut world.fetch_mut::<MyReader>().0;
            let mut iter = server_protocol_chan.read(reader_id);
            assert_eq!(iter.next(), Some(&pps_));
            assert_eq!(iter.next(), Some(&ca_));
        })
        .with_assertion(move |world: &mut World| {
            let server_protocol_chan = world.fetch::<EventChannel<server::ServerProtocol>>();
            let reader_id = &mut world.fetch_mut::<MyReader>().0;
            let mut iter = server_protocol_chan.read(reader_id);
            assert_eq!(iter.next(), None);
            assert_eq!(iter.next(), None);
        })
        .run()
        .is_ok())

}

#[test]
fn test_receiver_system() {
    assert!(AmethystApplication::blank()
        .with_setup(|world: &mut World| {
            let client_protocol_chan: EventChannel<client::ClientProtocol> = EventChannel::default();
            let mut player_name_chan: EventChannel<client::PlayerName> = EventChannel::default();
            let mut player_action_chan: EventChannel<client::PlayerAction> = EventChannel::default();

            let player_name_reader_id = player_name_chan.register_reader();
            let player_action_reader_id = player_action_chan.register_reader();

            world.insert(client_protocol_chan);
            world.insert(player_name_chan);
            world.insert(player_action_chan);
            world.insert(player_name_reader_id);
            world.insert(player_action_reader_id);
        })
        .with_effect(|world: &mut World| {
            world.fetch_mut::<EventChannel<client::ClientProtocol>>().iter_write(vec![
                client::PlayerName("player".to_string()).into(),
                client::PlayerAction("action".to_string()).into()
            ])
        })
        .with_system_desc(server::ServerReceiverSystemDesc::default(), "client_receiver_system", &[])
        .with_assertion(move |world: &mut World| {
            let player_name_chan = world.fetch::<EventChannel<client::PlayerName>>();
            let player_action_chan = world.fetch::<EventChannel<client::PlayerAction>>();

            let player_name_reader_id = &mut world.fetch_mut();
            assert_eq!(player_name_chan.read(player_name_reader_id).next().unwrap(), &client::PlayerName("player".to_string()));
            let player_action_reader_id = &mut world.fetch_mut();
            assert_eq!(player_action_chan.read(player_action_reader_id).next().unwrap(), &client::PlayerAction("action".to_string()));
        })
        .with_assertion(move |world: &mut World| {
            let player_name_chan = world.fetch::<EventChannel<client::PlayerName>>();
            let player_action_chan = world.fetch::<EventChannel<client::PlayerAction>>();

            let player_name_reader_id = &mut world.fetch_mut();
            assert_eq!(player_name_chan.read(player_name_reader_id).next(), None);
            let player_action_reader_id = &mut world.fetch_mut();
            assert_eq!(player_action_chan.read(player_action_reader_id).next(), None);
        })
        .run()
        .is_ok()
    )
}