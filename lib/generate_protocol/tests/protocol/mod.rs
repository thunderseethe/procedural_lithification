#![allow(dead_code)]
#![allow(unused_imports)]
use generate_protocol::generate_protocol;

generate_protocol! {
name NetworkProtocol,
server {
  struct PlayerPosSync {
    x: f32,
    y: f32,
    z: f32,
  }
  impl PlayerPosSync {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
      PlayerPosSync {x, y, z}
    }
  }

  struct ChunkAt {
    pos: [i32; 3],
    data: Vec<u8>,
  }
  impl ChunkAt {
    pub fn new(pos: [i32; 3], data: Vec<u8>) -> Self {
      ChunkAt { pos, data }
    }
  }
}

client {
  struct PlayerName(String);
  struct PlayerAction(String);
}
}