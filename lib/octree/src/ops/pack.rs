use nalgebra::Scalar;

use crate::*;

#[derive(ToPrimitive, FromPrimitive, Clone, Copy, PartialEq, Debug, Serialize, Deserialize)]
pub enum NodeVariant {
    Error = 0,
    Empty = 1,
    Leaf = 2,
    Branch = 3,
}

trait PairJoin<A, B> {
    fn and_then<F, OutA, OutB>(self, func: F) -> (OutA, OutB)
    where
        F: Fn(A, B) -> (OutA, OutB);
}
impl<A, B> PairJoin<A, B> for (A, B) {
    fn and_then<F, OutA, OutB>(self, func: F) -> (OutA, OutB)
    where
        F: FnOnce(A, B) -> (OutA, OutB),
    {
        func(self.0, self.1)
    }
}

pub trait PackInternal: ElementType {
    fn pack_into(
        &self,
        node_map: Vec<NodeVariant>,
        values: Vec<Self::Element>,
    ) -> (Vec<NodeVariant>, Vec<Self::Element>);
}

impl<O> PackInternal for OctreeLevel<O>
where
    O: OctreeTypes + PackInternal,
    ElementOf<Self>: Clone,
{
    fn pack_into(
        &self,
        mut node_map: Vec<NodeVariant>,
        mut values: Vec<Self::Element>,
    ) -> (Vec<NodeVariant>, Vec<Self::Element>) {
        match &self.data {
            LevelData::Empty => {
                node_map.push(NodeVariant::Empty);
                (node_map, values)
            }
            LevelData::Leaf(ref elem) => {
                node_map.push(NodeVariant::Leaf);
                values.push(elem.clone());
                (node_map, values)
            }
            LevelData::Node(nodes) => {
                node_map.push(NodeVariant::Branch);
                (node_map, values)
                    .and_then(|a, b| nodes[0].pack_into(a, b))
                    .and_then(|a, b| nodes[1].pack_into(a, b))
                    .and_then(|a, b| nodes[2].pack_into(a, b))
                    .and_then(|a, b| nodes[3].pack_into(a, b))
                    .and_then(|a, b| nodes[4].pack_into(a, b))
                    .and_then(|a, b| nodes[5].pack_into(a, b))
                    .and_then(|a, b| nodes[6].pack_into(a, b))
                    .and_then(|a, b| nodes[7].pack_into(a, b))
            }
        }
    }
}

impl<E: Clone, N: Scalar> PackInternal for OctreeBase<E, N> {
    fn pack_into(
        &self,
        mut node_map: Vec<NodeVariant>,
        mut values: Vec<E>,
    ) -> (Vec<NodeVariant>, Vec<E>) {
        if let Some(ref elem) = self.data {
            node_map.push(NodeVariant::Leaf);
            values.push(elem.clone());
        } else {
            node_map.push(NodeVariant::Empty);
        }
        (node_map, values)
    }
}

/// Right now this exists to preallocate Vecs used for pack()
pub trait CountNodes {
    /// Count only leaf nodes
    fn count_leaf_nodes(&self) -> usize;
    /// Count all nodes
    fn count_nodes(&self) -> usize;
}
impl<O> CountNodes for OctreeLevel<O>
where
    O: OctreeTypes + CountNodes,
{
    fn count_leaf_nodes(&self) -> usize {
        match &self.data {
            LevelData::Empty => 0,
            LevelData::Leaf(_) => 1,
            LevelData::Node(nodes) => {
                nodes[0].count_leaf_nodes()
                    + nodes[1].count_leaf_nodes()
                    + nodes[2].count_leaf_nodes()
                    + nodes[3].count_leaf_nodes()
                    + nodes[4].count_leaf_nodes()
                    + nodes[5].count_leaf_nodes()
                    + nodes[6].count_leaf_nodes()
                    + nodes[7].count_leaf_nodes()
            }
        }
    }

    fn count_nodes(&self) -> usize {
        match &self.data {
            LevelData::Empty | LevelData::Leaf(_) => 1,
            LevelData::Node(nodes) => {
                1 + nodes[0].count_nodes()
                    + nodes[1].count_nodes()
                    + nodes[2].count_nodes()
                    + nodes[3].count_nodes()
                    + nodes[4].count_nodes()
                    + nodes[5].count_nodes()
                    + nodes[6].count_nodes()
                    + nodes[7].count_nodes()
            }
        }
    }
}
impl<E, N: Scalar> CountNodes for OctreeBase<E, N> {
    fn count_leaf_nodes(&self) -> usize {
        self.data.as_ref().map(|_| 1).unwrap_or(0)
    }

    fn count_nodes(&self) -> usize {
        1
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct PackedOctree<E> {
    node_map: Vec<NodeVariant>,
    values: Vec<E>,
}
impl<O> OctreeLevel<O>
where
    Self: PackInternal + CountNodes,
    O: OctreeTypes,
{
    pub fn pack(&self) -> PackedOctree<ElementOf<Self>> {
        let empty_node_map = Vec::with_capacity(self.count_nodes());
        let empty_values = Vec::with_capacity(self.count_leaf_nodes());
        let (node_map, values) = self.pack_into(empty_node_map, empty_values);
        PackedOctree { node_map, values }
    }
}
impl<E: Clone, N: Scalar> OctreeBase<E, N> {
    pub fn pack(&self) -> PackedOctree<E> {
        // Pro trick, this is always 1
        let empty_node_map = Vec::with_capacity(self.count_nodes());
        let empty_values = Vec::with_capacity(self.count_leaf_nodes());
        let (node_map, values) = self.pack_into(empty_node_map, empty_values);
        PackedOctree { node_map, values }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum UnpackError<N: Scalar> {
    NodeVariantError,
    MissingValue(Point3<N>),
    MissingNode(Point3<N>),
    UnexpectedBranchAtBase(Point3<N>),
}
use UnpackError::*;

pub trait UnpackInternal: OctreeTypes
where
    Self: std::marker::Sized,
{
    fn unpack_from<N, V>(
        pos: Point3<Self::Field>,
        node_iter: &mut N,
        value_iter: &mut V,
    ) -> Result<Self, UnpackError<Self::Field>>
    where
        N: Iterator<Item = NodeVariant>,
        V: Iterator<Item = Self::Element>;
}

impl<O> UnpackInternal for OctreeLevel<O>
where
    O: OctreeTypes + UnpackInternal + Diameter,
{
    fn unpack_from<N, V>(
        pos: Point3<FieldOf<Self>>,
        node_iter: &mut N,
        value_iter: &mut V,
    ) -> Result<Self, UnpackError<FieldOf<Self>>>
    where
        N: Iterator<Item = NodeVariant>,
        V: Iterator<Item = ElementOf<Self>>,
    {
        use std::mem::MaybeUninit;
        node_iter
            .next()
            .ok_or_else(|| MissingNode(pos))
            .and_then(|node| match node {
                NodeVariant::Error => Err(NodeVariantError),
                NodeVariant::Empty => Ok(Self::new(LevelData::Empty, pos)),
                NodeVariant::Leaf => value_iter
                    .next()
                    .ok_or_else(|| MissingValue(pos))
                    .map(|val| Self::new(LevelData::Leaf(val), pos)),
                NodeVariant::Branch => {
                    let mut nodes: MaybeUninit<[Ref<O>; 8]> = MaybeUninit::uninit();
                    let nodes_ptr: *mut Ref<O> = nodes.as_mut_ptr() as *mut Ref<O>;
                    for octant in OctantId::iter() {
                        unsafe {
                            nodes_ptr
                                .add(
                                    octant
                                        .to_usize()
                                        .expect("conversion from OctantId to usize to succeed"),
                                )
                                .write(Ref::new(O::unpack_from(
                                    octant.sub_octant_bottom_left(pos, O::DIAMETER),
                                    node_iter,
                                    value_iter,
                                )?))
                        }
                    }
                    Ok(Self::new(
                        LevelData::Node(unsafe { nodes.assume_init() }),
                        pos,
                    ))
                }
            })
    }
}

impl<E, N> UnpackInternal for OctreeBase<E, N>
where
    N: field::Number,
{
    fn unpack_from<Nodes, Values>(
        pos: Point3<FieldOf<Self>>,
        node_iter: &mut Nodes,
        value_iter: &mut Values,
    ) -> Result<Self, UnpackError<N>>
    where
        Nodes: Iterator<Item = NodeVariant>,
        Values: Iterator<Item = E>,
    {
        node_iter
            .next()
            .ok_or_else(|| MissingNode(pos))
            .and_then(|node| match node {
                NodeVariant::Error => Err(NodeVariantError),
                NodeVariant::Branch => Err(UnexpectedBranchAtBase(pos)),
                NodeVariant::Empty => Ok(Self::new(None, pos)),
                NodeVariant::Leaf => value_iter
                    .next()
                    .ok_or_else(|| MissingValue(pos))
                    .map(|val| Self::new(Some(val), pos)),
            })
    }
}

impl<O> OctreeLevel<O>
where
    O: OctreeTypes,
    Self: UnpackInternal,
{
    pub fn unpack(
        packed: PackedOctree<ElementOf<Self>>,
    ) -> Result<Self, UnpackError<FieldOf<Self>>> {
        Self::unpack_from(
            Point3::origin(),
            &mut packed.node_map.into_iter(),
            &mut packed.values.into_iter(),
        )
    }
}

impl<E, N: field::Number> OctreeBase<E, N> {
    pub fn unpack(
        packed: PackedOctree<ElementOf<Self>>,
    ) -> Result<Self, UnpackError<FieldOf<Self>>> {
        Self::unpack_from(
            Point3::origin(),
            &mut packed.node_map.into_iter(),
            &mut packed.values.into_iter(),
        )
    }
}

#[cfg(test)]
mod test {
    use crate::octree::consts::Octree;
    use quickcheck_macros::quickcheck;
    use typenum::{U16, U2, U4, U8};

    macro_rules! pack_props {
        ($($name:ident<$octree:ty>);+) => {
            $(
                #[quickcheck]
                fn $name(octree: $octree) -> bool {
                    let p = octree.pack();
                    let result = <$octree>::unpack(p);
                    result.map(|res| octree == res).unwrap_or(false)
                }
            )+
        };
    }

    pack_props!(
        pack_u2_identity<Octree<u32, u8, U2>>;
        pack_u4_identity<Octree<String, u8, U4>>;
        pack_u8_identity<Octree<u32, u8, U8>>;
        pack_u16_identity<Octree<String, u8, U16>>
    );
}
