use crate::*;
use alga::general::RealField;
use field::FieldOf;
use ncollide3d::{
    bounding_volume::aabb::AABB,
    math::{Isometry, Point},
    query::{Ray, RayCast, RayIntersection},
};
use num_traits::AsPrimitive;
use rayon::prelude::*;
use std::cmp::Ordering;

impl<E, N, F> RayCast<F> for OctreeBase<E, N>
where
    E: fmt::Debug,
    N: Number + AsPrimitive<F> + fmt::Display,
    F: RealField,
    usize: AsPrimitive<F>,
{
    fn toi_and_normal_and_uv_with_ray(
        &self,
        m: &Isometry<F>,
        ray: &Ray<F>,
        solid: bool,
    ) -> Option<RayIntersection<F>> {
        self.data.as_ref().and_then(|_| {
            let aabb: AABB<F> = self.as_aabb();
            aabb.toi_and_normal_and_uv_with_ray(m, ray, solid)
        })
    }

    fn toi_and_normal_with_ray(
        &self,
        m: &Isometry<F>,
        ray: &Ray<F>,
        solid: bool,
    ) -> Option<RayIntersection<F>> {
        self.data.as_ref().and_then(|_| {
            let aabb: AABB<F> = self.as_aabb();
            aabb.toi_and_normal_with_ray(m, ray, solid)
        })
    }

    fn toi_with_ray(&self, m: &Isometry<F>, ray: &Ray<F>, solid: bool) -> Option<F> {
        self.data.as_ref().and_then(|_| {
            let aabb: AABB<F> = self.as_aabb();
            aabb.toi_with_ray(m, ray, solid)
        })
    }
}

macro_rules! ray_cast_fn {
    (fn $method:ident($this:ident) -> $output: ty where CmpFn = $cmp:expr) => {
        fn $method(
            &$this,
            m: &Isometry<F>,
            ray: &Ray<F>,
            solid: bool,
        ) -> $output
        {
            $this.map(
                // Why can't life ever be this simple.
                || None,
                |_| {
                    let aabb: AABB<F> = $this.as_aabb();
                    aabb.$method(m, ray, solid)
                },
                |children| {
                    children
                        .par_iter()
                        .map(|child| {
                            let aabb: AABB<F> = child.as_ref().as_aabb();
                            if !aabb.intersects_ray(m, ray) {
                                return None;
                            }
                            child.$method(m, ray, solid)
                        })
                        .reduce(
                            || None,
                            |a, b| match (a, b) {
                                (None, None) => None,
                                (Some(_), None) => a,
                                (None, Some(_)) => b,
                                (Some(a), Some(b)) => ($cmp)(&a, &b).map(|ord| match ord {
                                    Ordering::Greater | Ordering::Equal => b,
                                    Ordering::Less => a,
                                }),
                            },
                        )
                },
            )
        }
    };
}

#[allow(clippy::redundant_closure_call)]
impl<O, F> RayCast<F> for OctreeLevel<O>
where
    O: OctreeTypes + Diameter + RayCast<F> + Send + Sync + AsAABB<F> + HasPosition,
    FieldOf<O>: AsPrimitive<usize> + AsPrimitive<F>,
    PositionOf<O>: std::fmt::Display,
    F: RealField,
    usize: AsPrimitive<F>,
{
    ray_cast_fn!(
        fn toi_with_ray(self) -> Option<F>
        where CmpFn = |a: &F, b: &F| a.partial_cmp(&b)
    );

    ray_cast_fn!(
        fn toi_and_normal_with_ray(self) -> Option<RayIntersection<F>>
        where CmpFn = |a: &RayIntersection<F>, b: &RayIntersection<F>| a.toi.partial_cmp(&b.toi)
    );

    ray_cast_fn!(
        fn toi_and_normal_and_uv_with_ray(self) -> Option<RayIntersection<F>>
        where CmpFn = |a: &RayIntersection<F>, b: &RayIntersection<F>| a.toi.partial_cmp(&b.toi)
    );
}

pub trait AsAABB<F: RealField> {
    fn as_aabb(&self) -> AABB<F>;
}
impl<E, N, F> AsAABB<F> for OctreeBase<E, N>
where
    N: Number + AsPrimitive<F>,
    F: RealField,
    usize: AsPrimitive<F>,
{
    fn as_aabb(&self) -> AABB<F> {
        let mins = Point::new(
            self.bottom_left.x.as_(),
            self.bottom_left.y.as_(),
            self.bottom_left.z.as_(),
        );
        let diameter = self.diameter();
        let (max_x, max_y, max_z): (usize, usize, usize) = (
            self.bottom_left.x.as_(),
            self.bottom_left.y.as_(),
            self.bottom_left.z.as_(),
        );
        let maxs: Point<F> = Point::new(
            (max_x + diameter).as_(),
            (max_y + diameter).as_(),
            (max_z + diameter).as_(),
        );
        AABB::new(mins, maxs)
    }
}

impl<O, F> AsAABB<F> for OctreeLevel<O>
where
    O: OctreeTypes + Diameter,
    FieldOf<O>: AsPrimitive<usize> + AsPrimitive<F>,
    F: RealField,
    usize: AsPrimitive<F>,
{
    fn as_aabb(&self) -> AABB<F> {
        // Whatever happened to legos?
        let mins = Point::new(
            self.bottom_left.x.as_(),
            self.bottom_left.y.as_(),
            self.bottom_left.z.as_(),
        );
        let diameter = self.diameter();
        //I know you know what I'm a talking about. Legos used to be simple.
        let (max_x, max_y, max_z): (usize, usize, usize) = (
            self.bottom_left.x.as_(),
            self.bottom_left.y.as_(),
            self.bottom_left.z.as_(),
        );
        // I'm not mad, I just want to know what happened.
        let maxs: Point<F> = Point::new(
            (max_x + diameter).as_(),
            (max_y + diameter).as_(),
            (max_z + diameter).as_(),
        );
        // Wait a second
        // This is just a carbon copy of the shit we did for OctreeBase
        // I thought all these traits were supposed to help reduce boilerplate
        AABB::new(mins, maxs)
    }
}

#[cfg(test)]
mod test {
    use crate::{
        octree::{consts::Octree8, octant::OctantFace, Insert, LevelData, New, OctreeBase},
        rand::Rng,
    };
    use amethyst::core::math::{Isometry3, Point3, Vector3};
    use galvanic_assert::matchers::close_to;
    use galvanic_assert::*;
    use ncollide3d::query::{Ray, RayCast};
    use quickcheck::{Arbitrary, Gen};
    use typenum::*;

    #[derive(Clone, Debug)]
    struct CubeFaceRay<U> {
        origin: Point3<f32>,
        dir: Vector3<f32>,
        _marker: std::marker::PhantomData<U>,
    }
    impl<U> CubeFaceRay<U> {
        fn new(origin: Point3<f32>, dir: Vector3<f32>) -> Self {
            CubeFaceRay {
                origin,
                dir,
                _marker: std::marker::PhantomData,
            }
        }

        fn inwards_ray(&self, distance: f32) -> Ray<f32> {
            let away_point = self.origin + (distance * self.dir);
            let towards_dir = -self.dir;
            Ray::new(away_point, towards_dir)
        }
    }
    impl<U> Arbitrary for CubeFaceRay<U>
    where
        U: Unsigned + Send + Sync + Clone + 'static,
    {
        fn arbitrary<G: Gen>(g: &mut G) -> Self {
            use OctantFace::*;
            let cube_bound = U::USIZE as f32;
            let (a, b): (f32, f32) = (g.gen_range(0.0, cube_bound), g.gen_range(0.0, cube_bound));
            let (dir_a, dir_b): (f32, f32) = (g.gen_range(-1.0, 1.0), g.gen_range(-1.0, 1.0));
            match OctantFace::arbitrary(g) {
                Front => CubeFaceRay::new(
                    Point3::new(a, b, cube_bound),
                    Vector3::new(dir_a, dir_b, g.gen_range(0.0, 1.0)),
                ),
                Back => CubeFaceRay::new(
                    Point3::new(a, b, 0.0),
                    Vector3::new(dir_a, dir_b, g.gen_range(-1.0, 0.0)),
                ),
                Right => CubeFaceRay::new(
                    Point3::new(cube_bound, a, b),
                    Vector3::new(g.gen_range(0.0, 1.0), dir_a, dir_b),
                ),
                Left => CubeFaceRay::new(
                    Point3::new(0.0, a, b),
                    Vector3::new(g.gen_range(-1.0, 0.0), dir_a, dir_b),
                ),
                Up => CubeFaceRay::new(
                    Point3::new(a, cube_bound, b),
                    Vector3::new(dir_a, g.gen_range(0.0, 1.0), dir_b),
                ),
                Down => CubeFaceRay::new(
                    Point3::new(a, 0.0, b),
                    Vector3::new(dir_a, g.gen_range(-1.0, 0.0), dir_b),
                ),
            }
        }
    }

    #[quickcheck]
    fn raycast_octree_base(p: CubeFaceRay<U1>, distance_u32: u32) {
        let distance = distance_u32 as f32;
        let o: OctreeBase<u32, u8> = OctreeBase::new(Some(1), Point3::origin());

        let ray = p.inwards_ray(distance);
        let iso = Isometry3::identity();

        let intersection = o.toi_and_normal_with_ray(&iso, &ray, true).unwrap();
        let check_point = ray.origin + (ray.dir * intersection.toi);
        assert_that!(&intersection.toi, close_to(distance, 1e-5));
        assert_that!(
            &check_point.coords,
            close_to(p.origin.coords, Vector3::new(1e-4, 1e-4, 1e-4))
        );
    }

    #[quickcheck]
    fn raycast_octree_level_u1(p: CubeFaceRay<U1>, distance_u32: u32) {
        let distance = distance_u32 as f32;
        let o: Octree8<u32, u8> =
            Octree8::new(LevelData::Empty, Point3::origin()).insert(Point3::origin(), 1u32);

        let ray = p.inwards_ray(distance);
        let iso = Isometry3::identity();

        let intersection = o.toi_and_normal_with_ray(&iso, &ray, true).unwrap();
        let check_point = ray.origin + (ray.dir * intersection.toi);
        assert_that!(&intersection.toi, close_to(distance, 1e-5));
        assert_that!(
            &check_point.coords,
            close_to(p.origin.coords, Vector3::new(1e-4, 1e-4, 1e-4))
        );
    }
}
