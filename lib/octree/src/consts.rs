//! Module to ease the use of OctreeLevel and OctreeBase
//! This module provides aliases to mask the verbosity of an Octrees type.
//!
//! Example:
//! ```
//! use typenum::U128;
//! use cubes_lib::octree::Octree;
//!
//! let octree = Octree::<isize, u32, U128>::at_origin(Some(-124));
//! ```
//!
//! This example will construct an Octree of size 128 (height 7) filled with the element -124.
#![allow(dead_code)]

use super::*;
use typenum::{
    Bit, PowerOfTwo, Shright, UInt, UTerm, Unsigned, B1, U128, U16, U2, U256, U32, U4, U64, U8,
};

/// Trait implemented by UInt and UTerm to map them to their respective Octrees.
pub trait ToOctree<E, N> {
    type Octree: OctreeTypes;
}

impl<E, N: Number> ToOctree<E, N> for UTerm {
    type Octree = OctreeBase<E, N>;
}
impl<E, N: Number, U: Unsigned + ToOctree<E, N>, B: Bit> ToOctree<E, N> for UInt<U, B>
where
    UInt<U, B>: PowerOfTwo,
{
    type Octree = OctreeLevel<<U as ToOctree<E, N>>::Octree>;
}

pub type Octree<E, N, Diameter> = <Shright<Diameter, B1> as ToOctree<E, N>>::Octree;

// Special case 0 to be OctreeBase
pub type Octree0<E, N> = OctreeBase<E, N>;
macro_rules! octree_consts {
    ($($octree_num:ident => $diameter:ty),+) => {
        $(
            pub type $octree_num<E, N> = Octree<E, N, $diameter>;
        )*
    };
}
octree_consts! {
    Octree1 => U2,
    Octree2 => U4,
    Octree3 => U8,
    Octree4 => U16,
    Octree5 => U32,
    Octree6 => U64,
    Octree7 => U128,
    Octree8 => U256
}
