use crate::*;
use field::{FieldOf, Number};
use quickcheck::{Arbitrary, Gen, StdThreadGen};
use rayon::iter::{IntoParallelRefMutIterator, ParallelIterator};

impl<E, N> Arbitrary for OctreeBase<E, N>
where
    E: Clone + Arbitrary,
    N: Number + Arbitrary,
{
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        let (x, y, z) = <(N, N, N)>::arbitrary(g);
        Self::new(Option::<E>::arbitrary(g), Point3::new(x, y, z))
    }
}

impl<O> Arbitrary for OctreeLevel<O>
where
    O: OctreeTypes + Send + Clone + Arbitrary,
    Self: Builder,
    <Self as Builder>::Builder: Build<Self>
        + for<'r> IntoParallelRefMutIterator<
            'r,
            Item = (PositionOf<Self>, &'r mut Option<ElementOf<Self>>),
        >,
    ElementOf<Self>: Send + Clone + Arbitrary,
    FieldOf<Self>: Send,
{
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        let mut builder = Self::builder();
        let size = g.size();
        builder.par_iter_mut().for_each(
            |(_, block): (PositionOf<Self>, &'_ mut Option<ElementOf<Self>>)| {
                *block = Option::<ElementOf<Self>>::arbitrary(&mut StdThreadGen::new(size));
            },
        );
        builder.build()
    }
}
