#[macro_use]
extern crate criterion;
extern crate octree;
extern crate cubes_lib;

use amethyst::core::math::Point3;
use criterion::{Criterion, ParameterizedBenchmark};
use cubes_lib::chunk::serde::{ChunkDeserialize, ChunkSerialize};
use octree::*;
use cubes_lib::terrain::{Terrain, arbitrary::ArbitraryGenerateFn};
use rand::random;
use std::time::Duration;

fn chunk_generation(c: &mut Criterion) {
    c.bench(
        "chunk_generation",
        ParameterizedBenchmark::new(
            "Default Single Block",
            |b, i| b.iter(|| Terrain::default().generate_chunk(i)),
            vec![Point3::new(0, 0, 0)],
        ),
    );
    c.bench(
        "chunk_generation",
        ParameterizedBenchmark::new(
            "Arbitrary",
            |b, i| b.iter(|| Terrain::default().with_block_generator(ArbitraryGenerateFn::default()).generate_chunk(i)),
            vec![Point3::new(0, 0, 0)]
        )
    );
}

fn chunk_meshing(c: &mut Criterion) {
    c.bench_function("chunk_meshing", |b| {
        let chunk = Terrain::default().generate_chunk(Point3::origin());
        b.iter(|| chunk.generate_mesh())
    });
}

fn serialization_bench(c: &mut Criterion) {
    c.bench_function("Chunk Serialization", |b| {
        let chunk = Terrain::default().generate_chunk(Point3::origin());
        b.iter(|| ChunkSerialize::into(&mut std::io::sink(), &chunk))
    });

    c.bench_function("Chunk Deserialization", |b| {
        let center = Point3::origin();
        let chunk = Terrain::default().generate_chunk(center);
        let mut bytes: Vec<u8> = Vec::new();
        ChunkSerialize::into(&mut bytes, &chunk).expect("Failed to serialize chunk into bytes");
        b.iter(|| ChunkDeserialize::from(&bytes[..], center))
    });
}

/* * * * * * * * * * * * * * * *
 * Chunk Benchmarks Group
 * * * * * * * * * * * * * * * */
criterion_group!(
    name = chunk_generation_group;
    config = Criterion::default().warm_up_time(Duration::from_secs(10)).sample_size(20).measurement_time(Duration::from_secs(30));
    targets = chunk_generation
);
criterion_group!(
    chunk_group,
    chunk_meshing,
    serialization_bench
);

fn octree_comparison(c: &mut Criterion) {
    let points: Vec<(Point3<u8>, u32)> = (0..8000000)
        .map(|_| {
            (
                Point3::<u8>::new(random(), random(), random()),
                random::<u32>(),
            )
        })
        .collect();
    let octrees: Octree8<u32, u8> = points.iter().fold(
        Octree8::new(LevelData::Empty, Point3::origin()),
        |acc, (p, i)| acc.insert(p, *i),
    );
    c.bench(
        "octree_insert",
        ParameterizedBenchmark::new(
            "bounded_recursion",
            |b, octree| {
                b.iter(|| {
                    octree.insert(
                        Point3::<u8>::new(random(), random(), random()),
                        random::<u32>(),
                    )
                })
            },
            vec![octrees.clone()],
        ),
    );
    c.bench(
        "octree_delete",
        ParameterizedBenchmark::new(
            "bounded_recursion",
            |b, octree| {
                b.iter(|| {
                    octree.delete(Point3::new(random(), random(), random()));
                })
            },
            vec![octrees.clone()],
        ),
    );
    c.bench(
        "octree_get",
        ParameterizedBenchmark::new(
            "bounded_recursion",
            |b, octree| b.iter(|| octree.get(Point3::new(random(), random(), random()))),
            vec![octrees],
        ),
    );
}

/* * * * * * * * * * * * * * * *
 * Octree Benchmarks Group
 * * * * * * * * * * * * * * * */
criterion_group!(octree_group, octree_comparison);

criterion_main!(chunk_generation_group, chunk_group, octree_group);
