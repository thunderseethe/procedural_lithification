# Procedural Lithification
A procedurally generated voxel world.

# OS Dependencies (may not be exhaustive) for Ubuntu:
[Amethyst](https://amethyst.rs) depends on the follow OS packages:\
```
libasound2-dev cmake g++ gcc libfreetype6-dev libexpat1-dev libxcb-randr0-dev libxcb-xtest0-dev libxcb-xinerama0-dev libxcb-shape0-dev libxcb-xkb-dev libxcb-xfixes0-dev
```


# Environment Variables
Building requires a C/C++ compiler is findable by cargo:
```
export CC=/usr/bin/gcc
export CXX=/usr/bin/g++
```

# Short Term TODOs
* Chunks become invisible for certain viewing angles (suspect this is caused by a change in face culling with migration from opengl -> vulkan)
* Server should save chunks to disk
* Collision detection for player and chunk
* Find way to track what chunks server needs to send to what players
* ...etc.