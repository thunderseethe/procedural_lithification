extern crate amethyst;
extern crate anyhow;
extern crate cubes_lib;
extern crate dirs;
extern crate fern;
extern crate morton_code;
extern crate parking_lot;
extern crate rayon;
extern crate shared_memory_queue;
extern crate tokio;
#[cfg(feature = "profiler")]
extern crate thread_profiler;

use amethyst::{
    core::{frame_limiter::FrameRateLimitStrategy, TransformBundle},
    input::InputBundle,
    network::simulation::laminar::{LaminarNetworkBundle, LaminarSocket},
    prelude::*,
    renderer::{
        pass::*,
        plugins::{RenderBase3D, RenderSkybox},
        types::DefaultBackend,
        RenderToWindow, RenderingBundle,
    },
    shrev::EventChannel,
    ui::{RenderUi, UiBundle},
    utils::application_root_dir,
    CoreApplication, Result,
};
use clap::{Arg, App};
use cubes_lib::{
    input::*,
    protocol::{client::ClientProtocol, server::ServerProtocol},
    states::ClientDimensionState,
    systems::{
        collision::CheckPlayerCollisionSystem,
        dimension::{DimensionBundle, DimensionChunkEvent},
        net::{client::*, ClientLaminarToLocalChannelSystemDesc, ClientLocalChannelToLaminarChannelSystemDesc},
        player::PlayerControlBundle,
        ray_picking::RayPickingBundle,
    },
};
use log::*;
use std::path::PathBuf;
use tokio::runtime::Runtime;
use crossbeam::{Sender, Receiver};

fn build_client<'a, 'b>(
    resources: PathBuf,
    port: u16,
    server_addr: &str,
) -> Result<CoreApplication<'static, GameData<'a, 'b>>> 
{
    let display_config_path = resources.join("display_config.ron");
    let key_bindings_path = resources.join("input.ron");
    let (local_sender, local_receiver): (Sender<ClientProtocol>, Receiver<ClientProtocol>) = crossbeam::channel::bounded(1024);
    let (chunk_send, chunk_recv): (Sender<DimensionChunkEvent>, Receiver<DimensionChunkEvent>) = crossbeam::channel::bounded(1024);
    // This handles copying messages we receiver from our server (either locally or over a socket) to a common in-memory abstraction
    let (server_sender, server_receiver): (Sender<ServerProtocol>, Receiver<ServerProtocol>) = crossbeam::channel::bounded(1024);

    let server_addr = cubes_lib::systems::net::ServerAddr::new(server_addr.parse()?);
    let socket = LaminarSocket::bind(("127.0.0.1", port))?;

    let game_data = GameDataBuilder::default()
        .with_system_desc(ClientLaminarToLocalChannelSystemDesc::default(), "client_laminar_to_local", &[])
        .with_bundle(ClientNetBundle::default())?
        .with_system_desc(ClientLocalChannelToLaminarChannelSystemDesc::default(), "client_local_to_laminar", &[CLIENT_SENDER])
        .with_bundle(LaminarNetworkBundle::new(Some(socket)))?
        .with_bundle(
            PlayerControlBundle::<DefaultBindings>::new(
                Some(AxisBinding::KeyX),
                Some(AxisBinding::KeyY),
                Some(AxisBinding::KeyZ),
            )
            .with_speed(16.0)
            .with_sensitivity(0.1, 0.1),
        )?
        .with_bundle(TransformBundle::new().with_dep(&["player_movement"]))?
        .with_bundle(
            InputBundle::<DefaultBindings>::new().with_bindings_from_file(&key_bindings_path)?,
        )?
        .with_bundle(UiBundle::<DefaultBindings>::new())?
        .with_bundle(DimensionBundle::default())?
        .with_bundle(RayPickingBundle::default())?
        .with(
            CheckPlayerCollisionSystem::default(),
            "check_player_collission",
            &[],
        )
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(RenderToWindow::from_config_path(display_config_path)?)
                .with_plugin(RenderSkybox::default())
                .with_plugin(RenderBase3D::<FlatPassDef>::default())
                .with_plugin(RenderUi::default()),
        )?;

    Application::build(&resources, ClientDimensionState::default())?
        .with_resource(Runtime::new().unwrap())
        .with_resource(EventChannel::<DimensionChunkEvent>::new())
        .with_resource(server_addr)
        .with_resource(local_sender)
        .with_resource(local_receiver)
        .with_resource(chunk_send)
        .with_resource(chunk_recv)
        .with_resource(server_sender)
        .with_resource(server_receiver)
        .with_frame_limit(FrameRateLimitStrategy::Yield, 60)
        .build(game_data)
}

fn main() -> anyhow::Result<()> {
    let app_root = application_root_dir().map_err(|err| {
        error!("Failed to get application root dir");
        anyhow::Error::new(err)
    })?;
    cubes_lib::configure_logger(app_root.join("log").join("cubes_client.log")).map_err(|err| {
        error!("Failed to configure logger");
        anyhow::Error::new(err)
    })?;

    let matches = App::new("Procedural Lithification Client")
        .version("0.1")
        .author("thunderseethe <thunderseethe.dev@gmail.com>")
        .about("It's a client, for proc_lith")
        .arg(Arg::with_name("server_port")
            .long("server_port")
            .help("Port of existing local server")
            .takes_value(true))
        .get_matches();

    #[cfg(not(debug_resource_path))]
    let resources = app_root.join("resources");
    #[cfg(debug_resource_path)]
    let resources = app_root
        .parent()
        .unwrap()
        .parent()
        .unwrap()
        .join("resources");
    info!("RESOURCES: {:?}", resources);

    let (server_handle, server_port) = match matches.value_of("server_port") {
        Some(server_port_str) => (None, server_port_str),
        None =>  {
            let default_server_port = "3457";
            server_prog(app_root, default_server_port)
                .and_then(|mut prog| prog.spawn().map_err(anyhow::Error::new))
                .map(|handle| (Some(handle), default_server_port))
                .map_err(|err| {
                    error!("Failed to spawn server");
                    err
                })?
        }
    };

    let port: u16 = 3345;
    let server_addr = format!("127.0.0.1:{}", server_port);

    let mut client = build_client(resources, port, &server_addr).unwrap();
    info!("CLIENT STARTED");
    client.run();

    if let Some(mut server_handle) = server_handle {
        return server_handle.kill().or_else(|err| match err.kind() {
            // Invalid input indicates the server has already quit, we can safely ignore
            std::io::ErrorKind::InvalidInput => {
                info!("Server already shutdown");
                Ok(())
            },
            _ => Err(anyhow::Error::new(err)),
        });
    }
    Ok(())
}

fn server_prog(app_root: PathBuf,server_port_str: &str) -> anyhow::Result<std::process::Command> {
    let server_path = app_root.join("target").join("debug").join("server");
    let mut server_prog = std::process::Command::new(server_path);
    server_prog.args(&["--port", server_port_str]);
    info!("Server Command: {:?}", server_prog);
    server_prog.stdin(std::process::Stdio::null())
        .stdout::<std::process::Stdio>(std::fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(app_root.join("log").join("cubes_server_stdout.log"))?
            .into())
        .stderr::<std::process::Stdio>(std::fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(app_root.join("log").join("cubes_server_stderr.log"))?
            .into());
    Ok(server_prog)
}

//const ACQUIRE_S2C_THRESHOLD: u8 = 10;
//fn acquire_s2c_consumer<'a, S: AsRef<str>>(os_id: S) -> anyhow::Result<Consumer<'a, ServerProtocol>> {
//    use shared_memory_queue::ShmemError;
//    let mut count = 0u8;
//    loop {
//        if count > ACQUIRE_S2C_THRESHOLD {
//            break;
//        }
//
//        match Consumer::new(os_id.as_ref()) {
//            Ok(cons) => return Ok(cons),
//            Err(ShmemError::MapOpenFailed(_)) => {
//                count += 1;
//                // Wait on server to boot up
//                std::thread::sleep(std::time::Duration::from_millis(100));
//                continue;
//            },
//            Err(err) => return Err(anyhow::Error::new(err)),
//        }
//    }
//
//    return Err(anyhow::Error::msg("Failed to acquire Server2Client Consumer in client startup"));
//}