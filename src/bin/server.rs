extern crate amethyst;
extern crate anyhow;
extern crate clap;
extern crate cubes_lib;
extern crate dirs;
extern crate fern;
extern crate morton_code;
extern crate parking_lot;
extern crate rayon;
extern crate shared_memory_queue;
extern crate tokio;
#[cfg(feature = "profiler")]
extern crate thread_profiler;

use amethyst::{
    core::{frame_limiter::FrameRateLimitStrategy, TransformBundle},
    network::simulation::laminar::*,
    prelude::*,
    shrev::EventChannel,
    utils::application_root_dir,
    CoreApplication, Result,
};

use clap::{Arg, App};
use crossbeam::{Sender, Receiver};
use cubes_lib::{
    dimension::DimensionConfig,
    protocol::{server::ServerProtocol, client::ClientProtocol, WithAddr},
    player::PlayerAddr,
    states::ServerDimensionState,
    systems::{
        dimension::DimensionChunkEvent,
        net::{
            ServerLocalChannelToLaminarSystemDesc, ServerLaminarToLocalChannelSystemDesc,
            server::*,
        },
    }, 
};

use log::*;
use std::path::PathBuf;
use tokio::runtime::Runtime;

fn build_server<'a, 'b>(
    resources: PathBuf,
    port: u16,
) -> Result<CoreApplication<'static, GameData<'a, 'b>>> 
{
    let (local_sender, local_receiver): (Sender<(PlayerAddr, ServerProtocol)>, Receiver<(PlayerAddr, ServerProtocol)>) = crossbeam::channel::bounded(1024);
    let (chunk_send, chunk_recv): (Sender<DimensionChunkEvent>, Receiver<DimensionChunkEvent>) = crossbeam::channel::bounded(1024);
    let (receive_sender, recieve_receiver): (Sender<WithAddr<ClientProtocol>>, Receiver<WithAddr<ClientProtocol>>) = crossbeam::channel::bounded(1024);
    
    let dimension_dir = resources.join("dimension");

    let socket = LaminarSocket::bind(("127.0.0.1", port))?;

    let game_data = GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(LaminarNetworkBundle::new(Some(socket)))?
        .with_system_desc(ServerLaminarToLocalChannelSystemDesc::default(), "server_laminar_to_local", &[])
        .with_bundle(ServerNetBundle::default())?
        .with_system_desc(ServerLocalChannelToLaminarSystemDesc::default(), "server_local_to_laminar", &[SERVER_SENDER]);

    Application::build(
        &resources,
        ServerDimensionState::new(
            DimensionConfig::new(dimension_dir, 2).load_chunks_from_file(true),
        ),
    )?
    .with_resource(Runtime::new().unwrap())
    .with_resource(EventChannel::<DimensionChunkEvent>::new())
    .with_resource(local_sender)
    .with_resource(local_receiver)
    .with_resource(chunk_send)
    .with_resource(chunk_recv)
    .with_resource(receive_sender)
    .with_resource(recieve_receiver)
    .with_frame_limit(FrameRateLimitStrategy::Yield, 60)
    .build(game_data)
}

fn main() -> anyhow::Result<()> {
    let matches = App::new("Procedural Lithification Server")
        .version("0.1")
        .author("thunderseethe <thunderseethe.dev@gmail.com>")
        .about("It's a server, for proc_lith")
        .arg(Arg::with_name("port")
            .long("port")
            .help("The port to bind local net socket used for IPC")
            .takes_value(true))
        .get_matches();

    let port_str = matches.value_of("port")
        .ok_or_else(|| anyhow::Error::msg("Expected --port flag to be set"))?;
    let port = u16::from_str_radix(port_str, 10)?;
    
    let app_root = application_root_dir().map_err(|err| {
        error!("Failed to get application root dir");
        anyhow::Error::new(err)
    })?;

    cubes_lib::configure_logger(app_root.join("log").join("cubes_server.log")).map_err(|err| {
        error!("Failed to configure logger");
        anyhow::Error::new(err)
    })?;
    #[cfg(not(feature = "debug_resource_path"))]
    let resources = app_root.join("resources");
    #[cfg(feautre = "debug_resource_path")]
    let resources = app_root
        .parent()
        .unwrap()
        .parent()
        .unwrap()
        .join("resources");
    info!("RESOURCES: {:?}", resources);

    let mut server = build_server(
        resources, port).unwrap();

    server.run();
    
    Ok(())
}