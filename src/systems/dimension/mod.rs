use crate::dimension::ChunkMortonCode;
use amethyst::{
    core::{bundle::SystemBundle, ecs::DispatcherBuilder, SystemDesc},
    ecs::{Component, VecStorage},
    shred::World,
    Result,
};

pub mod render_dimension;
use crate::systems::dimension::render_dimension::RenderDimensionSystemDesc;

#[derive(Debug)]
pub enum DimensionChunkEvent {
    NewChunkAt(ChunkMortonCode),
}

pub struct ChunkTag(ChunkMortonCode);
impl Component for ChunkTag {
    type Storage = VecStorage<Self>;
}

#[derive(Default)]
pub struct DimensionBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for DimensionBundle {
    fn build(self, world: &mut World, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(
            RenderDimensionSystemDesc::default().build(world),
            "render_dimension",
            &[],
        );
        Ok(())
    }
}
