use super::{ChunkTag, DimensionChunkEvent};
use crate::{chunk::Chunk, dimension::DimensionRes, octree::descriptors::Diameter};
use amethyst::{
    assets::{AssetLoaderSystemData, AssetStorage, Handle, Loader},
    core::{math::Point3, SystemDesc, Transform},
    ecs::{Entities, Read, ReadExpect, System, SystemData, WriteStorage},
    renderer::{
        rendy::resource::{Filter, SamplerInfo, WrapMode},
        visibility::BoundingSphere,
        ImageFormat, Material, MaterialDefaults, Mesh, Texture,
    },
    shred::World,
};
use crossbeam::channel::Receiver;
use log::info;
#[cfg(feature = "profiler")]
use thread_profiler::profile_scope;

pub struct RenderDimensionSystem {
    albedo: Handle<Texture>,
    chunk_recv: Receiver<DimensionChunkEvent>,
}

#[derive(Default)]
pub struct RenderDimensionSystemDesc;
impl<'a, 'b> SystemDesc<'a, 'b, RenderDimensionSystem> for RenderDimensionSystemDesc {
    fn build(self, world: &mut World) -> RenderDimensionSystem {
        Read::<'_, AssetStorage<Texture>>::setup(world);
        
        let chunk_recv = (*world.fetch_mut::<Receiver<DimensionChunkEvent>>()).clone();

        let loader = world.fetch::<Loader>();
        let tex_storage = world.fetch();
        let mut image_format = ImageFormat::default();
        image_format.0.sampler_info = SamplerInfo::new(Filter::Linear, WrapMode::Tile);

        let albedo = loader.load("textures/dirt.png", image_format, (), &tex_storage);

        RenderDimensionSystem { albedo, chunk_recv }
    }
}

const SQRT_3: f32 = 1.732_050_8;
const CUBE_SIDE_LENGTH: usize = Chunk::DIAMETER;
const BOUNDING_SPHERE_RADIUS: f32 = SQRT_3 * CUBE_SIDE_LENGTH as f32;

impl<'a> System<'a> for RenderDimensionSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, MaterialDefaults>,
        ReadExpect<'a, DimensionRes>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Handle<Mesh>>,
        WriteStorage<'a, Handle<Material>>,
        WriteStorage<'a, ChunkTag>,
        WriteStorage<'a, BoundingSphere>,
        AssetLoaderSystemData<'a, Mesh>,
        AssetLoaderSystemData<'a, Material>,
    );

    fn run(
        &mut self,
        (
            entities,
            material_defaults,
            dimension,
            mut transforms,
            mut meshes,
            mut materials,
            mut chunk_tags,
            mut bounding_sphere,
            mesh_loader,
            material_loader,
        ): Self::SystemData,
    ) {
        #[cfg(feature = "profiler")]
        profile_scope!("render_dimension_system");
        for event in self.chunk_recv.try_iter() {
            match event {
                DimensionChunkEvent::NewChunkAt(morton) => {
                    if let Some((p, mesh_data)) = dimension
                        .get_chunk(morton)
                        .and_then(|chunk_mutex| {
                            let chunk = chunk_mutex.read();
                            chunk.generate_mesh()
                        })
                    {
                        let mut pos = Transform::default();
                        info!("Render Point: {}", p);
                        pos.set_translation_xyz(p.x, p.y, p.z);
                        let center = Point3::new(
                            p.x + CUBE_SIDE_LENGTH as f32,
                            p.y + CUBE_SIDE_LENGTH as f32,
                            p.z + CUBE_SIDE_LENGTH as f32,
                        );
                        entities
                            .build_entity()
                            .with(ChunkTag(morton), &mut chunk_tags)
                            .with(pos, &mut transforms)
                            .with(
                                mesh_loader.load_from_data(mesh_data.clone(), ()),
                                &mut meshes,
                            )
                            .with(
                                material_loader.load_from_data(
                                    Material {
                                        albedo: self.albedo.clone(),
                                        ..material_defaults.0.clone()
                                    },
                                    (),
                                ),
                                &mut materials,
                            )
                            .with(
                                BoundingSphere::new(center, BOUNDING_SPHERE_RADIUS),
                                &mut bounding_sphere,
                            )
                            .build();
                    }
                }
            }
        }
    }
}