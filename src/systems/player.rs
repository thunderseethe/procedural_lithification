use crate::{
    collision::{CollisionDetection, CollisionId},
    input::Action,
    player::*,
};
use amethyst::{
    controls::{CursorHideSystem, HideCursor, MouseFocusUpdateSystemDesc, WindowFocus},
    core::{
        bundle::SystemBundle,
        math::{Point3, Unit, Vector3},
        shrev::{EventChannel, ReaderId},
        Time, Transform,
    },
    derive::SystemDesc,
    ecs::{
        DispatcherBuilder, Entity, Join, Read, ReadStorage, System, SystemData, Write, WriteExpect,
        WriteStorage,
    },
    input::{get_input_axis_simple, BindingTypes, InputHandler},
    prelude::SystemDesc,
    shred::World,
    ui::{UiFinder, UiText},
    winit::{DeviceEvent, Event},
    Result,
};
use derive_new::new;
use std::marker::PhantomData;
#[cfg(feature = "profiler")]
use thread_profiler::profile_scope;

struct PlayerInputSystem<T>
where
    T: BindingTypes,
{
    // The name of the input axis to locally move in the x coordinates.
    right_input_axis: Option<T::Axis>,
    // The name of the input axis to locally move in the y coordinates.
    up_input_axis: Option<T::Axis>,
    // The name of the input axis to locally move in the z coordinates.
    forward_input_axis: Option<T::Axis>,
    _marker: std::marker::PhantomData<T::Action>,
}
impl<T> PlayerInputSystem<T>
where
    T: BindingTypes,
{
    pub fn new(
        right_input_axis: Option<T::Axis>,
        up_input_axis: Option<T::Axis>,
        forward_input_axis: Option<T::Axis>,
    ) -> Self {
        PlayerInputSystem {
            right_input_axis,
            up_input_axis,
            forward_input_axis,
            _marker: PhantomData,
        }
    }
}
impl<'a, T> System<'a> for PlayerInputSystem<T>
where
    T: BindingTypes,
{
    type SystemData = (Read<'a, InputHandler<T>>, Write<'a, EventChannel<Action>>);

    fn run(&mut self, (input, mut action_chan): Self::SystemData) {
        #[cfg(feature = "profiler")]
        profile_scope!("player_input_system");

        let x = get_input_axis_simple(&self.right_input_axis, &input);
        let y = get_input_axis_simple(&self.up_input_axis, &input);
        let z = get_input_axis_simple(&self.forward_input_axis, &input);

        if let Some(direction) = Unit::try_new(Vector3::new(x, y, z), std::f32::EPSILON) {
            action_chan.single_write(Action::Move(direction));
        }
    }
}

#[derive(SystemDesc, new)]
#[system_desc(name(PlayerMovementSystemDesc))]
struct PlayerMovementSystem {
    speed: f32,
    #[system_desc(event_channel_reader)]
    action_read_id: ReaderId<Action>,
}
impl<'a> System<'a> for PlayerMovementSystem {
    type SystemData = (
        Read<'a, Time>,
        WriteExpect<'a, CollisionDetection>,
        WriteStorage<'a, Transform>,
        ReadStorage<'a, CollisionId>,
        Read<'a, EventChannel<Action>>,
        ReadStorage<'a, PlayerControlTag>,
    );

    fn run(
        &mut self,
        (time, mut collision, mut transform, collision_id, action_chan, tag): Self::SystemData,
    ) {
        #[cfg(feature = "profiler")]
        profile_scope!("player_movement_system");

        for action in action_chan.read(&mut self.action_read_id) {
            for (transform, collision_id, _) in (&mut transform, &collision_id, &tag).join() {
                if let Action::Move(direction) = action {
                    transform
                        .append_translation_along(*direction, time.delta_seconds() * self.speed);
                    collision
                        .update_pos(collision_id.0, Point3::from(*transform.translation()))
                        .expect("Player exists in CollissionWorld");
                }
            }
        }
    }
}

struct PlayerRotationSystemDesc<T> {
    sensitivity_x: f32,
    sensitivity_y: f32,
    _marker: PhantomData<T>,
}
impl<T> PlayerRotationSystemDesc<T> {
    fn new(sensitivity_x: f32, sensitivity_y: f32) -> Self {
        PlayerRotationSystemDesc {
            sensitivity_x,
            sensitivity_y,
            _marker: std::marker::PhantomData,
        }
    }
}
impl<T> SystemDesc<'_, '_, PlayerRotationSystem<T>> for PlayerRotationSystemDesc<T>
where
    T: BindingTypes,
{
    fn build(self, world: &mut World) -> PlayerRotationSystem<T> {
        let mut event_channel = world.fetch_mut::<EventChannel<Event>>();
        PlayerRotationSystem {
            sensitivity_x: self.sensitivity_x,
            sensitivity_y: self.sensitivity_y,
            event_reader: event_channel.register_reader(),
            _marker: std::marker::PhantomData,
        }
    }
}

struct PlayerRotationSystem<T> {
    sensitivity_x: f32,
    sensitivity_y: f32,
    _marker: PhantomData<T>,
    event_reader: ReaderId<Event>,
}

impl<'a, T> System<'a> for PlayerRotationSystem<T>
where
    T: BindingTypes,
{
    type SystemData = (
        Read<'a, EventChannel<Event>>,
        WriteStorage<'a, Transform>,
        ReadStorage<'a, PlayerControlTag>,
        Read<'a, WindowFocus>,
        Read<'a, HideCursor>,
    );

    fn run(&mut self, (events, mut transform, tag, focus, hide): Self::SystemData) {
        #[cfg(feature = "profiler")]
        profile_scope!("player_rotation_system");

        let focused = focus.is_focused;
        for event in events.read(&mut self.event_reader) {
            if focused && hide.hide {
                if let Event::DeviceEvent { ref event, .. } = *event {
                    if let DeviceEvent::MouseMotion { delta: (x, y) } = *event {
                        for (transform, _) in (&mut transform, &tag).join() {
                            transform.append_rotation_x_axis(
                                (-y as f32 * self.sensitivity_y).to_radians(),
                            );
                            transform.prepend_rotation_y_axis(
                                (-x as f32 * self.sensitivity_x).to_radians(),
                            );
                        }
                    }
                }
            }
        }
    }
}

struct DrawPlayerPositionSystem {
    position_text: Option<Entity>,
    rotation_text: Option<Entity>,
}
impl DrawPlayerPositionSystem {
    fn new() -> Self {
        DrawPlayerPositionSystem {
            position_text: None,
            rotation_text: None,
        }
    }
}
impl<'a> System<'a> for DrawPlayerPositionSystem {
    type SystemData = (
        ReadStorage<'a, PlayerControlTag>,
        ReadStorage<'a, Transform>,
        WriteStorage<'a, UiText>,
        UiFinder<'a>,
        Read<'a, Time>,
    );

    fn run(&mut self, (player_tag, transforms, mut ui_text, ui_finder, time): Self::SystemData) {
        #[cfg(feature = "profiler")]
        profile_scope!("draw_player_position_system");

        if self.position_text.is_none() {
            if let Some(entity) = ui_finder.find("position_text") {
                self.position_text = Some(entity);
            }
        }
        if self.rotation_text.is_none() {
            if let Some(entity) = ui_finder.find("rotation_text") {
                self.rotation_text = Some(entity);
            }
        }
        for (transform, _) in (&transforms, &player_tag).join() {
            if time.frame_number() % 20 == 0 {
                if let Some(position_display) = self.position_text.and_then(|e| ui_text.get_mut(e))
                {
                    let t = transform.translation();
                    position_display.text = format!("{:.0}, {:.0}, {:.0}", t.x, t.y, t.z);
                }
                if let Some(rotation_display) = self.rotation_text.and_then(|e| ui_text.get_mut(e))
                {
                    let r = transform.rotation();
                    if let Some(axis) = r.axis() {
                        let (x, y, z) = (axis.x, axis.y, axis.z);
                        rotation_display.text = format!("{}, {{{}, {}, {}}}", r.angle(), x, y, z);
                    }
                }
            }
        }
    }
}

pub struct PlayerControlBundle<T>
where
    T: BindingTypes,
{
    sensitivity_x: f32,
    sensitivity_y: f32,
    speed: f32,
    right_input_axis: Option<T::Axis>,
    up_input_axis: Option<T::Axis>,
    forward_input_axis: Option<T::Axis>,
    _marker: PhantomData<T::Action>,
}

impl<T> PlayerControlBundle<T>
where
    T: BindingTypes,
{
    pub fn new(
        right_input_axis: Option<T::Axis>,
        up_input_axis: Option<T::Axis>,
        forward_input_axis: Option<T::Axis>,
    ) -> Self {
        PlayerControlBundle {
            sensitivity_x: 1.0,
            sensitivity_y: 1.0,
            speed: 1.0,
            right_input_axis,
            up_input_axis,
            forward_input_axis,
            _marker: PhantomData,
        }
    }

    pub fn with_sensitivity(mut self, x: f32, y: f32) -> Self {
        self.sensitivity_x = x;
        self.sensitivity_y = y;
        self
    }

    pub fn with_speed(mut self, speed: f32) -> Self {
        self.speed = speed;
        self
    }
}

impl<'a, 'b, T> SystemBundle<'a, 'b> for PlayerControlBundle<T>
where
    T: BindingTypes,
{
    fn build(self, world: &mut World, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(
            PlayerInputSystem::<T>::new(
                self.right_input_axis,
                self.up_input_axis,
                self.forward_input_axis,
            ),
            "player_input",
            &[],
        );
        builder.add(
            PlayerMovementSystemDesc::new(self.speed).build(world),
            "player_movement",
            &["player_input"],
        );
        builder.add(
            PlayerRotationSystemDesc::<T>::new(self.sensitivity_x, self.sensitivity_y).build(world),
            "player_rotation",
            &[],
        );
        builder.add(
            MouseFocusUpdateSystemDesc::default().build(world),
            "mouse_focus",
            &["player_rotation"],
        );
        builder.add(CursorHideSystem::new(), "cursor_hide", &["mouse_focus"]);
        builder.add(DrawPlayerPositionSystem::new(), "draw_player_position", &[]);
        Ok(())
    }
}
