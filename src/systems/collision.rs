use crate::{
    collision::CollisionDetection, dimension::DimensionRes, player::PlayerControlTag,
    systems::dimension::DimensionChunkEvent,
};
use amethyst::{
    core::{Transform, SystemDesc},
    ecs::{ReadExpect, ReadStorage, System, WriteExpect, WriteStorage},
    shred::World,
};
use crossbeam::channel::Receiver;
#[cfg(feature = "profiler")]
use thread_profiler::profile_scope;

#[derive(Default)]
pub struct CheckPlayerCollisionSystem;
impl<'a> System<'a> for CheckPlayerCollisionSystem {
    type SystemData = (
        WriteExpect<'a, CollisionDetection>,
        WriteStorage<'a, Transform>,
        ReadStorage<'a, PlayerControlTag>,
    );

    fn run(&mut self, (mut collision, _, _): Self::SystemData) {
        #[cfg(feature = "profiler")]
        profile_scope!("check_player_collision_system");

        collision.update();
        for event in collision.proximity_events() {
            println!("{:?}", event);
        }
    }
}

pub struct ChunkCollisionManagementSystemDesc;
impl<'a, 'b> SystemDesc<'a, 'b, ChunkCollisionManagementSystem> for ChunkCollisionManagementSystemDesc {
    fn build(self, world: &mut World) -> ChunkCollisionManagementSystem {
        let chunk_recv: Receiver<DimensionChunkEvent> = (*world.fetch::<Receiver<DimensionChunkEvent>>()).clone();
        ChunkCollisionManagementSystem {
            chunk_recv,
        }
    }
}
pub struct ChunkCollisionManagementSystem {
    chunk_recv: Receiver<DimensionChunkEvent>,
}
impl<'a> System<'a> for ChunkCollisionManagementSystem {
    type SystemData = (
        ReadExpect<'a, DimensionRes>,
        WriteExpect<'a, CollisionDetection>,
    );

    fn run(&mut self, (dimension, mut collision): Self::SystemData) {
        #[cfg(feature = "profiler")]
        profile_scope!("chunk_collision_management_system");
        for event in self.chunk_recv.try_iter() {
            match event {
                DimensionChunkEvent::NewChunkAt(morton) => {
                    if let Some(chunk_mutex) = dimension.get_chunk(morton) {
                        collision
                            .add_chunk(&chunk_mutex.read())
                            .unwrap_or_else(|err| {
                                println!(
                                    "Encountered error adding chunk to collision detection: {:?}",
                                    err
                                );
                            });
                    }
                }
            }
        }
    }
}
