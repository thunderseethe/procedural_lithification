use super::BlockUnderCursor;
use crate::{chunk::cube_mesh, player::PlayerControlTag};
use amethyst::{
    assets::{AssetLoaderSystemData, Handle},
    core::{Hidden, Transform},
    ecs::{Entities, Entity, Join, ReadStorage, System, SystemData, WriteStorage},
    prelude::SystemDesc,
    renderer::{
        loaders::{load_from_linear_rgba, load_from_srgba},
        mtl::TextureOffset,
        palette::rgb::{LinSrgba, Srgba},
        Material, Mesh, Texture,
    },
    shred::World,
};
use log::*;
#[cfg(feature = "profiler")]
use thread_profiler::profile_scope;

pub struct HighlightCursorSystem {
    highlight_entity: Entity,
}

#[derive(Default)]
pub struct HighlightCursorSystemDesc;
impl<'a, 'b> SystemDesc<'a, 'b, HighlightCursorSystem> for HighlightCursorSystemDesc {
    fn build(self, world: &mut World) -> HighlightCursorSystem {
        type SystemDescData<'a> = (
            Entities<'a>,
            WriteStorage<'a, Transform>,
            WriteStorage<'a, Hidden>,
            WriteStorage<'a, Handle<Mesh>>,
            WriteStorage<'a, Handle<Material>>,
            AssetLoaderSystemData<'a, Texture>,
            AssetLoaderSystemData<'a, Material>,
            AssetLoaderSystemData<'a, Mesh>,
        );
        SystemDescData::setup(world);

        let (
            entities,
            mut transform,
            mut hidden,
            mut mesh_store,
            mut material_store,
            tex_loader,
            material_loader,
            mesh_loader,
        ) = SystemDescData::fetch(&world);
        let mesh = mesh_loader.load_from_data(cube_mesh(MARGIN.mul_add(2., 1.0)).into(), ());
        let color =
            tex_loader.load_from_data(load_from_srgba(Srgba::new(1.0, 0.0, 0.0, 0.5)).into(), ());
        let mtl_default = create_default_mat(world);
        let material = Material {
            albedo: color,
            ..mtl_default
        };
        let highlight_entity = entities
            .build_entity()
            .with(Transform::default(), &mut transform)
            .with(Hidden::default(), &mut hidden)
            .with(mesh, &mut mesh_store)
            .with(
                material_loader.load_from_data(material, ()),
                &mut material_store,
            )
            .build();

        HighlightCursorSystem { highlight_entity }
    }
}

const MARGIN: f32 = 0.05;
impl<'a> System<'a> for HighlightCursorSystem {
    type SystemData = (
        ReadStorage<'a, PlayerControlTag>,
        ReadStorage<'a, BlockUnderCursor>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Hidden>,
    );

    fn run(&mut self, (tag, buc, mut transform_store, mut hidden_store): Self::SystemData) {
        #[cfg(feature = "profiler")]
        profile_scope!("highlight_cursor_system");

        let entity = self.highlight_entity;
        hidden_store.insert(entity, Hidden).unwrap_or_else(|err| {
            error!("Error inserting Hiddent for entity {:?}: {:?}", entity, err);
            None
        });
        for (_, buc) in (&tag, &buc).join() {
            if let Some(transform) = transform_store.get_mut(entity) {
                transform.set_translation_xyz(buc.0.x - MARGIN, buc.0.y - MARGIN, buc.0.z - MARGIN);
                hidden_store.remove(entity);
            };
        }
    }
}

// Courtesy of amethyst_renderer/src/system.rs
fn create_default_mat(res: &World) -> Material {
    use amethyst::assets::Loader;

    let loader = res.fetch::<Loader>();

    let albedo = load_from_srgba(Srgba::new(0.5, 0.5, 0.5, 1.0));
    let emission = load_from_srgba(Srgba::new(0.0, 0.0, 0.0, 0.0));
    let normal = load_from_linear_rgba(LinSrgba::new(0.5, 0.5, 1.0, 1.0));
    let metallic_roughness = load_from_linear_rgba(LinSrgba::new(0.0, 0.5, 0.0, 0.0));
    let ambient_occlusion = load_from_linear_rgba(LinSrgba::new(1.0, 1.0, 1.0, 1.0));
    let cavity = load_from_linear_rgba(LinSrgba::new(1.0, 1.0, 1.0, 1.0));

    let tex_storage = res.fetch();

    let albedo = loader.load_from_data(albedo.into(), (), &tex_storage);
    let emission = loader.load_from_data(emission.into(), (), &tex_storage);
    let normal = loader.load_from_data(normal.into(), (), &tex_storage);
    let metallic_roughness = loader.load_from_data(metallic_roughness.into(), (), &tex_storage);
    let ambient_occlusion = loader.load_from_data(ambient_occlusion.into(), (), &tex_storage);
    let cavity = loader.load_from_data(cavity.into(), (), &tex_storage);

    Material {
        alpha_cutoff: 0.01,
        albedo,
        emission,
        normal,
        metallic_roughness,
        ambient_occlusion,
        cavity,
        uv_offset: TextureOffset::default(),
    }
}
