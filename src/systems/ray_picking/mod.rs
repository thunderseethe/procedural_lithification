use crate::{
    chunk::Chunk, dimension::DimensionRes, nalgebra::PointExt, octree::octant::OctantFace,
    player::PlayerControlTag,
};
use amethyst::{
    core::{
        bundle::SystemBundle,
        math::{Isometry3, Point3, Vector3, Vector4},
        Transform,
    },
    ecs::{
        Component, DispatcherBuilder, Entities, HashMapStorage, Join, ReadExpect, ReadStorage,
        System, WriteStorage,
    },
    error::Error,
    prelude::SystemDesc,
    shred::World,
};

use ncollide3d::query::{Ray, RayCast};
use std::convert::TryFrom;

mod highlight;
use highlight::HighlightCursorSystemDesc;

pub struct BlockUnderCursor(Point3<f32>);
impl Component for BlockUnderCursor {
    // Consider swapping for a DenseVecStorage
    // We'll have one of these components for each player, which is on the order of 10s maybe maybe 100s
    type Storage = HashMapStorage<Self>;
}

#[derive(Default)]
pub struct CursorRayPickingSystem;

impl<'a> System<'a> for CursorRayPickingSystem {
    type SystemData = (
        ReadExpect<'a, DimensionRes>,
        Entities<'a>,
        ReadStorage<'a, PlayerControlTag>,
        ReadStorage<'a, Transform>,
        WriteStorage<'a, BlockUnderCursor>,
    );

    fn run(
        &mut self,
        (dimension, entities, tag, transform_store, mut buc_store): Self::SystemData,
    ) {
        for (entity, _, transform) in (&*entities, &tag, &transform_store).join() {
            let chunk_pos = Chunk::absl_to_chunk_coords(Point3::from(*transform.translation()));
            dimension
                .get_chunk(chunk_pos)
                .and_then(|chunk| {
                    let ray = {
                        let view_mat = transform.matrix();
                        let world_loc = view_mat * Point3::new(0.0, 0.0, -1.0).to_homogeneous();
                        let line_dir = Vector4::new(0.0, 0.0, -1.0, 0.0);
                        let world_dir = view_mat * line_dir;
                        Ray::new(
                            Point3::new(world_loc.x, world_loc.y, world_loc.z),
                            Vector3::new(world_dir.x, world_dir.y, world_dir.z),
                        )
                    };
                    let abs_chunk_pos: Point3<f32> = Chunk::chunk_to_absl_coords(chunk_pos);
                    let iso =
                        Isometry3::translation(abs_chunk_pos.x, abs_chunk_pos.y, abs_chunk_pos.z);
                    chunk
                        .read()
                        .toi_and_normal_with_ray(&iso, &ray, true)
                        .map(|inter| (ray, inter))
                })
                .map(|(ray, intersection)| {
                    let mut p = ray.origin + (intersection.toi * ray.dir);
                    let face = OctantFace::try_from(intersection.feature).unwrap();
                    match face {
                        OctantFace::Back => {
                            p.z -= 1.0;
                        }
                        OctantFace::Right => {
                            p.x -= 1.0;
                        }
                        OctantFace::Up => {
                            p.y -= 1.0;
                        }
                        _ => {}
                    }
                    let block_under_cursor = BlockUnderCursor(p.map(|coord| coord.floor()));
                    buc_store
                        .insert(entity, block_under_cursor)
                        .unwrap_or_else(|err| {
                            use log::error;
                            error!(
                                "Unable to insert BlockUnderCursor for entity {:?}: {:?}",
                                entity, err
                            );
                            None
                        });
                })
                .unwrap_or_else(|| {
                    buc_store.remove(entity);
                });
        }
    }
}

#[derive(Default)]
pub struct RayPickingBundle;
impl<'a, 'b> SystemBundle<'a, 'b> for RayPickingBundle {
    fn build(
        self,
        world: &mut World,
        dispatcher: &mut DispatcherBuilder<'a, 'b>,
    ) -> Result<(), Error> {
        dispatcher.add(CursorRayPickingSystem::default(), "ray_pick_cursor", &[]);
        dispatcher.add(
            HighlightCursorSystemDesc::default().build(world),
            "highlight_cursor",
            &[],
        );
        Ok(())
    }
}
