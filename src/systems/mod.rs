use amethyst::{
    core::ecs::{System, Entities, ReadStorage, Write, Join},
};
use crate::player::PlayerName;

pub mod collision;
pub mod dimension;
pub mod net;
pub mod player;
pub mod ray_picking;
pub mod shared_mem_net;

pub struct StopServer(bool);
impl StopServer {
    pub fn should_stop(&self) -> bool {
        self.0
    }
    fn set_stop(&mut self) {
        self.0 = true;
    }
}
impl Default for StopServer {
    fn default() -> Self {
        StopServer(false)
    }
}

#[derive(Default)]
pub struct PlayerDisconnectedStopServerSystem; 
impl<'s> System<'s> for PlayerDisconnectedStopServerSystem {
    type SystemData = (
        Entities<'s>,
        ReadStorage<'s, PlayerName>,
        Write<'s, StopServer>,
    );

    fn run(&mut self, (entities, player_names, mut stop_server): Self::SystemData) {
        if (&entities, &player_names).join().count() == 0 {
            stop_server.set_stop();
        }
    }
}