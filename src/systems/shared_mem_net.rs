use crate::protocol::{
    WithAddr, 
    client::ClientProtocol,
    server::ServerProtocol,
};

use amethyst::{
    core::SystemDesc,
    ecs::System,
    prelude::World,
    shred::{ReadExpect, WriteExpect},
};
use crossbeam::channel::{Sender, Receiver};
use log::{error, info};
use shared_memory_queue::{Producer, Consumer};

/**
 * CLIENT SHARED MEMORY SENDER
 */
pub struct ClientShmemSenderSystem<'a> {
    producer: Producer<'a, WithAddr<ClientProtocol>>,
    extra_buffer: Vec<WithAddr<ClientProtocol>>,
}
impl<'shmem> ClientShmemSenderSystem<'shmem> {
    pub fn new(producer: Producer<'shmem, WithAddr<ClientProtocol>>) -> Self {
        Self {
            producer,
            extra_buffer: Vec::new(),
        }
    }
}
impl<'s, 'shmem> System<'s> for ClientShmemSenderSystem<'shmem> {
    type SystemData = (
        ReadExpect<'s, Receiver<ClientProtocol>>,
    );

    fn run(&mut self, (channel,): Self::SystemData) {
        for msg in self.extra_buffer.drain(..) {
            if let Err(msg) = self.producer.enqueue(msg) {
                error!("Dropped a message: {:?}", msg);
                error!("Current queue: {:?}", self.producer);
            }
        }
        for evt in channel.try_iter() {
           let addr = crate::systems::net::localhost(12345).into();
           if let Err(msg) = self.producer.enqueue((addr, evt)) {
               self.extra_buffer.push(msg);
           }
        }
    }
}
pub struct ClientShmemSenderSystemDesc<'a> {
    producer: Producer<'a, WithAddr<ClientProtocol>>,
}
impl<'c> ClientShmemSenderSystemDesc<'c> {
    pub fn new(producer: Producer<'c, WithAddr<ClientProtocol>>) -> Self {
        Self {producer}
    }
}
impl<'a, 'b, 'c> SystemDesc<'a, 'b, ClientShmemSenderSystem<'c>> for ClientShmemSenderSystemDesc<'c> {
    fn build(self, _world: &mut World) -> ClientShmemSenderSystem<'c> {
        ClientShmemSenderSystem::new(self.producer)
    }
}

/**
 * CLIENT SHARED MEMORY RECEIVER
 */
pub struct ClientShmemReceiverSystem<'shmem> {
    consumer: Consumer<'shmem, ServerProtocol>,
}
impl<'shmem> ClientShmemReceiverSystem<'shmem> {
    pub fn new(consumer: Consumer<'shmem, ServerProtocol>) -> Self {
        Self { consumer }
    }
}
impl<'s, 'shmem> System<'s> for ClientShmemReceiverSystem<'shmem> {
    type SystemData = (
        WriteExpect<'s, Sender<ServerProtocol>>,
    );

    fn run(&mut self, (channel,): Self::SystemData) {
        while let Some(evt) = self.consumer.dequeue() {
            match channel.send(evt) {
                Ok(()) => {},
                Err(err) => {
                    error!("Failed to receive shared memory: {:?}", err);
                }
            }
        }
    }
}


/**
 * SERVER SHARED MEMORY SENDER
 */
pub struct ServerShmemSenderSystem<'shmem> {
    producer: Producer<'shmem, ServerProtocol>,
    extra_buffer: Vec<ServerProtocol>,
}
impl<'shmem> ServerShmemSenderSystem<'shmem> {
    pub fn new(producer: Producer<'shmem, ServerProtocol>) -> Self {
        Self {
            producer,
            extra_buffer: Vec::new(),
        }
    }
}
impl<'s, 'shmem> System<'s> for ServerShmemSenderSystem<'shmem> {
    type SystemData = (
        ReadExpect<'s, Receiver<WithAddr<ServerProtocol>>>,
    );

    fn run(&mut self, (channel,): Self::SystemData) {
        info!("Ran ServerShmemSenderSystem");
        for evt in self.extra_buffer.drain(..) {
            if let Err(evt) = self.producer.enqueue(evt) {
                error!("Dropped a message: {:?}", evt);
            }
        }
        for (_, evt) in channel.try_iter() {
            if let Err(evt) = self.producer.enqueue(evt) {
                self.extra_buffer.push(evt);
            }
        }
    }
}

//pub struct ServerShmemSenderSystemDesc<'shmem> {
//    producer: Producer<'shmem, ServerProtocol>,
//}
//impl<'shmem> ServerShmemSenderSystemDesc<'shmem> {
//    pub fn new(producer: Producer<'shmem, ServerProtocol>) -> Self {
//        Self { producer }
//    }
//}
//impl<'a, 'b, 'shmem> SystemDesc<'a, 'b, ServerShmemSenderSystem<'shmem>> for ServerShmemSenderSystemDesc<'shmem> {
//    fn build(self, world: &mut World) -> ServerShmemSenderSystem<'shmem> {
//        ServerShmemSenderSystem {
//            producer: self.producer,
//            extra_buffer: Vec::new(),
//        }
//    }
//}

/**
 * SERVER SHARED MEMORY RECEIVER
 */
pub struct ServerShmemReceiverSystem<'shmem> {
    consumer: Consumer<'shmem, WithAddr<ClientProtocol>>,
}
impl<'shmem> ServerShmemReceiverSystem<'shmem> {
    pub fn new(consumer: Consumer<'shmem, WithAddr<ClientProtocol>>) -> Self {
        Self { consumer }
    }
}
impl<'s, 'shmem> System<'s> for ServerShmemReceiverSystem<'shmem> {
    type SystemData = (
        WriteExpect<'s, Sender<WithAddr<ClientProtocol>>>,
    );

    fn run(&mut self, (channel,): Self::SystemData) {
        error!("Server receiver: {:#?}", self.consumer);
        while let Some(evt) = self.consumer.dequeue() {
            info!("consumed a message: {:?}", evt);
            channel.try_send(evt).expect("Failed to send message in ServerShmemReceiverSystem");
        }
    }
}