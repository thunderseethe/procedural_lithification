use crate::{
    protocol::{client::ClientProtocol, server::ServerProtocol, WithAddr},
    player::PlayerAddr,
};
use amethyst::{
    core::Transform,
    derive::SystemDesc,
    ecs::{Entities, Read, Write, ReadExpect, WriteExpect, WriteStorage, SystemData},
    network::simulation::{TransportResource, NetworkSimulationEvent},
    shred::System,
    shrev::*,
};
use crossbeam::channel::{Sender, Receiver};
use derive_new::new;
use flate2::bufread::{DeflateEncoder, DeflateDecoder};
use log::{info, error};
use std::{
    collections::HashSet,
    fmt::Debug,
    io::Read as IORead,
};

#[cfg(feature = "profiler")]
use thread_profiler::profile_scope;

pub mod client;
pub mod server;

pub struct ServerAddr(SocketAddr);
impl ServerAddr {
    pub fn new(addr: SocketAddr) -> Self {
        ServerAddr(addr)
    }
}

use std::net::{SocketAddr, IpAddr, Ipv4Addr};
#[inline]
pub fn localhost(port: u16) -> SocketAddr {
    SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), port)
}

#[derive(SystemDesc)]
#[system_desc(name(ServerLaminarToLocalChannelSystemDesc))]
pub struct ServerLaminarToLocalChannelSystem {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<NetworkSimulationEvent>,
    #[system_desc(skip)]
    connected_addrs: HashSet<SocketAddr>,
}
impl ServerLaminarToLocalChannelSystem {
    fn new(reader_id: ReaderId<NetworkSimulationEvent>) -> Self {
        Self {
            reader_id,
            connected_addrs: HashSet::new(),
        }
    }
}
impl<'s> System<'s> for ServerLaminarToLocalChannelSystem {
    type SystemData = (
        Entities<'s>,
        Read<'s, EventChannel<NetworkSimulationEvent>>,
        ReadExpect<'s, Sender<WithAddr<ClientProtocol>>>,
        WriteStorage<'s, Transform>,
        WriteStorage<'s, PlayerAddr>,
    );

    fn run(&mut self, (entities, recv, send_chan, mut transforms, mut player_addrs): Self::SystemData) {
        for event in recv.read(&mut self.reader_id).into_iter() {
            match event {
                NetworkSimulationEvent::Message(addr, bytes) => {
                    let mut buf = vec![];
                    DeflateDecoder::new(bytes.as_ref()).read_to_end(&mut buf).expect("Failed to compress");
                    match serde_cbor::from_slice(&buf[..]) {
                        Ok(msg) => {
                            send_chan.send((addr.into(), msg))
                                .expect("Failed to write client message to channel in server");
                        },
                        Err(err) => {
                            error!("Failed to deserialize client message in server: {:?}", err);
                        }
                    }
                },
                NetworkSimulationEvent::Connect(addr) => {
                    // We receive multiple connect requests from the same player addr so we deduplicate them here
                    if self.connected_addrs.contains(&addr) {
                        continue;
                    }
                    self.connected_addrs.insert(*addr);
                    entities
                        .build_entity()
                        .with(Transform::default(), &mut transforms)
                        .with(addr.into(), &mut player_addrs)
                        .build();
                },
                NetworkSimulationEvent::Disconnect(addr) => {
                    self.connected_addrs.remove(addr);
                },
                NetworkSimulationEvent::SendError(err, msg) => {
                    error!("Failed to send {:?}\n\twith error {:?}", msg, err);
                },
                NetworkSimulationEvent::RecvError(err) => {
                    error!("Failed to recv {:?}", err);
                },
                _ => {}, // pass for now
            }
        }
    }
}

#[derive(new, SystemDesc)]
#[system_desc(name(ServerLocalChannelToLaminarSystemDesc))]
pub struct ServerLocalChannelToLaminarSystem;
impl<'s> System<'s> for ServerLocalChannelToLaminarSystem {
    type SystemData = (
        ReadExpect<'s, Receiver<WithAddr<ServerProtocol>>>,
        Write<'s, TransportResource>,
    );

    fn run(&mut self, (recv_chan, mut net): Self::SystemData) {
        for (addr, event) in recv_chan.try_iter() {
            let buf = serde_cbor::to_vec(&event).expect("Failed to serialize server message");
            let mut compr_out: Vec<u8> = vec![];
            DeflateEncoder::new(&buf[..], flate2::Compression::best()).read_to_end(&mut compr_out).expect("Failed to compress server message");
            net.send(addr.into(), &compr_out[..]);
        }
    }
}


#[derive(new, SystemDesc)]
#[system_desc(name(ClientLaminarToLocalChannelSystemDesc))]
pub struct ClientLaminarToLocalChannelSystem {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<NetworkSimulationEvent>,
}
impl<'s> System<'s> for ClientLaminarToLocalChannelSystem {
    type SystemData = (
        Read<'s, EventChannel<NetworkSimulationEvent>>,
        WriteExpect<'s, Sender<ServerProtocol>>,
    );

    fn run(&mut self, (recv, send_chan): Self::SystemData) {
        for event in recv.read(&mut self.reader_id) {
            if let NetworkSimulationEvent::Message(_, bytes) = event {
                info!("Server message: {:?}", bytes.as_ref());
                let mut buf = vec![];
                DeflateDecoder::new(bytes.as_ref()).read_to_end(&mut buf).expect("Failed to decompress client message");
                match serde_cbor::from_slice(&buf[..]) {
                    Ok(msg) => {
                        info!("Received message from server: {:?}", msg);
                        send_chan.send(msg)
                            .expect("Failed to write server message to channel in client");
                    },
                    Err(err) => error!("Failed to deserialize server message in client: {:?}", err),
                }
            }
        }
    }
}

#[derive(new, SystemDesc)]
#[system_desc(name(ClientLocalChannelToLaminarChannelSystemDesc))]
pub struct ClientLocalChannelToLaminarChannelSystem;
impl<'s> System<'s> for ClientLocalChannelToLaminarChannelSystem {
    type SystemData = (
        ReadExpect<'s, Receiver<ClientProtocol>>,
        ReadExpect<'s, ServerAddr>,
        Write<'s, TransportResource>,
    );

    fn run(&mut self, (recv, server_addr, mut net): Self::SystemData) {
        for event in recv.try_iter().into_iter() {
            let err_msg = &format!("Failed to serialize client message {:?}", event);
            let buf = serde_cbor::to_vec(&event).expect(err_msg);
            let mut out = vec![];
            DeflateEncoder::new(&buf[..], flate2::Compression::best()).read_to_end(&mut out).expect("Failed to compress client message");
            net.send(server_addr.0, &out[..]);
        }
    }
}

// Attaches localhost player addr to all messages sent by player
//pub struct LocalhostNetworkClientSystem;
//impl<'s> System<'s> for LocalhostNetworkClientSystem {
//    type SystemData = (
//        ReadExpect<'s, Receiver<ClientProtocol>>,
//        WriteExpect<'s, Sender<(PlayerAddr, ClientProtocol)>>
//    );
//
//    fn run(&mut self, (source, dest): Self::SystemData) {
//        for msg in source.try_iter() {
//            dest.send((localhost().into(), msg)).expect("Coud not send message in LocalhostNetworkClientSystem");
//        }
//    }
//}

//#[derive(Default)]
//pub struct LocalhostNetworkClientSystemDesc;
//impl<'a, 'b> SystemDesc<'a, 'b, LocalhostNetworkClientSystem> for LocalhostNetworkClientSystemDesc {
//    // If you are using the Localhost system we insert an extra channel that adds an address to eaah method.
//    // We do this in the system desc so you only have this channel if you are using this system.
//    fn build(self, world: &mut World) -> LocalhostNetworkClientSystem {
//        let (sender, receiver): (Sender<WithAddr<ClientProtocol>>, Receiver<WithAddr<ClientProtocol>>) = crossbeam::channel::bounded(1024);
//        world.insert(sender);
//        world.insert(receiver);
//        LocalhostNetworkClientSystem
//    }
//}

// This is used for local server so we know there is only one player.
// Since our client and server aren't communicating over a socket we strip addr and forward all messages to a channel.
//pub struct LocalhostNetworkServerSystem;
//impl<'s> System<'s> for LocalhostNetworkServerSystem {
//    type SystemData = (
//        ReadExpect<'s, Receiver<(PlayerAddr, ServerProtocol)>>,
//        WriteExpect<'s, Sender<ServerProtocol>>,
//    );
//
//    fn run(&mut self, (source, dest): Self::SystemData) {
//        for (_, msg) in source.try_iter() {
//            dest.send(msg).expect("Could not send message in LocalhostNetworkServerSystem");
//        }
//    }
//}
