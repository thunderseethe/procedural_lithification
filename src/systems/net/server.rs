use crate::{
    dimension::{ChunkMortonCode, Dimension},
    player::{PlayerAddr},
    protocol::{
        WithAddr,
        ServerSenderSystemDesc, ServerReceiverSystem,
        server::ChunkAt,
        client::PlayerAction,
    },
    systems::dimension::DimensionChunkEvent, volume::Sphere,
};
use amethyst::{
    core::{bundle::SystemBundle, math::Point3, transform::Transform, Time, SystemDesc},
    derive::SystemDesc,
    ecs::{prelude::Entities, Read, Write, WriteStorage, ReadStorage, Join},
    shred::{DispatcherBuilder, System, SystemData, World},
    shrev::*,
};
use derive_new::new;
use crossbeam::channel::Receiver;
use itertools::Itertools;
use log::info;
#[cfg(feature = "profiler")]
use thread_profiler::profile_scope;

// Marker type for a new player. Right now this is just used to mark a new player addres using WithAddr<>
pub struct NewPlayer;

//#[derive(SystemDesc, new)]
//#[system_desc(name(ServerNewPlayerChunkSyncSystemDesc))]
//pub struct ServerNewPlayerChunkSyncSystem {
//    #[system_desc(event_channel_reader)]
//    reader: ReaderId<WithAddr<NewPlayer>>,
//}
//impl<'a> System<'a> for ServerNewPlayerChunkSyncSystem {
//    type SystemData = (
//        Read<'a, EventChannel<WithAddr<NewPlayer>>>,
//        Read<'a, Dimension>,
//        Write<'a, EventChannel<WithAddr<ChunkAt>>>,
//    );
//
//    fn run(&mut self, (new_player_evts, dimension, mut chunk_at_evts): Self::SystemData) {
//        let chunk_at_vec= new_player_evts
//            .read(&mut self.reader)
//            .flat_map(|(addr, _)| {
//                // TODO: Need a player render radius configured for the server
//                Sphere::new(Point3::origin(), 1)
//                    .into_iter()
//                    .map(|point| point.into())
//                    .filter_map(|morton: ChunkMortonCode| dimension.get_chunk(morton))
//                    .map(move |chunk| (addr, chunk))
//            })
//            .map(|(addr, chunk)| {
//                let (morton, packed) = { 
//                    let chunk_lock = chunk.read();
//                    (chunk_lock.pos.into(), chunk_lock.pack())
//                };
//                (*addr, ChunkAt {
//                    pos: morton,
//                    data: packed,
//                })
//            })
//            .collect::<Vec<_>>();
//
//        chunk_at_evts.iter_write(chunk_at_vec);
//    }
//}
pub struct ServerPlayerChunkSyncSystemDesc;
impl<'a, 'b> SystemDesc<'a, 'b, ServerPlayerChunkSyncSystem> for ServerPlayerChunkSyncSystemDesc {
    fn build(self, world: &mut World) -> ServerPlayerChunkSyncSystem {
        let chunk_recv: Receiver<DimensionChunkEvent> = (*world.fetch::<Receiver<DimensionChunkEvent>>()).clone();
        ServerPlayerChunkSyncSystem {
            chunk_recv
        }
    }
}

pub struct ServerPlayerChunkSyncSystem {
    chunk_recv: Receiver<DimensionChunkEvent>, 
}
impl<'a> System<'a> for ServerPlayerChunkSyncSystem {
    type SystemData = (
        Read<'a, Dimension>,
        ReadStorage<'a, PlayerAddr>,
        Write<'a, EventChannel<WithAddr<ChunkAt>>>,
    );

    fn run(&mut self, (dimension, player_addrs, mut chunk_at_evts): Self::SystemData) {
        for evt in self.chunk_recv.try_iter() {
            let DimensionChunkEvent::NewChunkAt(morton) = evt;
            if let Some(chunk) = dimension.get_chunk(morton) {
                let packed = { chunk.read().pack() };
                let point: Point3<i32> = morton.into();
                for (addr,) in (&player_addrs,).join() {
                    info!("Synced point {} to player {:?}", point, addr);
                    chunk_at_evts.single_write((*addr, ChunkAt {
                        pos: morton,
                        data: packed.clone(),
                    }));
                }
            }
        }
    }
}

#[derive(SystemDesc, new)]
#[system_desc(name(ServerPlayerActionSystemDesc))]
pub struct ServerPlayerActionSystem {
    #[system_desc(event_channel_reader)]
    reader: ReaderId<WithAddr<PlayerAction>>,
}
impl<'a> System<'a> for ServerPlayerActionSystem {
    type SystemData = (
        Read<'a, Time>,
        Read<'a, EventChannel<WithAddr<PlayerAction>>>, 
        ReadStorage<'a, PlayerAddr>,
        WriteStorage<'a, Transform>,
    );

    fn run(&mut self, (time, player_actions, player_addrs, mut transforms): Self::SystemData) {
        use crate::input::Action;
        use std::collections::HashMap;
        let actions: HashMap<PlayerAddr, Vec<&PlayerAction>> = 
            player_actions.read(&mut self.reader)
                .group_by(|t| t.0)
                .into_iter()
                .map(|(addr, actions)| 
                    (addr, actions.map(|t| &t.1).collect::<Vec<_>>()))
                .collect();
        
        for (addr, transform) in (&player_addrs, &mut transforms).join() {
            if let Some(actions) = actions.get(addr) {
                for action in actions {
                    if let Action::Move(direction) = action.action {
                        // This is bad
                        let speed: f32 = 16.0;
                        transform.append_translation_along(direction, time.delta_seconds() * speed);
                    }
                }
            }
        }
    }
}

//#[derive(SystemDesc, new)]
//#[system_desc(name(ServerPlayerDisconnectedSystemDesc))]
//pub struct ServerPlayerDisconnectedSystem {
//   #[system_desc(event_channel_reader)]
//   reader: ReaderId<WithAddr<PlayerDisconnected>>,
//}
//impl<'s> System<'s> for ServerPlayerDisconnectedSystem {
//    type SystemData = (
//        Entities<'s>,
//        Read<'s, EventChannel<WithAddr<PlayerDisconnected>>>,
//        ReadStorage<'s, PlayerName>,
//    );
//
//    fn run(&mut self, (entities, player_disconnected, names): Self::SystemData) {
//        use std::collections::HashSet;
//
//        let disconnected_players: HashSet<PlayerName> = player_disconnected.read(&mut self.reader).map(|(_, msg)| msg.name.clone()).collect();
//        for (ent, _) in (&entities, &names).join().filter(|(_, name)| disconnected_players.contains(name)) {
//            if let Err(_) = entities.delete(ent) {
//                info!("ServerPlayerDisconnectedSystemDesc tried to delete stale entities");
//            };
//        }
//    }
//}

pub const SERVER_SENDER: &str = "server_sender";
pub const SERVER_RECEIVER: &str = "server_receiver";

#[derive(Default)]
pub struct ServerNetBundle;
impl<'a, 'b> SystemBundle<'a, 'b> for ServerNetBundle {
    fn build(
        self,
        world: &mut World,
        dispatcher: &mut DispatcherBuilder<'a, 'b>,
    ) -> amethyst::Result<()> {
        dispatcher.add(
            ServerPlayerChunkSyncSystemDesc.build(world),
            "server_player_chunk_sync",
            &[],
        );
        // Handles sending all messages, this should rely on all previous systems that generate messages
        dispatcher.add(
            ServerSenderSystemDesc::default().build(world),
            SERVER_SENDER,
            &["server_new_player_chunk", "server_player_chunk_sync"],
        );

        // Receiving Systems
        dispatcher.add(
            ServerReceiverSystem::default(),
            SERVER_RECEIVER,
            &["server_laminar_to_local"],
        );
        dispatcher.add(
            ServerPlayerActionSystemDesc::default().build(world),
            "server_player_action",
            &[SERVER_RECEIVER],
        );
        Ok(())
    }
}
