use crate::input::Action;
use crate::protocol::{
    ClientReceiverSystem, ClientSenderSystemDesc,
    client::PlayerAction
};
use crate::{chunk::Chunk, dimension::Dimension, systems::dimension::DimensionChunkEvent};
use amethyst::{
    core::{bundle::SystemBundle, timing::Time, SystemDesc},
    derive::SystemDesc,
    ecs::{Read, System, SystemData, World, Write, WriteExpect},
    shred::DispatcherBuilder,
    shrev::*,
};
use crossbeam::channel::Sender;
use derive_new::new;
use log::info;

#[cfg(feature = "profiler")]
use thread_profiler::profile_scope;

#[derive(SystemDesc, new)]
#[system_desc(name(ClientSendActionSystemDesc))]
pub struct ClientSendActionSystem {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<Action>,
}
impl<'a> System<'a> for ClientSendActionSystem {
    type SystemData = (
        Read<'a, EventChannel<Action>>,
        Read<'a, Time>,
        Write<'a, EventChannel<PlayerAction>>,
    );

    fn run(&mut self, (actions, _time, mut action_packets): Self::SystemData) {
        action_packets.iter_write(actions.read(&mut self.reader_id).map(PlayerAction::from))
    }
}

use crate::protocol::server::ChunkAt;

#[derive(SystemDesc, new)]
#[system_desc(name(ClientDimensionSyncSystemDesc))]
pub struct ClientDimensionSyncSystem {
    #[system_desc(event_channel_reader)]
    chunk_at_reader: ReaderId<ChunkAt>,
}
impl<'a> System<'a> for ClientDimensionSyncSystem {
    type SystemData = (
        Read<'a, EventChannel<ChunkAt>>,
        WriteExpect<'a, Dimension>,
        WriteExpect<'a, Sender<DimensionChunkEvent>>,
    );

    fn run(&mut self, (chunk_packets, dimension, dimension_evts): Self::SystemData) {
        #[cfg(feature = "profiler")]
        profile_scope!("client_dimension_sync_system");

        for chunk_at in chunk_packets.read(&mut self.chunk_at_reader) {
            info!("ChunkAt: {:?}", chunk_at);
            let ChunkAt { pos, data } = chunk_at;
            let chunk = Chunk::unpack(data.clone(), (*pos).into())
                .expect("Unable to unpack chunk received from server.");

            dimension.insert(*pos, chunk);
            info!("Chunk inserted");
            dimension_evts.try_send(DimensionChunkEvent::NewChunkAt(*pos)).expect("Failed to send chunk event in ClientDimensionSyncSystem");
            info!("Chunk sent");
        }
    }
}

pub const CLIENT_SENDER: &str = "client_sender";
pub const CLIENT_RECEIVER: &str = "client_receiver";

#[derive(Default)]
pub struct ClientNetBundle;
impl<'a, 'b> SystemBundle<'a, 'b> for ClientNetBundle {
    fn build(
        self,
        world: &mut World,
        dispatcher: &mut DispatcherBuilder<'a, 'b>,
    ) -> amethyst::Result<()> {
        // Sending systems
        dispatcher.add(
            ClientSendActionSystemDesc::default().build(world),
            "client_send_action",
            &[],
        );
        dispatcher.add(
            ClientSenderSystemDesc::default().build(world),
            CLIENT_SENDER,
            &["client_send_action"],
        );

        // Receiving systems
        dispatcher.add(
            ClientReceiverSystem::default(),
            CLIENT_RECEIVER,
            &["client_laminar_to_local"],
        );
        dispatcher.add(
            ClientDimensionSyncSystemDesc::default().build(world),
            "client_dimension_sync",
            &[CLIENT_RECEIVER],
        );
        Ok(())
    }
}
