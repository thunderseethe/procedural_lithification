use crate::{
    chunk::Chunk,
    dimension::ChunkMortonCode,
    input::Action,
    octree::{pack::PackedOctree, ElementOf},
    player::{PlayerAddr},
};
use amethyst::core::math::Point3;
use derive_new::new;
use generate_protocol::generate_protocol;
use serde::{Deserialize, Serialize};

generate_protocol! {
    name Protocol,
    #[derive(Serialize, Deserialize)]
    server {
        #[derive(Serialize, Deserialize)]
        struct ChunkAt {
            pos: ChunkMortonCode,
            data: PackedOctree<ElementOf<Chunk>>,
        }

        #[derive(Serialize, Deserialize)]
        struct PlayerPosSync {
            pos: Point3<f32>,
        }
    }
    #[derive(Serialize, Deserialize)]
    client {
        #[derive(Serialize, Deserialize)]
        struct PlayerAction {
            action: Action
        }
        impl From<Action> for PlayerAction {
            fn from(action: Action) -> Self {
                PlayerAction { action }
            }
        }
        impl From<&Action> for PlayerAction {
            fn from(action: &Action) -> Self {
                PlayerAction { action: (*action).clone() }
            }
        }
    }
}

use amethyst::{
    core::{
        ecs::{Read, ReadExpect, System, SystemData, Write, WriteExpect},
        shrev::{EventChannel, ReaderId},
    },
    derive::SystemDesc,
};
use crossbeam::channel::{Receiver, Sender};

#[derive(SystemDesc, new)]
#[system_desc(name(ClientSenderSystemDesc))]
pub struct ClientSenderSystem {
    #[system_desc(event_channel_reader)]
    player_action_reader_id: ReaderId<client::PlayerAction>,
}
impl<'s> System<'s> for ClientSenderSystem {
    type SystemData = (
        Read<'s, EventChannel<client::PlayerAction>>,
        WriteExpect<'s, Sender<client::ClientProtocol>>,
    );

    fn run(
        &mut self,
        (player_action_chan, client_chan): Self::SystemData,
    ) {
        let player_action_reqs =
            player_action_chan.read(&mut self.player_action_reader_id).cloned().map(client::PlayerAction::into);

        player_action_reqs
            .for_each(|msg| {
                client_chan
                    .send(msg)
                    .expect("Unable to send message from client -> server")
            })
    }
}

pub struct ClientReceiverSystem {
    chunk_at_buf: Vec<server::ChunkAt>,
    player_pos_sync_buf: Vec<server::PlayerPosSync>,
}
impl Default for ClientReceiverSystem {
    fn default() -> Self {
        Self {
            chunk_at_buf: Vec::with_capacity(1024),
            player_pos_sync_buf: Vec::with_capacity(1024)
        }
    }
}
impl<'s> System<'s> for ClientReceiverSystem {
    type SystemData = (
        ReadExpect<'s, Receiver<server::ServerProtocol>>,
        Write<'s, EventChannel<server::PlayerPosSync>>,
        Write<'s, EventChannel<server::ChunkAt>>,
    );

    fn run(&mut self, (server_chan, mut player_pos_chan, mut chunk_at_chan): Self::SystemData) {
        use server::ServerProtocol::*;
        for evt in server_chan.try_iter() {
            match evt {
                ChunkAt(chunk_at) => {
                    self.chunk_at_buf.push(chunk_at);
                }
                PlayerPosSync(player_pos) => {
                    self.player_pos_sync_buf.push(player_pos);
                }
            }
        }
        if !self.chunk_at_buf.is_empty() {
            chunk_at_chan.drain_vec_write(&mut self.chunk_at_buf);
        }
        if !self.player_pos_sync_buf.is_empty() {
            player_pos_chan.drain_vec_write(&mut self.player_pos_sync_buf);
        }
    }
}

pub type WithAddr<T> = (PlayerAddr, T);

#[derive(SystemDesc, new)]
#[system_desc(name(ServerSenderSystemDesc))]
pub struct ServerSenderSystem {
    #[system_desc(event_channel_reader)]
    chunk_at_reader_id: ReaderId<WithAddr<server::ChunkAt>>,
    #[system_desc(event_channel_reader)]
    player_pos_sync_reader_id: ReaderId<WithAddr<server::PlayerPosSync>>
}
impl<'s> System<'s> for ServerSenderSystem {
    type SystemData = (
        Read<'s, EventChannel<WithAddr<server::ChunkAt>>>,
        Read<'s, EventChannel<WithAddr<server::PlayerPosSync>>>,
        WriteExpect<'s, Sender<WithAddr<server::ServerProtocol>>>,
    );

    fn run(&mut self, (chunk_at_chan, player_pos_chan, send_chan): Self::SystemData) {
        fn protocol_iter<'a, I, T>(iter: I) -> impl Iterator<Item=(PlayerAddr, server::ServerProtocol)> + 'a 
        where
            I: Iterator<Item=&'a (PlayerAddr, T)> + 'a,
            T: Clone + Into<server::ServerProtocol> + 'a,
        {
            iter.cloned().map(|(addr, msg)| (addr, msg.into()))
        }
        protocol_iter(chunk_at_chan.read(&mut self.chunk_at_reader_id))
            .chain(protocol_iter(player_pos_chan.read(&mut self.player_pos_sync_reader_id)))
            .for_each(|msg| {
            send_chan
                .send(msg)
                .expect("Unable to send message from server -> client")
        })
    }
}

pub struct ServerReceiverSystem {
    player_action_buf: Vec<WithAddr<client::PlayerAction>>,
}
impl Default for ServerReceiverSystem {
    fn default() -> Self {
        Self {
            player_action_buf: Vec::with_capacity(1024),
        }
    } 
}
impl<'s> System<'s> for ServerReceiverSystem {
    type SystemData = (
        ReadExpect<'s, Receiver<WithAddr<client::ClientProtocol>>>,
        Write<'s, EventChannel<WithAddr<client::PlayerAction>>>,
    );

    fn run(&mut self, (client_chan, mut player_action_chan): Self::SystemData) {
        use client::ClientProtocol::*;
        for (addr, evt) in client_chan.try_iter() {
            match evt {
                PlayerAction(player_action) => {
                    self.player_action_buf.push((addr, player_action))
                }
            }
        }

        player_action_chan.drain_vec_write(&mut self.player_action_buf);
    }
}