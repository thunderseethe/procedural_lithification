use amethyst::ecs::{Component, NullStorage, VecStorage};
use serde::{Deserialize, Serialize};
use std::{borrow::Cow, net::SocketAddr};

#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize)]
pub struct PlayerControlTag;
impl Component for PlayerControlTag {
    type Storage = NullStorage<PlayerControlTag>;
}

#[derive(Clone, PartialEq, Debug, Hash, Eq, Serialize, Deserialize)]
pub struct PlayerName(String);
impl From<String> for PlayerName {
    fn from(val: String) -> Self {
        PlayerName(val)
    }
}
impl Component for PlayerName {
    type Storage = VecStorage<PlayerName>;
}
impl Into<Cow<'static, str>> for PlayerName {
    fn into(self) -> Cow<'static, str> {
        Cow::Owned(self.0)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Serialize, Deserialize)]
pub struct PlayerAddr(SocketAddr);
impl From<SocketAddr> for PlayerAddr {
    fn from(val: SocketAddr) -> Self {
        PlayerAddr(val)
    }
}
impl From<&SocketAddr> for PlayerAddr {
    fn from(val: &SocketAddr) -> Self {
        PlayerAddr(*val)
    }
}
impl Into<SocketAddr> for PlayerAddr {
    fn into(self) -> SocketAddr {
        self.0
    }
}
impl<'a> Into<&'a SocketAddr> for &'a PlayerAddr {
    fn into(self) -> &'a SocketAddr {
        &self.0 
    }
}
impl Component for PlayerAddr {
    type Storage = VecStorage<PlayerAddr>;
}
/// Denotes newplayer connected to server.
pub struct NewPlayer(PlayerName, PlayerAddr);
impl NewPlayer {
    pub fn new(name: String, client_addr: SocketAddr) -> Self {
        NewPlayer(name.into(), client_addr.into())
    }
}
