use crate::chunk::serde as chunk_serde;
use crate::{
    chunk::Chunk,
    field::FieldOf,
    terrain::{DefaultGenerateBlock, Terrain},
    volume::Sphere,
};

use amethyst::core::math::Point3;
use async_compression::futures::{bufread::DeflateDecoder, write::DeflateEncoder};
use async_std::fs::{File, OpenOptions};
use async_std::io;
use futures::{AsyncReadExt, AsyncWriteExt, Future, FutureExt, TryFutureExt};
use log::info;
use morton_code::MortonCode;
use parking_lot::RwLock;
use sparse_vector::SparseVector;
use std::{borrow::Borrow, collections::HashSet, path::PathBuf, sync::Arc};

pub type StoredChunk = Arc<RwLock<Chunk>>;
type DimensionStorage = Arc<RwLock<SparseVector<ChunkMortonCode, StoredChunk>>>;

#[derive(Clone)]
pub struct DimensionConfig {
    pub directory: PathBuf,
    pub generate_radius: usize,
    /// Debug option, toggles loading chunks from file
    pub load_chunks_from_file: bool,
}
impl Default for DimensionConfig {
    fn default() -> Self {
        DimensionConfig {
            directory: PathBuf::from("./resources/dimension/"),
            generate_radius: 4,
            load_chunks_from_file: true,
        }
    }
}
impl DimensionConfig {
    pub fn new(directory: PathBuf, generate_radius: usize) -> Self {
        DimensionConfig {
            directory,
            generate_radius,
            load_chunks_from_file: true,
        }
    }

    pub fn load_chunks_from_file(mut self, value: bool) -> Self {
        self.load_chunks_from_file = value;
        self
    }
}

const CHUNK_DIR: &str = "chunk";

pub struct Dimension {
    pub config: DimensionConfig,
    terrain: Arc<Terrain<DefaultGenerateBlock>>,
    currently_generating_chunks: Arc<RwLock<HashSet<Point3<FieldOf<Chunk>>>>>,
    storage: DimensionStorage,
}

pub type ChunkMortonCode = MortonCode<FieldOf<Chunk>>;
pub type DimensionRes = Dimension;

impl Default for Dimension {
    fn default() -> Self {
        Dimension {
            config: DimensionConfig::default(),
            terrain: Arc::new(Terrain::default()),
            currently_generating_chunks: Arc::new(RwLock::new(HashSet::new())),
            storage: Arc::new(RwLock::new(SparseVector::default()))//DimensionStorage::default(),
        }
    }
}
unsafe impl Sync for Dimension {}
unsafe impl Send for Dimension {}
impl Dimension {
    pub fn new() -> Self {
        Dimension::default()
    }
    pub fn with_config(config: DimensionConfig) -> Self {
        Dimension {
            config,
            ..Dimension::default()
        }
    }

    pub fn chunk_exists<M: Into<ChunkMortonCode>>(&self, pos: M) -> bool {
        self.storage 
            .read()
            .contains_key(pos.into())
    }

    pub fn insert(
        &self,
        morton_code: ChunkMortonCode,
        chunk: Chunk,
    ) -> (StoredChunk, Option<StoredChunk>) {
        let mut lock = self.storage.write();
        let (new, old_val) = lock 
            .insert(morton_code, Arc::new(RwLock::new(chunk)));
        (Arc::clone(new), old_val)
    }

    fn chunk_file_exists<P>(&self, pos: P) -> bool
    where
        P: Into<ChunkMortonCode>,
    {
        let morton: ChunkMortonCode = pos.into();
        self.chunk_dir().join(format!("{}", morton)).exists()
    }

    fn chunk_dir(&self) -> PathBuf {
        self.config.directory.as_path().join(CHUNK_DIR)
    }

    pub fn create<P>(&mut self, pos: P) -> StoredChunk
    where
        P: Borrow<Point3<FieldOf<Chunk>>>,
    {
        let point = pos.borrow();
        let morton: ChunkMortonCode = pos.borrow().into();
        self.create_with_morton(morton, point)
    }

    pub fn create_with_morton<P>(&mut self, morton: ChunkMortonCode, point: P) -> StoredChunk
    where
        P: Borrow<Point3<FieldOf<Chunk>>>,
    {
        info!("Generating new Chunk {}", point.borrow());
        self.insert(morton, self.terrain.generate_chunk(point)).0
    }

    pub fn spawn_around_point<'a, P>(
        &self,
        pos: P,
        radius: FieldOf<Chunk>,
    ) -> (
        Vec<impl 'a + Send + Future<Output=chunk_serde::Result<StoredChunk>>>,
        Vec<impl 'a + Send + FnOnce() -> StoredChunk>,
    )
    where
        P: Borrow<Point3<f32>>,
    {
        let mut load_chunks = Vec::new();
        let mut create_chunks = Vec::new();
        let player_chunk = Chunk::absl_to_chunk_coords(*pos.borrow());
        for point in Sphere::new(player_chunk, radius)
            .into_iter()
            //.filter(|point| point == &Point3::origin())
            .filter(|point| !self.chunk_exists(point) 
                && !self.currently_generating_chunks.read().contains(point))
        {
            let storage = Arc::clone(&self.storage);
            self.currently_generating_chunks.write().insert(point);
            let currently_generating = Arc::clone(&self.currently_generating_chunks);
            if self.chunk_file_exists(player_chunk) {
                // Load chunk from file
                let chunk_dir = self.chunk_dir();
                load_chunks.push(async move {
                    let morton = point.into();
                    let chunk_path = chunk_dir.join(format!("{}", morton));

                    let file = OpenOptions::new().read(true).open(chunk_path).await?;
                    let mut reader = DeflateDecoder::new(io::BufReader::new(file));

                    let mut buffer = Vec::new();
                    reader.read_to_end(&mut buffer).await?;

                    let packed_octree = bincode::deserialize(&buffer)?;
                    let chunk = Chunk::unpack(packed_octree, point)?;

                    let mut lock = storage.write();
                    let chunk = 
                        lock.insert(morton, Arc::new(RwLock::new(chunk))).0;
                    Arc::as_ref(&currently_generating).write().remove(&point);
                    Ok(Arc::clone(chunk))
                });
            } else {
                // Generate new chunk
                let terrain = Arc::clone(&self.terrain);
                create_chunks.push(move || {
                    //Arc::clone(self_lock.create_with_morton(morton, point))
                    let chunk =  terrain.generate_chunk(&point);
                    let mut lock = storage.write();
                    let chunk =
                        lock.insert(point.into(), Arc::new(RwLock::new(chunk))).0;
                    Arc::as_ref(&currently_generating).write().remove(&point);
                    Arc::clone(chunk)
                });
            }
        }
        return (load_chunks, create_chunks);
    }

    pub async fn create_or_load_chunk<P>(
        &mut self,
        morton: ChunkMortonCode,
        point: P,
    ) -> chunk_serde::Result<StoredChunk>
    where
        P: Borrow<Point3<FieldOf<Chunk>>>,
    {
        if self.config.load_chunks_from_file && self.chunk_file_exists(morton) {
            self.load_with_morton_and_point(morton, point).await
        } else {
            Ok(self.create_with_morton(morton, point))
        }
    }

    pub fn get_chunk<M>(&self, morton: M) -> Option<StoredChunk>
    where
        M: Into<ChunkMortonCode>,
    {
        let lock = self.storage.read();
        lock.get(morton.into()).map(Arc::clone)
    }

    pub fn iter(&self) -> impl Iterator<Item = StoredChunk> {
        (&self).into_iter()
    }

    pub async fn ensure_directory_exists(&self) -> io::Result<()> {
        let dim_dir = async_std::fs::canonicalize(&self.config.directory).await?;
        let chunk_dir = dim_dir.join(CHUNK_DIR);
        async_std::fs::create_dir_all(chunk_dir).await
    }

    // Return type here needs some work, this is not great
    pub fn store(&self) -> impl Future<Output = Vec<io::Result<usize>>> {
        let chunk_dir = &self.config.directory.join(CHUNK_DIR);
        let lock = self.storage.read();
        let futures = lock 
            .iter()
            // Take ownership of our indice and value
            .map(|(morton_ref, arc_chunk_ref)| (morton_ref.clone(), Arc::clone(arc_chunk_ref)))
            .map(|(morton, lock_chunk)| {
                let chunk_file = chunk_dir.join(format!("{}", morton));
                OpenOptions::new()
                    .write(true)
                    .create(true)
                    .open(chunk_file)
                    .map(move |file_res: io::Result<File>| {
              
                        file_res.and_then(|file| {
                            let chunk = lock_chunk.read();
                            let buffer = chunk_serde::serialize_chunk(&chunk)?;
                            Ok((file, buffer))
                        })
                    })
                    .and_then(write)
            });

        futures::future::join_all(futures)
    }

    /// If a caller already has a Point and a ChunkMortonCode there is no need to redo conversions.
    // A shorter name would be nice
    pub async fn load_with_morton_and_point<P, M>(
        &self,
        into_morton: M,
        pos_ref: P,
    ) -> chunk_serde::Result<StoredChunk>
    where
        M: Into<ChunkMortonCode>,
        P: Borrow<Point3<FieldOf<Chunk>>>,
    {
        let morton = into_morton.into();
        let pos = pos_ref.borrow();
        let chunk_path = self.chunk_dir()
            .join(format!("{}", morton));

        let file = 
            OpenOptions::new().read(true).open(chunk_path).await?;
        let mut reader = 
            DeflateDecoder::new(io::BufReader::new(file));

        let mut buffer = Vec::new();
        reader.read_to_end(&mut buffer).await?;

        let packed_octree = bincode::deserialize(&buffer)?;
        let chunk = Chunk::unpack(packed_octree, *pos)?;


        // We're overwriting whatever was previously present at this index.
        Ok(self.insert(morton, chunk).0)
    }
}

async fn write((file, buffer): (File, Vec<u8>)) -> io::Result<usize> {
    DeflateEncoder::new(file).write(&buffer).await
}

impl IntoIterator for Dimension {
    type Item = StoredChunk;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.storage.read()
            .value_iter()
            .map(Arc::clone)
            .collect::<Vec<_>>()
            .into_iter()
    }
}

impl<'a> IntoIterator for &'a Dimension {
    type Item = StoredChunk;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.storage.read()
            .value_iter()
            .map(Arc::clone)
            .collect::<Vec<_>>()
            .into_iter()
    }
}

impl<'a> IntoIterator for &'a mut Dimension {
    type Item = StoredChunk;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.storage.read()
            .value_iter()
            .map(Arc::clone)
            .collect::<Vec<_>>()
            .into_iter()
    }
}
