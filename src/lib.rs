#![warn(clippy::all)]
// Since we use Amethyst this triggers for all of our SystemDatas
#![allow(clippy::type_complexity)]
#[macro_use]
extern crate num_derive;
#[macro_use]
extern crate serde_derive;

extern crate amethyst;
extern crate async_compression;
extern crate async_std;
extern crate bincode;
extern crate derive_new;
extern crate either;
extern crate field;
extern crate flate2;
extern crate futures;
extern crate itertools;
extern crate log;
extern crate morton_code;
extern crate ncollide3d;
extern crate noise;
extern crate num_traits;
extern crate octree;
extern crate par_array_init;
extern crate parking_lot;
extern crate rand;
extern crate rayon;
extern crate serde;
extern crate sparse_vector;
extern crate shared_memory_queue;
#[cfg(feature = "profiler")]
extern crate thread_profiler;
extern crate tokio;
extern crate typenum;

extern crate quickcheck;
#[cfg(test)]
#[macro_use(quickcheck)]
extern crate quickcheck_macros;
#[cfg(test)]
extern crate galvanic_assert;

pub mod chunk;
pub mod collision;
pub mod dimension;
pub mod input;
pub mod player;
pub mod protocol;
pub mod states;
pub mod systems;
pub mod terrain;
pub mod volume;

pub(crate) mod nalgebra;

pub const SERVER_NET_SYSTEM_NAME: &str = "server_network_shim";
pub const CLIENT_NET_SYSTEM_NAME: &str = "client_network_shim";

use std::path::Path;

pub fn configure_logger<P: AsRef<Path>>(path: P) -> std::result::Result<(), log::SetLoggerError> {
    use amethyst::LogLevelFilter;
    use log::*;

    let colors = fern::colors::ColoredLevelConfig::new();
    fern::Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "[{level}][{target}] {message}",
                level = colors.color(record.level()),
                target = record.target(),
                message = message
            ))
        })
        .level(LogLevelFilter::Info)
        .level_for("gfx_device_gl", LogLevelFilter::Error)
        .level_for("gfx_backend_vulkan", LogLevelFilter::Error)
        .chain(std::io::stdout())
        .chain(fern::log_file(path).unwrap())
        .apply()
}