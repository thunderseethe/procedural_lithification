use amethyst::core::math::Point3;
use field::*;
use noise::{NoiseFn, Perlin, Seedable};
use rayon::prelude::*;
use std::{borrow::Borrow, cmp::Ordering};

use crate::chunk::{
    block::{Block, DIRT_BLOCK},
    Chunk, OctreeOf,
};
use crate::octree::builder::Builder;
use crate::octree::Diameter;

pub type HeightMap = [[u8; Chunk::DIAMETER]; Chunk::DIAMETER];

pub trait GenerateBlockFn {
    fn generate(
        &self,
        height_map: &HeightMap,
        point: Point3<FieldOf<OctreeOf<Chunk>>>,
    ) -> Option<Block>;
}
impl<F> GenerateBlockFn for F
where
    F: Fn(&HeightMap, Point3<FieldOf<OctreeOf<Chunk>>>) -> Option<Block>,
{
    fn generate(
        &self,
        height_map: &HeightMap,
        pos: Point3<FieldOf<OctreeOf<Chunk>>>,
    ) -> Option<Block> {
        self(height_map, pos)
    }
}

pub struct PlateauGenerateBlock {
    y_threshold: usize,
}
impl PlateauGenerateBlock {
    pub fn new(y_threshold: usize) -> Self {
        PlateauGenerateBlock { y_threshold }
    }
}
impl GenerateBlockFn for PlateauGenerateBlock {
    fn generate(
        &self,
        _height_map: &HeightMap,
        point: Point3<FieldOf<OctreeOf<Chunk>>>,
    ) -> Option<Block> {
        if (point.y as usize) < self.y_threshold {
            Some(1)
        } else {
            None
        }
    }
}

pub struct SphereGenerateBlock {
    radius: usize,
}
impl SphereGenerateBlock {
    pub fn new(radius: usize) -> Self {
        SphereGenerateBlock { radius }
    }
}
impl GenerateBlockFn for SphereGenerateBlock {
    fn generate(
        &self,
        _height_map: &HeightMap,
        point: Point3<FieldOf<OctreeOf<Chunk>>>,
    ) -> Option<Block> {
        let chunk_radius = (Chunk::DIAMETER / 2) as isize;
        let x = (point.x as isize) - chunk_radius;
        let y = (point.y as isize) - chunk_radius;
        let z = (point.z as isize) - chunk_radius;
        if x * x + y * y + z * z < (self.radius * self.radius) as isize {
            Some(1)
        } else {
            None
        }
    }
}

pub struct DefaultGenerateBlock();
impl GenerateBlockFn for DefaultGenerateBlock {
    fn generate(
        &self,
        height_map: &HeightMap,
        p: Point3<FieldOf<OctreeOf<Chunk>>>,
    ) -> Option<Block> {
        let subarray: [u8; Chunk::DIAMETER] = height_map[p.x as usize];
        let height: u8 = subarray[p.z as usize];
        if p.y <= height {
            Some(DIRT_BLOCK)
        } else {
            None
        }
    }
}


pub struct Terrain<F> {
    perlin: Perlin,
    generate_block: F,
}

impl Default for Terrain<DefaultGenerateBlock> {
    fn default() -> Self {
        Terrain {
            perlin: Perlin::new(),
            generate_block: DefaultGenerateBlock(),
        }
    }
}

impl<F> Terrain<F>
where
    F: GenerateBlockFn + Sync,
{
    pub fn new(seed: u32, generate_block: F) -> Self {
        Terrain {
            perlin: Perlin::new().set_seed(seed),
            generate_block,
        }
    }

    pub fn with_seed(self, seed: u32) -> Self {
        Terrain {
            perlin: Perlin::new().set_seed(seed),
            ..self
        }
    }

    pub fn with_block_generator<NewF>(self, generate_block: NewF) -> Terrain<NewF>
    where
        NewF: GenerateBlockFn + Sync,
    {
        Terrain {
            generate_block,
            perlin: self.perlin,
        }
    }

    pub fn generate_chunk<P>(&self, chunk_pos_ref: P) -> Chunk
    where
        P: Borrow<Point3<FieldOf<Chunk>>>,
    {
        let chunk_pos = chunk_pos_ref.borrow();
        match chunk_pos.y.cmp(&0) {
            Ordering::Less => Chunk::with_block(*chunk_pos, DIRT_BLOCK),
            Ordering::Greater => Chunk::with_empty(*chunk_pos),
            Ordering::Equal => self.y_zero_chunk_generator(chunk_pos),
        }
    }

    #[inline]
    fn create_height_map(&self, chunk_pos: &Point3<FieldOf<Chunk>>) -> HeightMap {
        // TODO: generalize this over Octree::Diameter once new_octree lands
        let chunk_size = Chunk::DIAMETER as f64;
        par_array_init::par_array_init(|x| {
            par_array_init::par_array_init(|z| {
                let nx = f64::from(chunk_pos.x) + ((x as f64 / chunk_size) - 0.5);
                let nz = f64::from(chunk_pos.z) + ((z as f64 / chunk_size) - 0.5);
                let two = 0.5f64.mul_add(
                    self.perlin.get([2.0 * nx, 2.0 * nz]),
                    self.perlin.get([nx, nz]),
                );
                let four = 0.25f64.mul_add(self.perlin.get([4.0 * nx, 4.0 * nz]), two);
                let eight = 0.125f64.mul_add(self.perlin.get([8.0 * nx, 8.0 * nz]), four);
                let sixten = 0.0625f64.mul_add(self.perlin.get([16.0 * nx, 16.0 * nz]), eight);
                let thirtytwo = 0.03125f64.mul_add(self.perlin.get([32.0 * nx, 32.0 * nz]), sixten);
                let recipricol: f64 = 0.507_936_507_93 * 0.5;
                let noise = thirtytwo.mul_add(recipricol, 0.5);
                (noise * chunk_size).ceil() as u8
            })
        })
    }

    #[inline]
    pub fn y_zero_chunk_generator<P>(&self, chunk_pos_ref: P) -> Chunk
    where
        P: Borrow<Point3<FieldOf<Chunk>>>,
    {
        let chunk_pos = chunk_pos_ref.borrow();
        let height_map = self.create_height_map(chunk_pos);
        let mut chunk_to_be = Chunk::builder();
        chunk_to_be
            .par_iter_mut()
            .for_each(|(pos, block)| *block = self.generate_block.generate(&height_map, pos));
        chunk_to_be.build(*chunk_pos)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use num_traits::AsPrimitive;

    #[test]
    fn test_generating_plateau_works_correctly() {
        let threshold = (Chunk::DIAMETER / 2).as_();
        let terrain =
            Terrain::default().with_block_generator(PlateauGenerateBlock::new(Chunk::DIAMETER / 2));
        let chunk = terrain.generate_chunk(Point3::origin());
        println!("{:?}", chunk);
        for octant in chunk.iter() {
            assert_eq!(octant.data, &1);
            assert!(octant.bottom_left_front.y < threshold);
        }
    }

    #[test]
    fn test_generating_sphere_works_correctly() {
        let chunk_size = Chunk::DIAMETER as isize;
        let chunk_half = chunk_size / 2;
        let chunk_quarter = chunk_half / 2;
        let terrain =
            Terrain::default().with_block_generator(SphereGenerateBlock::new(Chunk::DIAMETER / 4));
        let chunk = terrain.generate_chunk(Point3::origin());
        for octant in chunk.iter() {
            let x = octant.bottom_left_front.x as isize - chunk_half;
            let y = octant.bottom_left_front.y as isize - chunk_half;
            let z = octant.bottom_left_front.z as isize - chunk_half;
            assert!(x * x + y * y + z * z <= chunk_quarter * chunk_quarter);
        }
    }
}

pub mod arbitrary {
    use super::*;
    use quickcheck::{Arbitrary, StdThreadGen};

    pub struct ArbitraryGenerateFn {
        size: usize
    }
    impl Default for ArbitraryGenerateFn {
        fn default() -> Self {
            Self {
                size: usize::max_value()
            }
        }
    }
    impl GenerateBlockFn for ArbitraryGenerateFn {
        fn generate(
            &self,
            _height_map: &HeightMap,
            _p: Point3<FieldOf<OctreeOf<Chunk>>>,
        ) -> Option<Block> {
            let mut gen = StdThreadGen::new(self.size);
            Option::<Block>::arbitrary(&mut gen)
        }
    }
}