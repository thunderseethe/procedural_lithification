use crate::octree::octant::{Octant, OctantFace};
use crate::octree::*;
use crate::volume::Cuboid;
use alga::general::SubsetOf;
use amethyst::{
    core::math::{try_convert, Point3, Scalar, Unit, Vector3},
    renderer::rendy::mesh::{Normal, Position, TexCoord},
};
use field::*;
use num_traits::{AsPrimitive, One, Zero};
use std::fmt::Display;
use std::{cmp::Ordering, fmt};

#[derive(Eq, PartialEq, Debug, Copy, Clone, FromPrimitive, ToPrimitive)]
enum Axis {
    X = 0,
    Y = 1,
    Z = 2,
}
impl Axis {
    pub fn next(self) -> Self {
        match self {
            Axis::X => Axis::Y,
            Axis::Y => Axis::Z,
            Axis::Z => Axis::X,
        }
    }

    pub fn index(self) -> usize {
        match self {
            Axis::X => 0,
            Axis::Y => 1,
            Axis::Z => 2,
        }
    }

    pub fn unit<N: Scalar + Zero + One>(self) -> Unit<Vector3<N>> {
        match self {
            Axis::X => Vector3::x_axis(),
            Axis::Y => Vector3::y_axis(),
            Axis::Z => Vector3::z_axis(),
        }
    }

    pub fn front_face(self) -> OctantFace {
        match self {
            Axis::X => OctantFace::Right,
            Axis::Y => OctantFace::Up,
            Axis::Z => OctantFace::Back,
        }
    }

    pub fn back_face(self) -> OctantFace {
        match self {
            Axis::X => OctantFace::Left,
            Axis::Y => OctantFace::Down,
            Axis::Z => OctantFace::Front,
        }
    }
}

fn option_xor<A>(opt_a: Option<A>, opt_b: Option<A>) -> Option<A> {
    match (opt_a, opt_b) {
        (Some(a), None) => Some(a),
        (None, Some(b)) => Some(b),
        _ => None,
    }
}

pub struct Mesher<'a, O>
where
    O: FieldType,
{
    octree: &'a O,
    offset: Vector3<FieldOf<O>>,
    size: usize,
}
impl<'a, O: FieldType> Mesher<'a, O> {
    pub fn to_index<N: SubsetOf<usize> + Display>(&self, x_: N, y_: N, z_: N) -> usize {
        let x: usize = x_.to_superset();
        let y: usize = y_.to_superset();
        let z: usize = z_.to_superset();
        x + y * self.size + z * self.size * self.size
    }
}
use crate::octree::iter::OctreeRefIter;
impl<'a, O> Mesher<'a, O>
where
    O: OctreeLike
        + HasPosition<Position = Point3<FieldOf<O>>>
        + OctreeRefIter<'a, Item = Octant<&'a ElementOf<O>, &'a Point3<FieldOf<O>>>>,
    ElementOf<O>: Clone + PartialEq,
    FieldOf<O>: SubsetOf<usize>,
{
    pub fn new(octree: &'a O) -> Self {
        let p = octree.position();
        Mesher {
            octree,
            offset: Vector3::new(p.x, p.y, p.z),
            size: O::DIAMETER,
        }
    }

    pub fn generate_quads_array(&self) -> Vec<Quad<ElementOf<O>>> {
        let mut quads = Vec::new();
        let size_iter: i32 = self.size as i32;
        // Mask of each face used to remember which blocks are visible
        let mut mask: Vec<Option<(ElementOf<O>, bool)>> = vec![None; self.size * self.size];
        // Holds copy of chunk as flat array for fast iteration
        let mut chunk: Vec<Option<ElementOf<O>>> = vec![None; self.size * self.size * self.size];

        self.octree.iter().for_each(|octant| {
            let blf: Point3<FieldOf<O>> = octant.bottom_left_front - self.offset;
            let bottom_left: Point3<usize> = Point3::new(blf.x.as_(), blf.y.as_(), blf.z.as_());

            let top_right: Point3<usize> = {
                octant.top_right()
                    - Vector3::new(
                        self.offset.x.as_(),
                        self.offset.y.as_(),
                        self.offset.z.as_(),
                    )
            };
            for p in Cuboid::new(bottom_left, top_right).into_iter() {
                chunk[self.to_index(p.x, p.y, p.z)] = Some(octant.data.clone());
            }
        });
        let mut x: Point3<i32> = Point3::origin();
        for d in &[Axis::X, Axis::Y, Axis::Z] {
            let dim_u = d.next();
            let dim_v = dim_u.next();
            for dimension_cursor in -1..size_iter {
                let mut mask_indx = 0;
                for j in 0..size_iter {
                    for i in 0..size_iter {
                        x[d.index()] = dimension_cursor;
                        x[dim_v.index()] = j;
                        x[dim_u.index()] = i;
                        let q = d.unit();

                        let front_face = as_option(0 <= dimension_cursor)
                            .and_then(|_| try_convert(x))
                            .and_then(|p: Point3<u8>| chunk[self.to_index(p.x, p.y, p.z)].clone());
                        let back_face = as_option(dimension_cursor < size_iter - 1)
                            .and_then(|_| try_convert(x + q.as_ref()))
                            .and_then(|p: Point3<u8>| chunk[self.to_index(p.x, p.y, p.z)].clone());
                        mask[mask_indx] = option_xor(
                            front_face.map(|block| (block, false)),
                            back_face.map(|block| (block, true)),
                        );
                        mask_indx += 1;
                    }
                }
                mask_indx = 0;
                for j in 0..size_iter {
                    let mut i = 0;
                    while i < size_iter && mask_indx < mask.len() {
                        if mask[mask_indx].is_none() {
                            i += 1;
                            mask_indx += 1;
                            continue;
                        }

                        let (w, h) = self.determine_quad_dimensions(
                            &mask[mask_indx..],
                            self.size - i as usize,
                            self.size - j as usize,
                        );

                        let (block, is_back_face) = mask[mask_indx].clone().unwrap();
                        x[d.index()] = dimension_cursor + 1;
                        x[dim_u.index()] = i as i32;
                        x[dim_v.index()] = j as i32;
                        let du: Vector3<i32> = dim_u.unit().into_inner() * w as i32;
                        let dv: Vector3<i32> = dim_v.unit().into_inner() * h as i32;

                        quads.push(Quad::new(
                            x,
                            x + dv,
                            x + du,
                            x + du + dv,
                            block,
                            if is_back_face {
                                d.back_face()
                            } else {
                                d.front_face()
                            },
                        ));

                        for l in 0..h {
                            for k in mask_indx..(mask_indx + w) {
                                mask[k + l * self.size] = None;
                            }
                        }

                        i += w as i32;
                        mask_indx += w;
                    }
                }
            }
        }
        quads
    }

    fn determine_quad_dimensions<E: PartialEq>(
        &self,
        mask: &[E],
        max_width: usize,
        max_height: usize,
    ) -> (usize, usize) {
        let test = &mask[0];
        let w = mask
            .iter()
            .take(max_width)
            .take_while(|ele| test.eq(ele))
            .count();
        let h = mask
            .chunks(self.size)
            .take(max_height)
            .take_while(|row| row.iter().take(w).all(|ele| test.eq(ele)))
            .count();
        (w, h)
    }
}

#[derive(Eq, PartialEq)]
pub struct Quad<T> {
    bottom_left: Point3<i32>,
    top_left: Point3<i32>,
    bottom_right: Point3<i32>,
    top_right: Point3<i32>,
    data: T,
    pub face: OctantFace,
}
impl<T> Quad<T> {
    pub fn new(
        bottom_left: Point3<i32>,
        top_left: Point3<i32>,
        bottom_right: Point3<i32>,
        top_right: Point3<i32>,
        data: T,
        face: OctantFace,
    ) -> Self {
        Quad {
            bottom_left,
            top_left,
            bottom_right,
            top_right,
            data,
            face,
        }
    }

    pub fn u(&self) -> i32 {
        use OctantFace::*;
        match self.face {
            Left | Right => self.bottom_left.y,
            Up | Down => self.bottom_left.z,
            Front | Back => self.bottom_left.x,
        }
    }

    pub fn v(&self) -> i32 {
        use OctantFace::*;
        match self.face {
            Left | Right => self.bottom_left.z,
            Up | Down => self.bottom_left.x,
            Front | Back => self.bottom_left.y,
        }
    }

    pub fn width(&self) -> i32 {
        use OctantFace::*;
        match self.face {
            Left | Right => self.top_right.y - self.bottom_left.y,
            Up | Down => self.top_right.z - self.bottom_left.z,
            Front | Back => self.top_right.x - self.bottom_left.x,
        }
    }

    pub fn height(&self) -> i32 {
        use OctantFace::*;
        match self.face {
            Left | Right => self.top_right.z - self.bottom_left.z,
            Up | Down => self.top_right.x - self.bottom_left.x,
            Front | Back => self.top_right.y - self.bottom_left.y,
        }
    }

    pub fn normal_vector(&self) -> Vector3<f32> {
        use OctantFace::*;
        match self.face {
            Back => Vector3::new(0.0, 0.0, 1.0),
            //Back => Vector3::new(0.0, 0.0, -1.0),
            Up => Vector3::new(0.0, 1.0, 0.0),
            //Up => Vector3::new(0.0, -1.0, 0.0),
            Front => Vector3::new(0.0, 0.0, -1.0),
            //Front => Vector3::new(0.0, 0.0, 1.0),
            Down => Vector3::new(0.0, -1.0, 0.0),
            //Down => Vector3::new(0.0, 1.0, 0.0),
            Right => Vector3::new(1.0, 0.0, 0.0),
            //Right => Vector3::new(-1.0, 0.0, 0.0),
            Left => Vector3::new(-1.0, 0.0, 0.0),
            //Left => Vector3::new(1.0, 0.0, 0.0),
        }
    }

    pub fn mesh_coords(
        self,
        pos: &mut Vec<Position>,
        norm: &mut Vec<Normal>,
        tex: &mut Vec<TexCoord>,
    ) {
        use itertools::repeat_n;

        let v: [[f32; 3]; 4] = [
            self.bottom_left.coords.map(|i| i as f32).into(),
            self.bottom_right.coords.map(|i| i as f32).into(),
            self.top_left.coords.map(|i| i as f32).into(),
            self.top_right.coords.map(|i| i as f32).into(),
        ];

        let (width, height): (f32, f32) = (self.width().as_(), self.height().as_());
        let t = [[0.0, 0.0], [width, 0.0], [0.0, height], [width, height]];
        let n: Normal = Normal(self.normal_vector().into());

        use OctantFace::*;
        let order: Vec<usize> = match self.face {
            Back => vec![0, 1, 2, 2, 1, 3],
            //Back => vec![],
            Up => vec![1, 3, 0, 0, 3, 2],
            Front => vec![2, 3, 0, 0, 3, 1],
            Down => vec![0, 2, 1, 1, 2, 3],
            Right => vec![2, 0, 3, 3, 0, 1],
            Left => vec![0, 2, 1, 1, 2, 3],
        };
        norm.extend(repeat_n(n, order.len()));
        order.into_iter().for_each(|i| {
            pos.push(Position(v[i]));
            tex.push(TexCoord(t[i]));
        })
    }
}

impl<T: fmt::Display> fmt::Display for Quad<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Quad(\n\t{}, {},\n\t{}, {},\n\t{}, {:?})",
            self.top_left,
            self.top_right,
            self.bottom_left,
            self.bottom_right,
            self.data,
            self.face
        )
    }
}
impl<T: fmt::Debug> fmt::Debug for Quad<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Quad({}, {}, {}, {}, {:?}, {:?})",
            self.bottom_left,
            self.top_left,
            self.bottom_right,
            self.top_right,
            self.data,
            self.face
        )
    }
}
impl<T: PartialOrd + Eq> PartialOrd for Quad<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.face != other.face || self.data != other.data {
            None
        } else {
            Some(self.cmp(other))
        }
    }
}
impl<T: PartialOrd + Eq> Ord for Quad<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        let (x, y, w, h) = (self.u(), self.v(), self.width(), self.height());
        let (_x, _y, _w, _h) = (other.u(), other.v(), other.width(), other.height());

        y.cmp(&_y)
            .then(x.cmp(&_x))
            .then(_w.cmp(&w))
            .then(_h.cmp(&h))
    }
}

fn as_option(pred: bool) -> Option<()> {
    if pred {
        Some(())
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use super::Mesher;

    use crate::amethyst::core::math::Point3;
    use crate::octree::consts::*;
    use crate::octree::{New, OctreeBase};

    #[test]
    fn test_mesh_full_octree_works() {
        let octree: OctreeBase<i32, u8> = Octree0::new(Some(1), Point3::origin());
        let mesher: Mesher<'_, OctreeBase<i32, u8>> = Mesher::new(&octree);
        let quads = mesher.generate_quads_array();
        for quad in quads {
            println!("{:?}", quad);
        }
    }
}
