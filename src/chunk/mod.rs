use crate::octree::{
    pack::{PackedOctree, UnpackError},
    Diameter, ElementOf, ElementType, Get, HasPosition, Insert, Map, Octree, OctreeLike,
    OctreeTypes, PositionOf,
};
use alga::general::{ClosedDiv, RealField};
use amethyst::{
    core::math::{Point3, Scalar},
    renderer::{rendy::mesh::MeshBuilder, types::MeshData},
};
use field::*;
use ncollide3d::{
    math::Isometry,
    query::{Ray, RayCast, RayIntersection},
};
use num_traits::{AsPrimitive, Float, NumCast};
use std::borrow::Borrow;
use typenum::U64;

#[cfg(test)]
pub mod arbitrary;
pub mod block;
pub mod chunk_builder;
pub mod mesher;
pub mod serde;

use block::Block;
use mesher::Mesher;

// Shorthand to access Octree of type that implements HasOctree.
pub type OctreeOf<T> = <T as HasOctree>::Octree;

pub trait HasOctree: OctreeTypes + Diameter {
    type Octree: OctreeLike;
}
impl HasOctree for Chunk {
    type Octree = Octree<Block, u8, U64>;
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Chunk {
    pub pos: Point3<FieldOf<Chunk>>,
    octree: OctreeOf<Self>,
}

impl Chunk {
    pub fn new(pos: Point3<FieldOf<Chunk>>, octree: OctreeOf<Chunk>) -> Self {
        Chunk { pos, octree }
    }

    pub fn with_block(pos: Point3<FieldOf<Chunk>>, block: Block) -> Self {
        Chunk {
            pos,
            octree: <Chunk as HasOctree>::Octree::at_origin(Some(block)),
        }
    }

    pub fn with_empty(pos: Point3<FieldOf<Chunk>>) -> Self {
        Chunk {
            pos,
            octree: <Chunk as HasOctree>::Octree::at_origin(None),
        }
    }

    pub fn get_block<P>(&self, pos: P) -> Option<Block>
    where
        P: Borrow<PositionOf<OctreeOf<Self>>>,
    {
        self.octree.get(pos).cloned()
    }

    pub fn place_block<P>(&mut self, pos: P, block: Block) -> &mut Self
    where
        P: Borrow<PositionOf<OctreeOf<Self>>>,
    {
        self.octree = self.octree.insert(pos, block);
        self
    }

    pub fn generate_mesh(&self) -> Option<(Point3<f32>, MeshData)> {
        let chunk_render_pos: Point3<f32> = Chunk::chunk_to_absl_coords(self.pos);

        self.octree.map(
            || None,
            |_| {
                Some((
                    chunk_render_pos,
                    cube_mesh(self.octree.diameter() as f32).into(),
                ))
            },
            |_| {
                let mesher = Mesher::<Octree<Block, u8, U64>>::new(&self.octree);
                let quads = mesher.generate_quads_array();
                let capacity = quads.len() * 6;
                let (mut pos, mut norm, mut tex) = (
                    Vec::with_capacity(capacity),
                    Vec::with_capacity(capacity),
                    Vec::with_capacity(capacity),
                );
                for quad in quads.into_iter() {
                    quad.mesh_coords(&mut pos, &mut norm, &mut tex);
                }
                Some((
                    chunk_render_pos,
                    MeshBuilder::new()
                        .with_vertices(pos)
                        .with_vertices(norm)
                        .with_vertices(tex)
                        .into(),
                ))
            },
        )
    }

    pub fn chunk_to_absl_coords<N>(chunk_pos: Point3<FieldOf<Chunk>>) -> Point3<N>
    where
        N: Scalar,
        FieldOf<Chunk>: AsPrimitive<N>,
    {
        let translated = chunk_pos * Chunk::DIAMETER.as_();
        Point3::new(translated.x.as_(), translated.y.as_(), translated.z.as_())
    }

    pub fn absl_to_chunk_coords<N>(absl_pos: Point3<N>) -> Point3<FieldOf<Chunk>>
    where
        N: Scalar + ClosedDiv + AsPrimitive<FieldOf<Chunk>> + NumCast + Float,
        usize: AsPrimitive<N>,
    {
        let n_diameter = Chunk::DIAMETER.as_();
        Point3::new(
            (absl_pos.x / n_diameter).floor().as_(),
            (absl_pos.y / n_diameter).floor().as_(),
            (absl_pos.z / n_diameter).floor().as_(),
        )
    }

    pub fn iter(&self) -> <&Self as IntoIterator>::IntoIter {
        self.into_iter()
    }

    pub fn pack(&self) -> PackedOctree<ElementOf<Chunk>> {
        self.octree.pack()
    }

    pub fn unpack(
        packed: PackedOctree<ElementOf<Self>>,
        pos: PositionOf<Chunk>,
    ) -> Result<Self, UnpackError<FieldOf<OctreeOf<Self>>>> {
        <OctreeOf<Self>>::unpack(packed).map(|octree| Chunk::new(pos, octree))
    }
}

impl<'a> IntoIterator for &'a Chunk {
    type IntoIter = <&'a OctreeOf<Chunk> as IntoIterator>::IntoIter;
    type Item = <Self::IntoIter as Iterator>::Item;

    fn into_iter(self) -> Self::IntoIter {
        self.octree.into_iter()
    }
}

// Unlike ElementType FieldType is the coordinate type for Chunks which is i32 (not u8 the field of the Octree).
impl FieldType for Chunk {
    type Field = i32;
}
impl HasPosition for Chunk {
    type Position = Point3<FieldOf<Chunk>>;

    fn position(&self) -> &Self::Position {
        &self.pos
    }
}
impl ElementType for Chunk {
    type Element = ElementOf<OctreeOf<Chunk>>;
}
impl Diameter for Chunk {
    type Diameter = <OctreeOf<Chunk> as Diameter>::Diameter;

    const DIAMETER: usize = <OctreeOf<Chunk> as Diameter>::DIAMETER;
}

impl<F> RayCast<F> for Chunk
where
    F: RealField,
    OctreeOf<Chunk>: RayCast<F>,
{
    fn toi_and_normal_with_ray(
        &self,
        m: &Isometry<F>,
        ray: &Ray<F>,
        solid: bool,
    ) -> Option<RayIntersection<F>> {
        self.octree.toi_and_normal_with_ray(m, ray, solid)
    }
    fn toi_and_normal_and_uv_with_ray(
        &self,
        m: &Isometry<F>,
        ray: &Ray<F>,
        solid: bool,
    ) -> Option<RayIntersection<F>> {
        self.octree.toi_and_normal_and_uv_with_ray(m, ray, solid)
    }
    fn toi_with_ray(&self, m: &Isometry<F>, ray: &Ray<F>, solid: bool) -> Option<F> {
        self.octree.toi_with_ray(m, ray, solid)
    }
}

pub fn cube_mesh(size: f32) -> MeshBuilder<'static> {
    use amethyst::renderer::rendy::mesh::{Normal, Position, TexCoord};
    use itertools::repeat_n;
    // vertices
    let v = [
        [0.0, 0.0, size],   // 0
        [size, 0.0, size],  // 1
        [0.0, size, size],  // 2
        [size, size, size], // 3
        [0.0, size, 0.0],   // 4
        [size, size, 0.0],  // 5
        [0.0, 0.0, 0.0],    // 6
        [size, 0.0, 0.0],   // 7
    ];
    // normal
    let n = [
        Normal([0.0, 0.0, 1.0]),
        Normal([0.0, 1.0, 0.0]),
        Normal([0.0, 0.0, -1.0]),
        Normal([0.0, -1.0, 0.0]),
        Normal([1.0, 0.0, 0.0]),
        Normal([-1.0, 0.0, 0.0]),
    ];

    // textures
    let tx = [
        TexCoord([0.0, 0.0]),
        TexCoord([size, 0.0]),
        TexCoord([0.0, size]),
        TexCoord([size, size]),
    ];

    let positions: Vec<Position> = vec![
        0, 1, 2, 2, 1, 3, 2, 3, 4, 4, 3, 5, 4, 5, 6, 6, 5, 7, 6, 7, 0, 0, 7, 1, 1, 7, 3, 3, 7, 5,
        6, 0, 4, 4, 0, 2,
    ]
    .into_iter()
    .map(|i| Position(v[i]))
    .collect();
    let normals: Vec<Normal> = repeat_n(0, 6)
        .chain(repeat_n(1, 6))
        .chain(repeat_n(2, 6))
        .chain(repeat_n(3, 6))
        .chain(repeat_n(4, 6))
        .chain(repeat_n(5, 6))
        .map(|i| n[i])
        .collect();
    let tex_coords: Vec<TexCoord> = vec![0, 1, 2, 2, 1, 3]
        .into_iter()
        .cycle()
        .take(positions.len())
        .map(|i| tx[i])
        .collect();

    MeshBuilder::new()
        .with_vertices(positions)
        .with_vertices(normals)
        .with_vertices(tex_coords)
}

#[cfg(test)]
mod test {
    use super::Chunk;
    use crate::{
        octree::Diameter,
        terrain::{PlateauGenerateBlock, SphereGenerateBlock, Terrain},
    };
    use amethyst::core::math::Point3;
    use galvanic_assert::assert_that;
    use galvanic_assert::matchers::*;
    use ncollide3d::{
        math::{Isometry, Point, Vector},
        query::{Ray, RayCast},
        shape::FeatureId,
    };

    #[test]
    pub fn test_ray_casting_with_plateau_from_above() {
        let terrain =
            Terrain::default().with_block_generator(PlateauGenerateBlock::new(Chunk::DIAMETER / 2));

        let chunk = terrain.generate_chunk(Point3::origin());
        let iso_id = Isometry::identity();

        // Left Face
        let ray = Ray::new(Point::new(-32., 16., 16.), Vector::new(1., 0., 0.));
        let inter = chunk
            .toi_and_normal_and_uv_with_ray(&iso_id, &ray, true)
            .unwrap();
        assert_that!(&inter.toi, close_to(32., std::f32::EPSILON));
        assert_eq!(inter.normal, Vector::new(-1., 0., 0.));
        assert_eq!(inter.feature, FeatureId::Face(0));

        // Down Face
        let ray = Ray::new(Point::new(32., -32., 32.), Vector::new(0., 1., 0.));
        let inter = chunk.toi_and_normal_with_ray(&iso_id, &ray, true).unwrap();
        assert_that!(&inter.toi, close_to(32., std::f32::EPSILON));
        assert_eq!(inter.normal, Vector::new(0., -1., 0.));
        assert_eq!(inter.feature, FeatureId::Face(1));

        // Front Face
        let ray = Ray::new(Point::new(16., 16., -32.), Vector::new(0., 0., 1.));
        let inter = chunk.toi_and_normal_with_ray(&iso_id, &ray, true).unwrap();
        assert_that!(&inter.toi, close_to(32., std::f32::EPSILON));
        assert_eq!(inter.normal, Vector::new(0., 0., -1.));
        assert_eq!(inter.feature, FeatureId::Face(2));

        // Right Face
        let ray = Ray::new(Point::new(96., 16., 16.), Vector::new(-1., 0., 0.));
        let inter = chunk.toi_and_normal_with_ray(&iso_id, &ray, true).unwrap();
        assert_that!(&inter.toi, close_to(32., std::f32::EPSILON));
        assert_eq!(inter.normal, Vector::new(1., 0., 0.));
        assert_eq!(inter.feature, FeatureId::Face(3));

        // Up Face
        let ray = Ray::new(Point::new(32., 64., 32.), Vector::new(0., -1., 0.));
        let inter = chunk.toi_and_normal_with_ray(&iso_id, &ray, true).unwrap();
        assert_that!(&inter.toi, close_to(32., std::f32::EPSILON));
        assert_eq!(inter.normal, Vector::new(0., 1., 0.));
        assert_eq!(inter.feature, FeatureId::Face(4));

        // Back Face
        let ray = Ray::new(Point::new(16., 16., 96.), Vector::new(0., 0., -1.));
        let inter = chunk.toi_and_normal_with_ray(&iso_id, &ray, true).unwrap();
        assert_that!(&inter.toi, close_to(32., std::f32::EPSILON));
        assert_eq!(inter.normal, Vector::new(0., 0., 1.));
        assert_eq!(inter.feature, FeatureId::Face(5));
    }

    #[test]
    pub fn test_ray_casting_with_sphere_from_above() {
        let terrain =
            Terrain::default().with_block_generator(SphereGenerateBlock::new(Chunk::DIAMETER / 2));
        let chunk = terrain.generate_chunk(Point3::origin());

        let iso_id = Isometry::identity();
        let ray = Ray::new(Point::new(32., 64., 32.), Vector::new(0., -1., 0.));
        let toi = chunk.toi_with_ray(&iso_id, &ray, true);
        assert_eq!(toi, Some(0.));
    }

    #[quickcheck]
    fn qc_chunk_abls_chunk_is_identity(x: i32, y: i32, z: i32) -> bool {
        Point3::new(x, y, z)
            == Chunk::absl_to_chunk_coords::<f32>(Chunk::chunk_to_absl_coords(Point3::new(x, y, z)))
    }

    #[quickcheck]
    fn qc_absl_chunk_absl_snaps_to_chunk_boundaries(x: f32, y: f32, z: f32) -> bool {
        let diameter = Chunk::DIAMETER as f32;
        Point3::new(
            (x / diameter).floor() * diameter,
            (y / diameter).floor() * diameter,
            (z / diameter).floor() * diameter,
        ) == Chunk::chunk_to_absl_coords(Chunk::absl_to_chunk_coords(Point3::new(x, y, z)))
    }
}
