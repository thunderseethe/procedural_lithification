pub type Block = u32;
pub static AIR_BLOCK: Block = 0;
pub static DIRT_BLOCK: Block = 1;
