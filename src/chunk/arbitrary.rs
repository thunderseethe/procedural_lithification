use super::{Chunk, OctreeOf};

use amethyst::core::math::Point3;
use quickcheck::{Arbitrary, Gen};

#[cfg(test)]
impl Arbitrary for Chunk {
    fn arbitrary<G: Gen>(g: &mut G) -> Self {
        //let octree = <OctreeOf<Self> as Arbitrary>::arbitrary(g);
        let octree = OctreeOf::<Self>::default();
        let (x, y, z) = <(i32, i32, i32)>::arbitrary(g);
        Chunk::new(Point3::new(x, y, z), octree)
    }
}