use crate::chunk::{Chunk, OctreeOf};
use amethyst::core::math::Point3;
use field::*;

use crate::octree::pack::UnpackError;

#[derive(Debug)]
pub enum ChunkSerdeError<F> {
    /// Error occurred in serde format code
    Format(F),
    /// Error occurred during unpacking code
    Unpack(UnpackError<FieldOf<OctreeOf<Chunk>>>),
}
pub type Result<T> = std::result::Result<T, ChunkSerdeError<bincode::Error>>;
// This may be sketch but more than likely we will need something like this
// And we want to do it as a trait so external code doesn't rely on the fact that we just use bincodes variant for this
// If we use a format other than bincode at some point in the futue this will make the hotswap easier
impl From<std::io::Error> for ChunkSerdeError<bincode::Error> {
    fn from(err: std::io::Error) -> Self {
        ChunkSerdeError::Format(Box::new(bincode::ErrorKind::Io(err)))
    }
}
impl From<bincode::Error> for ChunkSerdeError<bincode::Error> {
    fn from(err: bincode::Error) -> Self {
        ChunkSerdeError::Format(err)
    }
}
impl<F> From<UnpackError<FieldOf<OctreeOf<Chunk>>>> for ChunkSerdeError<F> {
    fn from(err: UnpackError<FieldOf<OctreeOf<Chunk>>>) -> Self {
        ChunkSerdeError::Unpack(err)
    }
}

pub struct ChunkDeserialize;
impl ChunkDeserialize {
    pub fn from<R>(reader: R, pos: Point3<FieldOf<Chunk>>) -> Result<Chunk>
    where
        R: std::io::Read,
    {
        bincode::deserialize_from(reader)
            .map_err(ChunkSerdeError::Format)
            .and_then(|packed| Chunk::unpack(packed, pos).map_err(ChunkSerdeError::Unpack))
    }
}

pub struct ChunkSerialize;
impl ChunkSerialize {
    pub fn into<W>(writer: W, chunk: &Chunk) -> Result<()>
    where
        W: std::io::Write,
    {
        let packed = chunk.pack();
        bincode::serialize_into(writer, &packed).map_err(ChunkSerdeError::Format)
    }
}

pub fn serialize_chunk(chunk: &Chunk) -> std::result::Result<Vec<u8>, std::io::Error> {
    let packed = chunk.pack();
    bincode::serialize(&packed).map_err(|err| match *err {
        bincode::ErrorKind::Io(err) => err,
        err => std::io::Error::new(std::io::ErrorKind::Other, err)
    })
}