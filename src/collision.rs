use crate::chunk::Chunk;
use field::*;
use amethyst::core::math as na;
use amethyst::core::math::{Isometry3, Point3, Vector3};
use amethyst::ecs::{Component, VecStorage};
use ncollide3d::pipeline::{
    CollisionGroups, CollisionObjectSlabHandle, CollisionWorld, GeometricQueryType, ProximityEvents,
};
use ncollide3d::shape::{Cuboid, ShapeHandle};
use std::borrow::Borrow;
use std::collections::HashMap;
use std::result::Result;

const TERRAIN_GROUP: usize = 1;
const PLAYER_GROUP: usize = 2;

pub struct CollisionId(pub CollisionObjectSlabHandle);
impl Component for CollisionId {
    type Storage = VecStorage<Self>;
}
impl CollisionId {
    pub fn new(handle: CollisionObjectSlabHandle) -> Self {
        CollisionId(handle)
    }
    pub fn handle(&self) -> CollisionObjectSlabHandle {
        self.0
    }
}

#[derive(Debug)]
pub enum CollisionDetectionError {
    ChunkAlreadyPresent,
    HandleNotFound,
}

// better name
pub struct CollisionDetection {
    world: CollisionWorld<f32, ShapeHandle<f32>>,
    terrain_handles: HashMap<Point3<FieldOf<Chunk>>, Vec<CollisionObjectSlabHandle>>,
}

impl Default for CollisionDetection {
    fn default() -> Self {
        CollisionDetection {
            world: CollisionWorld::new(0.2),
            terrain_handles: HashMap::new(),
        }
    }
}

impl CollisionDetection {
    pub fn add_player<P>(&mut self, pos: P) -> CollisionObjectSlabHandle
    where
        P: Borrow<Point3<f32>>,
    {
        let player_pos = pos.borrow();
        let isometry = Isometry3::translation(player_pos.x, player_pos.y, player_pos.z);
        let shape = ShapeHandle::new(Cuboid::new(Vector3::new(1.5, 1.0, 0.5)));
        let (handle, _) = self.world.add(
            isometry,
            shape.clone(),
            CollisionGroups::new().with_membership(&[PLAYER_GROUP]),
            GeometricQueryType::Proximity(0.1),
            shape,
        );
        handle
    }

    pub fn update_pos<P>(
        &mut self,
        handle: CollisionObjectSlabHandle,
        pos: P,
    ) -> Result<(), CollisionDetectionError>
    where
        P: Borrow<Point3<f32>>,
    {
        let p = pos.borrow();
        self.world
            .get_mut(handle)
            .map(|player| player.set_position(Isometry3::translation(p.x, p.y, p.z)))
            .ok_or(CollisionDetectionError::HandleNotFound)
    }

    pub fn add_chunk(&mut self, chunk: &Chunk) -> Result<(), CollisionDetectionError> {
        if self.terrain_handles.contains_key(&chunk.pos) {
            return Err(CollisionDetectionError::ChunkAlreadyPresent);
        }
        let root = Chunk::chunk_to_absl_coords(chunk.pos);
        let terrain_handles = chunk
            .iter()
            .map(|octant| {
                let rel_pos: Point3<FieldOf<Chunk>> = na::convert(*octant.bottom_left_front);
                let pos: Point3<f32> = na::convert(root + rel_pos.coords);
                let radius = (octant.diameter / 2) as f32;
                let isometry =
                    Isometry3::translation(pos.x + radius, pos.y + radius, pos.z + radius);
                let shape = ShapeHandle::new(Cuboid::new(Vector3::new(radius, radius, radius)));
                let (handle, _) = self.world.add(
                    isometry,
                    shape.clone(),
                    CollisionGroups::new()
                        .with_membership(&[TERRAIN_GROUP])
                        .with_blacklist(&[TERRAIN_GROUP]),
                    GeometricQueryType::Proximity(0.2),
                    shape,
                );
                handle
            })
            .collect();
        self.terrain_handles.insert(chunk.pos, terrain_handles);
        Ok(())
    }

    pub fn remove_chunk(&mut self, chunk: &Chunk) {
        use std::collections::hash_map::Entry::*;
        if let Occupied(entry) = self.terrain_handles.entry(chunk.pos) {
            let (_, handles) = entry.remove_entry();
            self.world.remove(&handles);
        }
    }

    pub fn update(&mut self) {
        self.world.update()
    }

    pub fn proximity_events(&mut self) -> &ProximityEvents<CollisionObjectSlabHandle> {
        self.world.proximity_events()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::chunk::{Chunk, OctreeOf};
    use crate::octree::Diameter;
    use crate::terrain::{HeightMap, Terrain};
    use amethyst::core::math::{Point3, Vector3};
    //use ncollide3d::math::{Point, Vector};
    use ncollide3d::query::Ray;

    #[test]
    fn test_proximity_event_created_for_player_near_chunk() {
        let mut world = CollisionDetection::default();
        let _player_handle = world.add_player(Point3::origin());
        let chunk = Terrain::default()
            .with_block_generator(
                |_height_map: &HeightMap, p: Point3<FieldOf<OctreeOf<Chunk>>>| {
                    if p.y < (Chunk::DIAMETER / 2) as u8 {
                        Some(1)
                    } else {
                        None
                    }
                },
            )
            .generate_chunk(Point3::origin());
        world
            .add_chunk(&chunk)
            .expect("Empty world contained a chunk");
        world.update();
        let player_ray = Ray::new(Point3::new(64., 64., -2.), Vector3::new(0., 0., 1.));
        let groups = CollisionGroups::new()
            .with_membership(&[PLAYER_GROUP])
            .with_blacklist(&[PLAYER_GROUP]);

        let intersections = world.world.interferences_with_ray(&player_ray, &groups);
        for (_, _, intersection) in intersections {
            println!("intersection: {:?}", intersection);
            println!(
                "point of contact: {}",
                player_ray.origin + player_ray.dir * intersection.toi
            );
        }
    }
}
