use amethyst::core::math::{allocator::Allocator, DefaultAllocator, DimName, Point, Scalar};

pub trait PointExt<I, D>
where
    D: DimName,
{
    fn map<Output, F>(self, map_fn: F) -> Point<Output, D>
    where
        Output: Scalar,
        F: FnMut(I) -> Output,
        DefaultAllocator: Allocator<Output, D>;
}

impl<I, D> PointExt<I, D> for Point<I, D>
where
    I: Scalar,
    D: DimName,
    DefaultAllocator: Allocator<I, D>,
{
    fn map<Output, F>(self, map_fn: F) -> Point<Output, D>
    where
        Output: Scalar,
        F: FnMut(I) -> Output,
        DefaultAllocator: Allocator<Output, D>,
    {
        Point::from(self.coords.map(map_fn))
    }
}
