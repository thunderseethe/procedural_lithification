use crate::{
    chunk::{Chunk, serde},
    collision::{CollisionDetection, CollisionId},
    dimension::{Dimension, DimensionConfig, DimensionRes, StoredChunk},
    field::*,
    player::{PlayerControlTag, PlayerAddr},
    systems::{
        dimension::DimensionChunkEvent,
    }
};
use amethyst::{
    core::{math::Point3, ArcThreadPool, Transform},
    ecs::Join,
    prelude::*,
};

use crossbeam::channel::{Sender};
use futures::{FutureExt, Future};
use log::*;
use tokio::runtime::Runtime;

pub struct ServerDimensionState {
    dimension_config: DimensionConfig,
    // Holds points that have been queued for generation so we don't re generate them
    generate_tick: usize,
}
impl ServerDimensionState {
    pub fn new(dimension_config: DimensionConfig) -> Self {
        ServerDimensionState {
            dimension_config,
            generate_tick: 0,
        }
    }
}

pub fn spawn_chunk_events<'a, L, G>(
    (load_chunks, create_chunks): (Vec<L>, Vec<G>),
    runtime: &mut Runtime,
    thread_pool: &ArcThreadPool,
    chunk_send: Sender<DimensionChunkEvent>,
)
where
    L: 'static + Send + Future<Output=serde::Result<StoredChunk>>,
    G: 'a + Send + FnOnce() -> StoredChunk
{

    for load_chunk in load_chunks.into_iter() {
        let chunk_send = chunk_send.clone();
        runtime.spawn(load_chunk.map(move |chunk| {
            let point = chunk.expect("Error deserializing chunk").read().pos;
            chunk_send.send(DimensionChunkEvent::NewChunkAt(point.into())).map_err(|err|{
                error!("Failed to send message during chunk load: {:?}", err);
            }).expect("");
        }));
    }

    thread_pool.scope(move |s| {
        create_chunks
            .into_iter()
            .for_each(|create_chunk| {
                let chunk_send = (&chunk_send).clone();
                s.spawn(move |_| {
                    let chunk = create_chunk();
                    let point = chunk.read().pos;
                    chunk_send.send(DimensionChunkEvent::NewChunkAt(point.into())).map_err(|err| {
                        error!("Failed to send dimension chunk event: {:?}", err);
                    }).expect("");
                })
            });
    });

}

impl<'a, 'b> State<GameData<'a, 'b>, StateEvent> for ServerDimensionState {
    fn on_start(&mut self, data: StateData<GameData>) {
        let StateData { world, .. } = data;
        world.register::<CollisionId>();
        world.register::<PlayerControlTag>();

        let mut collision = CollisionDetection::default();
        let mut transform = Transform::default();
        let player_pos = Point3::new(32.0, 64.0, 32.0);
        let player_handle = collision.add_player(player_pos);
        transform.set_translation(player_pos.coords);
        world
            .create_entity()
            .with(transform)
            .with(CollisionId::new(player_handle))
            .build();

        world.insert(collision);

        let dimension =
            Dimension::with_config(self.dimension_config.clone());
        world.insert(dimension);

        info!("SERVER STARTED");

    }

    fn on_stop(&mut self, data: StateData<GameData>) {
        let StateData { world, .. } = data;
        let dimension = world.write_resource::<DimensionRes>();
        let runtime = world.write_resource::<Runtime>();
        runtime.spawn(dimension.store());
    }

    fn handle_event(
        &mut self,
        _data: StateData<'_, GameData<'a, 'b>>,
        _event: StateEvent,
    ) -> Trans<GameData<'a, 'b>, StateEvent> {
        Trans::None
    }

    fn update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        data.data.update(&data.world);
        Trans::None
    }

    fn fixed_update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        self.generate_tick += 1;
        if self.generate_tick % 100 != 1 {
            return Trans::None;
        }

        let chunk_send_ref = data.world.read_resource::<Sender<DimensionChunkEvent>>();
        let dimension = data.world.read_resource::<Dimension>();
        let generate_radius = dimension.config.generate_radius;
        for (addr, transform) in (
            &data.world.read_storage::<PlayerAddr>(),
            &data.world.read_storage::<Transform>(),
        )
            .join()
        {
            let player_pos = Point3::from(*transform.translation());
            let thread_pool = data.world.read_resource::<ArcThreadPool>().clone();
            let mut runtime = data.world.write_resource::<Runtime>();
            let chunk_send = (*chunk_send_ref).clone();
            spawn_chunk_events(
                dimension.spawn_around_point(player_pos, generate_radius as FieldOf<Chunk>), 
                &mut runtime, 
                &thread_pool,
                chunk_send
            );
            info!("Spawned chunks for {:?}", addr);
        }
        Trans::None
    }
}
