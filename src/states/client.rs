use crate::{
    collision::{CollisionDetection, CollisionId},
    dimension::{Dimension, DimensionRes},
    player::PlayerControlTag,
};
use amethyst::{
    core::{
        ecs::Write,
        shrev::EventChannel,
        math::{Point3, Vector3},
        Transform,
    },
    input::{is_close_requested, is_key_down, VirtualKeyCode},
    prelude::*,
    renderer::{
        light::{DirectionalLight, Light},
        palette::rgb::Rgb,
        Camera,
    },
    ui::UiCreator,
};
use log::debug;
use tokio::runtime::Runtime;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};


#[derive(Default)]
pub struct ClientDimensionState {}

impl<'a, 'b> State<GameData<'a, 'b>, StateEvent> for ClientDimensionState {
    fn on_start(&mut self, data: StateData<GameData>) {
        let StateData { world, .. } = data;

        debug!("Creating lights...");
        let light: Light = DirectionalLight {
            color: Rgb::new(0.9, 0.9, 0.9),
            direction: Vector3::new(-1.0, -1.0, -1.0),
            intensity: 1.0,
        }
        .into();

        world.create_entity().with(light).build();

        let mut collision = CollisionDetection::default();
        let mut transform = Transform::default();
        let player_pos = Point3::new(32.0, 64.0, 32.0);
        let player_handle = collision.add_player(player_pos);
        transform.set_translation(player_pos.coords);
        transform.append_rotation(Vector3::y_axis(), std::f32::consts::PI);
        world
            .create_entity()
            .with(Camera::standard_3d(1600.0, 900.0))
            .with(transform)
            .with(PlayerControlTag::default())
            .with(CollisionId::new(player_handle))
            .build();

        world.insert(Dimension::default());
        world.insert(collision);


        //let localhost = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 7072u16);
        world.exec(
            |mut creator: UiCreator<'_>| {
                creator.create("ui/position.ron", ());
            },
        );
    }

    fn on_stop(&mut self, data: StateData<GameData>) {
        let StateData { world, .. } = data;
        let dimension = world.write_resource::<DimensionRes>();
        let runtime = world.write_resource::<Runtime>();
        runtime.spawn(dimension.store());
    }

    fn update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        data.data.update(&data.world);
        Trans::None
    }
    fn handle_event(
        &mut self,
        data: StateData<'_, GameData<'a, 'b>>,
        event: StateEvent,
    ) -> Trans<GameData<'a, 'b>, StateEvent> {
        let StateData { world, .. } = data;
        if let StateEvent::Window(event) = &event {
            if is_close_requested(&event) || is_key_down(&event, VirtualKeyCode::Escape) {
                Trans::Quit
            } else {
                Trans::None
            }
        } else {
            Trans::None
        }
    }
}
